﻿using ClassLibrary1.ResponseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TicTacTec.TA.Library;

namespace ClassLibrary1
{
    public class Otp
    {

        public static void UpdateOtpFunds()
        {
            Common.Logger("\nUpdateOtpFunds() " + DateTime.Now.ToString());
            try
            {
                OtpDb otp = new OtpDb();
                var alapidk = (from f in otp.otpfund
                               orderby f.lastupdate ascending
                               select f.id).ToList();
                foreach (var alapid in alapidk)
                    UpdateFund(alapid);
                Common.Logger("\nOTP UPDATE READY " + DateTime.Now.ToString());
            }
            catch (Exception e)
            {
                Helper.Instance.LogException(e);
            }
        }

        private static int UpdateFund(int fundid)
        {
            OtpDb otp = new OtpDb();
            otpfund fund = (from f in otp.otpfund
                            where f.id == fundid
                            select f).FirstOrDefault();
            fund.state = 1;
            otp.SaveChanges();
            Common.Logger("\n\n===============" + DateTime.Now.ToString() + "=================\nUpdateing Fund: " + fund.name);
            int linesChanged = 0;
            string strPriceQueryUrl = getOtpFundQueryString(fund.issn, "true", fund.lastupdate);
            string strVolumeQueryUrl = getOtpFundQueryString(fund.issn, "false", fund.lastupdate);
            string strPriceData = Http.Get("https://www.otpbank.hu/otpalapkezelo/do/hozamDBletoltes/grafikonExport", strPriceQueryUrl).Result.Replace("\"", "");
            string strVolumeData = Http.Get("https://www.otpbank.hu/otpalapkezelo/do/hozamDBletoltes/grafikonExport", strVolumeQueryUrl).Result.Replace("\"", "");
            List<string> strListPriceData = strPriceData.Split('\n').ToList();
            List<string> strListVolumeData = strVolumeData.Split('\n').ToList();
            if (strListPriceData != null && strListPriceData != null && strListPriceData.Count > 3 && strListPriceData.Count == strListVolumeData.Count)
            {
                List<otpdata> parsedData = ParseRowData(strListPriceData, strListVolumeData, fund.id);
                Common.Logger("Recieved " + parsedData.Count + " new data");
                foreach (otpdata newRow in parsedData)
                {
                    if ((from d in otp.otpdata
                         where d.fund == fund.id && d.date == newRow.date
                         select true).Any()) continue;
                    otp.otpdata.Add(newRow);
                }
                linesChanged = otp.SaveChanges();
                Common.Logger("Rows changed: " + linesChanged);
                fund.number = (from od in otp.otpdata
                               where od.fund == fund.id
                               select true).Count();
                Common.Logger("Rows: " + fund.number);
                if(parsedData.Last() != null)
                    Common.Logger("Last date: " + parsedData.Last().date.ToShortDateString());
            }
            fund.state = 0;
            fund.lastupdate = DateTime.Today;
            otp.SaveChanges();
            return linesChanged;
        }

        private static List<otpdata> ParseRowData(List<string> strListPriceData, List<string> strListVolumeData, int fund)
        {
            List<otpdata> parsedData = new List<otpdata>();
            strListPriceData.RemoveAt(0);
            strListPriceData.RemoveAt(0);
            strListVolumeData.RemoveAt(0);
            strListVolumeData.RemoveAt(0);
            for (int i = 0; i < strListPriceData.Count; i++)
            {
                List<string> priceRow = strListPriceData.ElementAt(i).Split(',').ToList();
                List<string> volumeRow = strListVolumeData.ElementAt(i).Split(',').ToList();
                if (priceRow.Count < 2) break;
                DateTime tempDate = new DateTime();
                double tempCapitalization = -1;
                double tempPrice = -1;
                if (DateTime.TryParse(priceRow.ElementAt(0), out tempDate) && double.TryParse(priceRow.ElementAt(1).Replace('.', ','), out tempPrice) && double.TryParse(volumeRow.ElementAt(1).Replace('.', ','), out tempCapitalization))
                {
                    parsedData.Add(new otpdata()
                    {
                        fund = fund,
                        date = tempDate,
                        price = tempPrice,
                        capitalization = tempCapitalization
                    });
                }
            }
            parsedData.Reverse();
            return parsedData;
        }

        private static string getOtpFundQueryString(string issn, string arfolyam, DateTime from)
        {
            return "?issnKod=" + issn +
                "&exportType=1&arfolyam=" + arfolyam +
                "&kezdoDatumEv=" + from.Year.ToString() +
                "&kezdoDatumHonap=" + from.Month.ToString() +
                "&kezdoDatumNap=" + from.Month.ToString() +
                "&vegDatumEv=2050&vegDatumHonap=1&vegDatumNap=1";
        }

        public static StockListResponse getStockList(string orderby = "name")
        {
            var result = new StockListResponse();
            try
            {
                result.list = new List<StockListItem>();
                switch (orderby)
                {
                    case "name":
                        result.list.AddRange((from d in Db.Instance.otpdata
                                              join s in Db.Instance.otpfund on d.fund equals s.id
                                              orderby s.name
                                              group new { d, s } by d.fund into g
                                              select new StockListItem { Id = g.Key, name = g.FirstOrDefault().s.name, min = g.Min(p => p.d.date), max = g.Max(p => p.d.date) }).ToList());
                        break;
                    case "rank":
                        result.list.AddRange((from f in Db.Instance.otpfund
                                              orderby f.ranking
                                              select new StockListItem { Id = f.id, name = f.name, min = new DateTime(2000,1,1), max = DateTime.Today }).ToList());
                        break;
                }
              
            }
            catch (Exception e)
            {
                Helper.Instance.LogException(e);
            }
            return result;
        }

        public static List<otpdata> GetOtpFundDataBetweenDates(int id, DateTime datefrom, DateTime dateto)
        {
            OtpDb OtpDb = new OtpDb();
            var list = (from o in OtpDb.otpdata
                        where o.fund == id && o.date >= datefrom && o.date <= dateto
                        orderby o.date
                        select o).ToList();
            return list;
        }

        public static string getOtpFundName(int id)
        {
            OtpDb OtpDb = new OtpDb();
            return (from o in OtpDb.otpfund
                    where o.id == id
                    select o.name).FirstOrDefault();
        }

        public static void ranking()
        {
            OtpDb otp = new OtpDb();
            List<otpfund> alapok = otp.otpfund.ToList();
            List<RankingItem> ranking = new List<RankingItem>();
            double darab = alapok.Count;
            double cnt = 0;
            foreach (otpfund alap in alapok)
            {
                Console.WriteLine(Math.Round((double)(cnt * 100 / darab), 0) + " %");
                otpdata start = otp.otpdata.Where(d => d.fund == alap.id).OrderBy(d => d.date).FirstOrDefault();
                otpdata last = otp.otpdata.Where(d => d.fund == alap.id).OrderByDescending(d => d.date).FirstOrDefault();
                double y = Math.Round(((double)(last.price / start.price) * 100), 2);
                List<YieldInYear> yearlyYields = calculateYearlyYields(alap, start.date.Year, last.date.Year);
                double avgYearlyYield = (yearlyYields.Select(yield => yield.percent).Sum() / yearlyYields.Count);
                avgYearlyYield = Math.Round((double)avgYearlyYield, 2);
                double yearlyYield = CalculateYield(start.price, last.price, (last.date - start.date).Days);
                RankingItem r = new RankingItem
                {
                    alapid = alap.id,
                    name = alap.name,
                    capitalization = otp.otpdata.Where(o => o.fund == alap.id).OrderByDescending(o => o.date).FirstOrDefault().capitalization,
                    fromDate = start.date,
                    toDate = last.date,
                    days = (last.date - start.date).Days,
                    yieldPercent = y,
                    yearlyYields = yearlyYields,
                    avgYearlyYield = avgYearlyYield,
                    yearlyYield = yearlyYield
                };
                ranking.Add(r);
                cnt++;
            }

            //ranking = ranking.OrderByDescending(r => r.avgYearlyYield).ToList();
            ranking = ranking.OrderByDescending(r => r.yearlyYield).ToList();
            //ranking = ranking.OrderByDescending(r => r.capitalization).ToList();

            int rank = 1;
            string result = "";
            foreach (var r in ranking)
            {
                result += "_________________________  " + r.name + "  ___________________________________\n" + r.fromDate.ToStr() + "  -  " + r.toDate.ToStr() + "\n";
                result +=  "Yield: " + r.yieldPercent + " %   in  " + r.days + " days\n";
                result += "Avg. yearly yield: " + r.avgYearlyYield + " %\n";
                result += "Yearly yield: " + r.yearlyYield + " %\n";
                result += "Capitalization: " + r.capitalization.ToString("N") + "\n";
                foreach (var yy in r.yearlyYields)
                {
                    result += yy.year + ": " + yy.percent + "\n";
                }
                otp.otpfund.Find(r.alapid).ranking = rank++;
            }            
            otp.SaveChanges();
            Console.WriteLine(result);
            Common.Logger(result);
        }

        private static double CalculateYield(double buy, double sell, double days)
        {
            return Math.Round( (Math.Pow((sell / buy), ((double)365 / days)) - 1) * 100, 2);
        }

        private static List<YieldInYear> calculateYearlyYields(otpfund alap, int fromYear, int toYear)
        {
            OtpDb otp = new OtpDb();
            List<YieldInYear> yearlyYields = new List<YieldInYear>();
            for (int year = fromYear; year < toYear; year++)
            {
                otpdata firstInYear = otp.otpdata.Where(d => d.fund == alap.id && d.date.Year == year).OrderBy(d => d.date).FirstOrDefault();
                otpdata lastInYear = otp.otpdata.Where(d => d.fund == alap.id && d.date.Year == year).OrderByDescending(d => d.date).FirstOrDefault();
                var yearlyYield = Math.Round(((double)(lastInYear.price / firstInYear.price) * 100) - 100, 2);
                yearlyYields.Add(new YieldInYear() { year = year, percent = yearlyYield });
            }
            return yearlyYields;
        }

    }

    public class Drop
    {
        public double percent { get; set; }
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public DateTime? Recovered { get; set; }
        public int? RecoveryDays { get; set; }
    }

    public class DaysWithoutRise
    {
        public double dropPercent { get; set; }
        public int days { get; set; }
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
    }

    public class YieldInYear
    {
        public int year { get; set; }
        public double percent { get; set; }
    }

    public class YieldInMonth
    {
        public DateTime month { get; set; }
        public double percent { get; set; }
        public YieldInMonth(DateTime month)
        {
            this.month = month;
        }
    }

    public class RankingItem
    {
        public int alapid { get; set; }
        public string name { get; set; }
        public stockType type { get; set; }
        public double capitalization { get; set; }
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public double yieldPercent { get; set; }
        public int days { get; set; }
        public List<YieldInYear> yearlyYields { get; set; }
        public List<YieldInMonth> monthlyYields { get; set; }
        public double avgYearlyYield { get; set; }
        public double yearlyYield { get; set; }
        public double maxDropPercent { get; set; }
        public int maxDaysWithoutRise { get; set; }
        public List<Drop> DropList { get; set; }
        public List<DaysWithoutRise> DaysWithoutRise { get; set; } // TODO
        public RankingItem()
        {
            this.yearlyYields = new List<YieldInYear>();
            this.monthlyYields = new List<YieldInMonth>();
            this.DropList = new List<Drop>();
            this.DaysWithoutRise = new List<DaysWithoutRise>();
        }
    }
}
