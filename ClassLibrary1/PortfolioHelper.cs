﻿using ClassLibrary1.ResponseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public static class PortfolioHelper
    {
        public async static Task<string> DownloadPlainTextEquityData(string ticker, string from, string to)
        {
            List<KeyValuePair<string, string>> kvpList = new List<KeyValuePair<string, string>>();
            kvpList.Add(new KeyValuePair<string, string>("tipus", "1"));
            kvpList.Add(new KeyValuePair<string, string>("startdate", from));
            kvpList.Add(new KeyValuePair<string, string>("enddate", to));
            kvpList.Add(new KeyValuePair<string, string>("open", "1"));
            kvpList.Add(new KeyValuePair<string, string>("max", "1"));
            kvpList.Add(new KeyValuePair<string, string>("avg", "1"));
            kvpList.Add(new KeyValuePair<string, string>("min", "1"));
            kvpList.Add(new KeyValuePair<string, string>("close", "1"));
            kvpList.Add(new KeyValuePair<string, string>("forg", "1"));
            kvpList.Add(new KeyValuePair<string, string>("forgdb", "1"));
            kvpList.Add(new KeyValuePair<string, string>("ticker", ticker + ":" + from + ":" + to));
            kvpList.Add(new KeyValuePair<string, string>("text", "szövegfájl"));
            string response = await Http.Post("http://www.portfolio.hu", "history/reszveny-adatok.php", kvpList);
            return response;
        }

        public async static Task<string> DownloadPlainTextMutualFundData(string befalap, string from, string to)
        {
            List<KeyValuePair<string, string>> kvpList = new List<KeyValuePair<string, string>>();
            kvpList.Add(new KeyValuePair<string, string>("startdate", from));
            kvpList.Add(new KeyValuePair<string, string>("enddate", to));
            kvpList.Add(new KeyValuePair<string, string>("befalap", befalap + ":" + from + ":" + to));
            kvpList.Add(new KeyValuePair<string, string>("neertek", "1"));
            kvpList.Add(new KeyValuePair<string, string>("ejj", "1"));
            kvpList.Add(new KeyValuePair<string, string>("cf1d", "1"));
            kvpList.Add(new KeyValuePair<string, string>("hozam1y", "1"));
            kvpList.Add(new KeyValuePair<string, string>("text", "szövegfájl"));
            string response = await Http.Post("http://www.portfolio.hu", "history/befalap-adatok.php", kvpList);
            return response;
        }

        public static string GenerateCSV(string response, stockType stockType)
        {
            var lines = response.Split('\n').Skip(2).ToArray();
            switch (stockType)
            {
                case stockType.HungarianEquities:
                    lines[0] = lines[0].Replace("dátum", "Date")
                              .Replace("nyitóár", "Open")
                              .Replace("maximum ár", "High")
                              .Replace("minimum ár", "Low")
                              .Replace("záróár", "Close")
                              .Replace("átlagár", "Avg")
                              .Replace("forgalom (Ft)", "Volume")
                              .Replace("forgalom (db)", "VolumeCount");
                    break;
                case stockType.HungarianMutualFunds:
                    lines[0] = lines[0].Replace("dátum", "Date")
                              .Replace("nettóeszközé.", "Capitalization")
                              .Replace("cashflow", "Cashflow")
                              .Replace("egy jegyre jutó neé.", "Price")
                              .Replace("12 hónapos hozam", "YearlyYield");
                    break;
            }

            for (int l = 0; l < lines.Count(); l++)
            {
                bool spaceFound = false;
                for (int c = 0; c < lines[l].Count(); c++)
                {
                    if (lines[l][c] == ' ')
                    {
                        if (spaceFound == false)
                        {
                            lines[l] = lines[l].ReplaceAt(c, 1, ";");
                            spaceFound = true;
                        }
                    }
                    else
                    {
                        spaceFound = false;
                    }
                }
                lines[l] = lines[l].Replace(" ", "");
            }
            return string.Join("\n", lines);
        }
        
        public static void UpdateHungarianEquity(int ticker)
        {
            portfolio_hunstock stock = null;
            var db = new OtpDb();
            try
            {
                stock = db.portfolio_hunstock.Where(s => s.ticker == ticker).FirstOrDefault();
                if (stock.state == 1) return;
                stock.state = 1;
                db.SaveChanges();
                Console.WriteLine(stock.name);
                var lastDate = (from eq in db.portfolio_hunstock
                                join d in db.portfolio_hunstock_data on eq.id equals d.StockId
                                where eq.ticker == ticker
                                orderby d.Date descending
                                select d.Date).FirstOrDefault();
                if (lastDate == DateTime.MinValue)
                    lastDate = new DateTime(1970, 1, 1);
                if (isUpdated(lastDate, stockType.HungarianEquities))
                {
                    Console.WriteLine(stock.name + " updated");
                    stock.state = 0;
                    db.SaveChanges();
                    return;
                }
                lastDate = lastDate.AddDays(1);
                var csv = PortfolioHelper.DownloadPlainTextMutualFundData(ticker.ToString(), lastDate.ToStr(), DateTime.Today.ToStr()).Result;
                csv = PortfolioHelper.GenerateCSV(csv, stockType.HungarianEquities);
                var datas = Common.ParseCSV<PORFOLIO_HUN_EQUITY>(csv, ";").Select(d => new portfolio_hunstock_data
                {
                    Avg = d.Avg,
                    Close = d.Close,
                    Date = d.Date,
                    High = d.High,
                    Low = d.Low,
                    Open = d.Open,
                    Volume = d.Volume,
                    VolumeCount = (int)d.VolumeCount,
                    StockId = stock.id
                });
                foreach (var d in datas)
                {
                    db.portfolio_hunstock_data.Add(d);
                }
                stock.state = 0;
                db.SaveChanges();
                Console.WriteLine(stock.name + " ready");
            }
            catch (Exception e)
            {
                stock.state = -1;
                db.SaveChanges();
                Console.WriteLine(e.Message);
                Helper.Instance.LogException(e);
                throw e;
            }
        }

        public static void UpdateHungarianMutualFund(int ticker)
        {
            portfolio_hunfunds fund = null;
            var db = new OtpDb();
            try
            {
                fund = db.portfolio_hunfunds.Where(s => s.ticker == ticker).FirstOrDefault();
                if (fund.state == 1) return;
                fund.state = 1;
                db.SaveChanges();
                Console.WriteLine(fund.name);
                var lastDate = (from eq in db.portfolio_hunfunds
                                join d in db.portfolio_hunfund_data on eq.id equals d.FundId
                                where eq.ticker == ticker
                                orderby d.Date descending
                                select d.Date).FirstOrDefault();
                if (lastDate == DateTime.MinValue)
                    lastDate = new DateTime(1970, 1, 1);
                if (isUpdated(lastDate, stockType.HungarianMutualFunds))
                {
                    Console.WriteLine(fund.name + " updated");
                    fund.state = 0;
                    db.SaveChanges();
                    return;
                }
                lastDate = lastDate.AddDays(1);
                var csv = PortfolioHelper.DownloadPlainTextMutualFundData(ticker.ToString(), lastDate.ToStr(), DateTime.Today.ToStr()).Result;
                csv = PortfolioHelper.GenerateCSV(csv, stockType.HungarianMutualFunds);
                var datas = Common.ParseCSV<PORFOLIO_HUN_MUTUALFUND>(csv, ";").Select(d => new portfolio_hunfund_data
                {
                    FundId = fund.id,
                    Date = d.Date,
                    Price = d.Price,
                    YearlyYield = d.YearlyYield,
                    Capitalization = d.Capitalization,
                    Cashflow = d.Cashflow
                });
                foreach (var d in datas)
                {
                    db.portfolio_hunfund_data.Add(d);
                }
                fund.state = 0;
                db.SaveChanges();
                Console.WriteLine(fund.name + " ready");
            }
            catch (Exception e)
            {
                fund.state = -1;
                db.SaveChanges();
                Console.WriteLine(e.Message);
                Helper.Instance.LogException(e);
                throw e;
            }
        }

        public async static Task<List<TIME_SERIES_DAILY_ADJUSTED>> GetHungarianEquityBetweenDates(int ticker, DateTime dtFrom, DateTime dtTo)
        {
            var lastDate = (from eq in Db.Instance.portfolio_hunstock
                            join d in Db.Instance.portfolio_hunstock_data on eq.id equals d.StockId
                            where eq.ticker == ticker
                            orderby d.Date descending
                            select d.Date).FirstOrDefault();
            if (isUpdated(lastDate, stockType.HungarianEquities))
            {
                portfolio_hunstock stock = Db.Instance.portfolio_hunstock.Where(s => s.ticker == ticker).FirstOrDefault();
                return Db.Instance.portfolio_hunstock_data.Where(d => d.StockId == stock.id && d.Date >= dtFrom && d.Date <= dtTo).OrderBy(d => d.Date).Select(d => new TIME_SERIES_DAILY_ADJUSTED
                {
                    timestamp = d.Date,
                    volume = d.Volume.Value,
                    adjusted_close = d.Close.Value,
                    close = d.Close.Value,
                    high = d.High.Value,
                    low = d.Low.Value,
                    open = d.Open.Value
                }).ToList();
            }
            else
            {
                string csv = await PortfolioHelper.DownloadPlainTextEquityData(ticker.ToString(), dtFrom.ToStr(), dtTo.ToStr());
                csv = PortfolioHelper.GenerateCSV(csv, stockType.HungarianEquities);
                Task updateTask = Task.Run(() => UpdateHungarianEquity(ticker));
                return Common.ParseCSV<PORFOLIO_HUN_EQUITY>(csv, ";").OrderBy(d => d.Date).Select(p => new TIME_SERIES_DAILY_ADJUSTED
                {
                    timestamp = p.Date,
                    volume = p.Volume,
                    adjusted_close = p.Close,
                    close = p.Close,
                    high = p.High,
                    low = p.Low,
                    open = p.Open
                }).ToList();
            }
        }

        public async static Task<List<TIME_SERIES_DAILY_ADJUSTED>> GetHungarianMutualFundBetweenDates(int ticker, DateTime dtFrom, DateTime dtTo)
        {
            try
            {
                var lastDate = (from eq in Db.Instance.portfolio_hunfunds
                                join d in Db.Instance.portfolio_hunfund_data on eq.id equals d.FundId
                                where eq.ticker == ticker
                                orderby d.Date descending
                                select d.Date).FirstOrDefault();
                if (isUpdated(lastDate, stockType.HungarianMutualFunds))
                {
                    portfolio_hunfunds fund = Db.Instance.portfolio_hunfunds.Where(s => s.ticker == ticker).FirstOrDefault();
                    return Db.Instance.portfolio_hunfund_data.Where(d => d.FundId == fund.id && d.Date >= dtFrom && d.Date <= dtTo).OrderBy(d => d.Date).Select(d => new TIME_SERIES_DAILY_ADJUSTED
                    {
                        timestamp = d.Date,
                        adjusted_close = d.Price.Value,
                        close = d.Price.Value,
                        capitalization = d.Capitalization.Value,
                        volume = d.Cashflow.Value
                    }).ToList();
                }
                else
                {
                    string csv = await PortfolioHelper.DownloadPlainTextMutualFundData(ticker.ToString(), dtFrom.ToStr(), dtTo.ToStr());
                    csv = PortfolioHelper.GenerateCSV(csv, stockType.HungarianMutualFunds);
                    Task updateTask = Task.Run(() => UpdateHungarianMutualFund(ticker));
                    return Common.ParseCSV<PORFOLIO_HUN_MUTUALFUND>(csv, ";").OrderBy(d => d.Date).Select(p => new TIME_SERIES_DAILY_ADJUSTED
                    {
                        timestamp = p.Date,
                        close = p.Price,
                        adjusted_close = p.Price,
                        capitalization = p.Capitalization,
                        volume = p.Cashflow
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static bool isUpdated(DateTime lastDate, stockType type)
        {
            var diff = DateTime.Today - lastDate;
            int maxDaysOlds = 0;
            switch (type)
            {
                case stockType.HungarianEquities:
                    maxDaysOlds = 1;
                    break;
                case stockType.HungarianMutualFunds:
                    maxDaysOlds = 2;
                    break;
            }
            switch ((DayOfWeek)DateTime.Today.DayOfWeek)
            {
                case DayOfWeek.Saturday:
                    maxDaysOlds += 1;
                    break;
                case DayOfWeek.Sunday:
                    maxDaysOlds += 2;
                    break;
            }
            bool isUpdated = ((lastDate == DateTime.Today) // Utolso adat mai
                     ||
                   (lastDate >= DateTime.Today.AddDays(-maxDaysOlds) && DateTime.Now.TimeOfDay.TotalHours < 20) // Utolso adat tegnapi,vagy tegnapelőtti (bef alap) de azert mert a mai meg nem frissult
                      ||
                   (lastDate.DayOfWeek == DayOfWeek.Friday && (DateTime.Today - lastDate).TotalDays <= 2) // Utolso adat penteki, es 2 napnal nem regebbi
                   );
            return isUpdated;
        }

        public static List<StockListItem> GetHungarianEquitiesList()
        {
            List<StockListItem> result = new List<StockListItem>();
            #region StockList
            result.Add(new StockListItem { Id = 7326, name = "4IG" });
            result.Add(new StockListItem { Id = 199012, name = "ALTEO" });
            result.Add(new StockListItem { Id = 309088, name = "ALTERA" });
            result.Add(new StockListItem { Id = 14352, name = "ANY" });
            result.Add(new StockListItem { Id = 184094, name = "APPENINN" });
            result.Add(new StockListItem { Id = 73, name = "BIF" });
            result.Add(new StockListItem { Id = 201819, name = "CIGPANNONIA" });
            result.Add(new StockListItem { Id = 41715, name = "CSEPEL" });
            result.Add(new StockListItem { Id = 454107, name = "DUNAHOUSE" });
            result.Add(new StockListItem { Id = 132, name = "EHEP" });
            result.Add(new StockListItem { Id = 135, name = "ELMU" });
            result.Add(new StockListItem { Id = 136, name = "EMASZ" });
            result.Add(new StockListItem { Id = 54314, name = "ENEFI" });
            result.Add(new StockListItem { Id = 314, name = "ESTMEDIA" });
            result.Add(new StockListItem { Id = 38262, name = "ETFBUXOTP" });
            result.Add(new StockListItem { Id = 5686, name = "FHB" });
            result.Add(new StockListItem { Id = 159157, name = "FINEXT" });
            result.Add(new StockListItem { Id = 5151, name = "FORRAS/OE" });
            result.Add(new StockListItem { Id = 5152, name = "FORRAS/T" });
            result.Add(new StockListItem { Id = 245958, name = "FUTURAQUA" });
            result.Add(new StockListItem { Id = 308155, name = "GRENERGIE" });
            result.Add(new StockListItem { Id = 31203, name = "GSPARK" });
            result.Add(new StockListItem { Id = 165, name = "KARPOT" });
            result.Add(new StockListItem { Id = 141638, name = "KEG" });
            result.Add(new StockListItem { Id = 168, name = "KONZUM" });
            result.Add(new StockListItem { Id = 170, name = "KPACK" });
            result.Add(new StockListItem { Id = 157754, name = "KULCSSOFT" });
            result.Add(new StockListItem { Id = 246133, name = "MASTERPLAST" });
            result.Add(new StockListItem { Id = 206, name = "MOL" });
            result.Add(new StockListItem { Id = 178, name = "MTELEKOM" });
            result.Add(new StockListItem { Id = 162225, name = "NORDTELEKOM" });
            result.Add(new StockListItem { Id = 212286, name = "NUTEX" });
            result.Add(new StockListItem { Id = 277, name = "OPUS" });
            result.Add(new StockListItem { Id = 204343, name = "ORMESTER" });
            result.Add(new StockListItem { Id = 266, name = "OTP" });
            result.Add(new StockListItem { Id = 197477, name = "OTT1" });
            result.Add(new StockListItem { Id = 283, name = "PANNERGY" });
            result.Add(new StockListItem { Id = 274, name = "PFLAX" });
            result.Add(new StockListItem { Id = 212183, name = "PLOTINUS" });
            result.Add(new StockListItem { Id = 288, name = "RABA" });
            result.Add(new StockListItem { Id = 295, name = "RICHTER" });
            result.Add(new StockListItem { Id = 237592, name = "SET" });
            result.Add(new StockListItem { Id = 467518, name = "UBM" });
            result.Add(new StockListItem { Id = 225612, name = "VISONKA" });
            result.Add(new StockListItem { Id = 494053, name = "WABERERS" });
            result.Add(new StockListItem { Id = 316, name = "ZWACK" });
            #endregion
            return result;
        }

        public static List<StockListItem> GetHungarianMutualFundsList()
        {
            List<StockListItem> result = new List<StockListItem>();
            #region FundList
            result.Add(new StockListItem { Id = 990, name = "ACCESS Alternative Származtatott Nyíltvégű Befektetési Alap" });
            result.Add(new StockListItem { Id = 2442, name = "ACCESS Alternative Származtatott Nyíltvégű Befektetési Alap A sorozat" });
            result.Add(new StockListItem { Id = 2441, name = "ACCESS Alternative Származtatott Nyíltvégű Befektetési Alap IL sorozat" });
            result.Add(new StockListItem { Id = 50, name = "ACCESS PP Deposit Nyíltvégű Befektetési Alap" });
            result.Add(new StockListItem { Id = 2440, name = "ACCESS PP Deposit Nyíltvégű Befektetési Alap A sorozat" });
            result.Add(new StockListItem { Id = 2439, name = "ACCESS PP Deposit Nyíltvégű Befektetési Alap IL sorozat" });
            result.Add(new StockListItem { Id = 2386, name = "ADÜTON Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 331, name = "AEGON Alfa Abszolút Hozamú  Befektetési Alap A sorozat" });
            result.Add(new StockListItem { Id = 1205, name = "AEGON Alfa Abszolút Hozamú  Befektetési Alap B sorozat" });
            result.Add(new StockListItem { Id = 2466, name = "AEGON Alfa Abszolút Hozamú Befektetési Alap C sorozat" });
            result.Add(new StockListItem { Id = 2464, name = "AEGON Alfa Abszolút Hozamú Befektetési Alap E sorozat" });
            result.Add(new StockListItem { Id = 2463, name = "AEGON Alfa Abszolút Hozamú Befektetési Alap I sorozat" });
            result.Add(new StockListItem { Id = 1721, name = "AEGON Alfa Abszolút Hozamú Befektetési Alap R sorozat" });
            result.Add(new StockListItem { Id = 2465, name = "AEGON Alfa Abszolút Hozamú Befektetési Alap U sorozat" });
            result.Add(new StockListItem { Id = 75, name = "AEGON Belföldi Kötvény Befektetési Alap" });
            result.Add(new StockListItem { Id = 2645, name = "AEGON Belföldi Kötvény Befektetési Alap I sorozat" });
            result.Add(new StockListItem { Id = 863, name = "AEGON Bessa Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 766, name = "AEGON Climate Change Részvény Befektetési Alap A sorozat" });
            result.Add(new StockListItem { Id = 1066, name = "AEGON Climate Change Részvény Befektetési Alap B sorozat" });
            result.Add(new StockListItem { Id = 1744, name = "AEGON IstanBull Részvény Alap PL sorozat PL sorozat" });
            result.Add(new StockListItem { Id = 1099, name = "AEGON IstanBull Részvény Befektetési Alap A sorozat" });
            result.Add(new StockListItem { Id = 1355, name = "AEGON IstanBull Részvény Befektetési Alap I sorozat" });
            result.Add(new StockListItem { Id = 1604, name = "AEGON IstanBull Részvény Befektetési Alap P sorozat" });
            result.Add(new StockListItem { Id = 1549, name = "AEGON IstanBull Részvény Befektetési Alap T Sorozat" });
            result.Add(new StockListItem { Id = 78, name = "AEGON Közép-Európai Részvény Befektetési Alap A sorozat" });
            result.Add(new StockListItem { Id = 906, name = "AEGON Közép-Európai Részvény Befektetési Alap B sorozat" });
            result.Add(new StockListItem { Id = 2604, name = "AEGON Közép-Európai Részvény Befektetési Alap C sorozat" });
            result.Add(new StockListItem { Id = 1358, name = "AEGON Közép-Európai Részvény Befektetési Alap I sorozat" });
            result.Add(new StockListItem { Id = 2230, name = "AEGON Lengyel Kötvény Alap P sorozat" });
            result.Add(new StockListItem { Id = 751, name = "AEGON Lengyel Kötvény Befektetési Alap A sorozat" });
            result.Add(new StockListItem { Id = 1540, name = "AEGON Lengyel Kötvény Befektetési Alap I sorozat" });
            result.Add(new StockListItem { Id = 1634, name = "AEGON Lengyel Pénzpiaci Befektetési Alap A sorozat" });
            result.Add(new StockListItem { Id = 1635, name = "AEGON Lengyel Pénzpiaci Befektetési Alap I sorozat" });
            result.Add(new StockListItem { Id = 1544, name = "AEGON Lengyel Részvény Befektetési Alap B sorozat" });
            result.Add(new StockListItem { Id = 1503, name = "AEGON Lengyel Részvény Befektetési Alap I sorozat" });
            result.Add(new StockListItem { Id = 173, name = "AEGON MoneyMaxx Expressz Abszolút Hozamú Befektetési Alap" });
            result.Add(new StockListItem { Id = 2470, name = "AEGON MoneyMaxx Expressz Abszolút Hozamú Befektetési Alap C sorozat" });
            result.Add(new StockListItem { Id = 2469, name = "AEGON MoneyMaxx Expressz Abszolút Hozamú Befektetési Alap E sorozat" });
            result.Add(new StockListItem { Id = 2467, name = "AEGON MoneyMaxx Expressz Abszolút Hozamú Befektetési Alap I sorozat" });
            result.Add(new StockListItem { Id = 1709, name = "AEGON MoneyMaxx Expressz Abszolút Hozamú Befektetési Alap P sorozat" });
            result.Add(new StockListItem { Id = 1708, name = "AEGON MoneyMaxx Expressz Abszolút Hozamú Befektetési Alap R sorozat" });
            result.Add(new StockListItem { Id = 2468, name = "AEGON MoneyMaxx Expressz Abszolút Hozamú Befektetési Alap U sorozat" });
            result.Add(new StockListItem { Id = 76, name = "AEGON Nemzetközi Kötvény Befektetési Alap A sorozat" });
            result.Add(new StockListItem { Id = 77, name = "AEGON Nemzetközi Részvény Befektetési Alap A sorozat" });
            result.Add(new StockListItem { Id = 907, name = "AEGON Nemzetközi Részvény Befektetési Alap B sorozat" });
            result.Add(new StockListItem { Id = 1701, name = "AEGON Nemzetközi Részvény Befektetési Alap I sorozat" });
            result.Add(new StockListItem { Id = 2273, name = "AEGON Panoráma Származtatott Befektetési Alap A sorozat (HUF)" });
            result.Add(new StockListItem { Id = 2280, name = "AEGON Panoráma Származtatott Befektetési Alap E sorozat" });
            result.Add(new StockListItem { Id = 2274, name = "AEGON Panoráma Származtatott Befektetési Alap I sorozat (HUF)" });
            result.Add(new StockListItem { Id = 2285, name = "AEGON Panoráma Származtatott Befektetési Alap P sorozat" });
            result.Add(new StockListItem { Id = 2286, name = "AEGON Panoráma Származtatott Befektetési Alap R sorozat" });
            result.Add(new StockListItem { Id = 2281, name = "AEGON Panoráma Származtatott Befektetési Alap U sorozat" });
            result.Add(new StockListItem { Id = 139, name = "AEGON Pénzpiaci Befektetési Alap" });
            result.Add(new StockListItem { Id = 2646, name = "AEGON Pénzpiaci Befektetési Alap I sorozat" });
            result.Add(new StockListItem { Id = 1743, name = "AEGON Russia PI sorozat PI sorozat" });
            result.Add(new StockListItem { Id = 1098, name = "AEGON Russia Részvény Befektetési Alap A sorozat" });
            result.Add(new StockListItem { Id = 1348, name = "AEGON Russia Részvény Befektetési Alap I sorozat" });
            result.Add(new StockListItem { Id = 1670, name = "AEGON Russia Részvény Befektetési Alap P sorozat" });
            result.Add(new StockListItem { Id = 1162, name = "AEGON Smart Money Befektetési Alapok Alapja" });
            result.Add(new StockListItem { Id = 750, name = "AEGON Ázsia Részvény Befektetési Alapok Alapja A sorozat" });
            result.Add(new StockListItem { Id = 888, name = "AEGON Ázsia Részvény Befektetési Alapok Alapja B sorozat" });
            result.Add(new StockListItem { Id = 676, name = "AEGON Ózon Éves Tőkevédett Befektetési Alap" });
            result.Add(new StockListItem { Id = 1167, name = "Aberdeen Global - American Equity Fund A2 USD Acc" });
            result.Add(new StockListItem { Id = 1531, name = "Aberdeen Global - Asia Pacific Equity Fund A2 Hedged EUR Acc" });
            result.Add(new StockListItem { Id = 1168, name = "Aberdeen Global - Asia Pacific Equity Fund A2 USD Acc" });
            result.Add(new StockListItem { Id = 1437, name = "Aberdeen Global - Asia Pacific Equity Fund E2 EUR Acc" });
            result.Add(new StockListItem { Id = 1260, name = "Aberdeen Global - Asia Pacific Equity Fund S2 USD Acc" });
            result.Add(new StockListItem { Id = 1611, name = "Aberdeen Global - Asian Local Currency Short Duration Bond Fund A2 Hedged EUR Acc" });
            result.Add(new StockListItem { Id = 1169, name = "Aberdeen Global - Asian Local Currency Short Duration Bond Fund A2 USD Acc" });
            result.Add(new StockListItem { Id = 1433, name = "Aberdeen Global - Asian Property Share Fund A2 Hedged EUR Acc" });
            result.Add(new StockListItem { Id = 1431, name = "Aberdeen Global - Asian Property Share Fund A2 USD Acc" });
            result.Add(new StockListItem { Id = 1257, name = "Aberdeen Global - Asian Property Share Fund S2 USD Acc" });
            result.Add(new StockListItem { Id = 1170, name = "Aberdeen Global - Asian Smaller Companies Fund A2 USD Acc" });
            result.Add(new StockListItem { Id = 1171, name = "Aberdeen Global - Australasian Equity Fund A2 AUD Acc" });
            result.Add(new StockListItem { Id = 1623, name = "Aberdeen Global - Brazil Bond Fund A2 USD Acc" });
            result.Add(new StockListItem { Id = 1621, name = "Aberdeen Global - Brazil Equity Fund A2 USD Acc" });
            result.Add(new StockListItem { Id = 1622, name = "Aberdeen Global - Brazil Equity Fund E2 EUR Acc" });
            result.Add(new StockListItem { Id = 1624, name = "Aberdeen Global - Brazil Equity Fund S2 USD Acc" });
            result.Add(new StockListItem { Id = 1172, name = "Aberdeen Global - Chinese Equity Fund A2 USD Acc" });
            result.Add(new StockListItem { Id = 1263, name = "Aberdeen Global - Chinese Equity Fund S2 USD Acc" });
            result.Add(new StockListItem { Id = 1278, name = "Aberdeen Global - Eastern European Equity Fund A2 Acc" });
            result.Add(new StockListItem { Id = 1287, name = "Aberdeen Global - Eastern European Equity Fund S2 Acc" });
            result.Add(new StockListItem { Id = 1445, name = "Aberdeen Global - Emerging Markets Corporate Bond Fund A2 USD Acc" });
            result.Add(new StockListItem { Id = 1441, name = "Aberdeen Global - Emerging Markets Equity Fund A2 Hedged CHF Acc" });
            result.Add(new StockListItem { Id = 1174, name = "Aberdeen Global - Emerging Markets Equity Fund A2 USD Acc" });
            result.Add(new StockListItem { Id = 1438, name = "Aberdeen Global - Emerging Markets Equity Fund E2 EUR Acc" });
            result.Add(new StockListItem { Id = 1261, name = "Aberdeen Global - Emerging Markets Equity Fund S2 USD Acc" });
            result.Add(new StockListItem { Id = 1443, name = "Aberdeen Global - Emerging Markets Infrastructure Equity Fund A2 Hedged CHF Acc" });
            result.Add(new StockListItem { Id = 1442, name = "Aberdeen Global - Emerging Markets Infrastructure Equity Fund A2 Hedged EUR Acc" });
            result.Add(new StockListItem { Id = 1444, name = "Aberdeen Global - Emerging Markets Infrastructure Equity Fund A2 USD Acc" });
            result.Add(new StockListItem { Id = 1411, name = "Aberdeen Global - Emerging Markets Infrastructure Equity Fund S2 Hedged EUR Acc" });
            result.Add(new StockListItem { Id = 1409, name = "Aberdeen Global - Emerging Markets Infrastructure Equity Fund S2 USD Acc" });
            result.Add(new StockListItem { Id = 1505, name = "Aberdeen Global - Emerging Markets Local Currency Bond Fund A2 USD Acc" });
            result.Add(new StockListItem { Id = 1175, name = "Aberdeen Global - Emerging Markets Smaller Companies Fund A2 USD Acc" });
            result.Add(new StockListItem { Id = 1446, name = "Aberdeen Global - Ethical World Equity Fund A2 USD Acc" });
            result.Add(new StockListItem { Id = 1180, name = "Aberdeen Global - European Equity Ex UK Fund A2 EUR Acc" });
            result.Add(new StockListItem { Id = 1179, name = "Aberdeen Global - European Equity Fund A2 EUR Acc" });
            result.Add(new StockListItem { Id = 1262, name = "Aberdeen Global - European Equity Fund S2 EUR Acc" });
            result.Add(new StockListItem { Id = 1272, name = "Aberdeen Global - European Equity Income Fund A2 EUR Acc" });
            result.Add(new StockListItem { Id = 1273, name = "Aberdeen Global - European Equity Income Fund A2 Hedged CHF Acc" });
            result.Add(new StockListItem { Id = 1274, name = "Aberdeen Global - European Equity Income Fund A2 Hedged USD Acc" });
            result.Add(new StockListItem { Id = 1281, name = "Aberdeen Global - European Equity Income Fund S2 EUR Acc" });
            result.Add(new StockListItem { Id = 1181, name = "Aberdeen Global - Indian Equity Fund A2 USD Acc" });
            result.Add(new StockListItem { Id = 1434, name = "Aberdeen Global - Japanese Equity Fund A2 Hedged CHF Acc" });
            result.Add(new StockListItem { Id = 1435, name = "Aberdeen Global - Japanese Equity Fund A2 Hedged EUR Acc" });
            result.Add(new StockListItem { Id = 1182, name = "Aberdeen Global - Japanese Equity Fund A2 JPY Acc" });
            result.Add(new StockListItem { Id = 1265, name = "Aberdeen Global - Japanese Equity Fund S2 Hedged CHF Acc" });
            result.Add(new StockListItem { Id = 1266, name = "Aberdeen Global - Japanese Equity Fund S2 Hedged EUR Acc" });
            result.Add(new StockListItem { Id = 1264, name = "Aberdeen Global - Japanese Equity Fund S2 JPY Acc" });
            result.Add(new StockListItem { Id = 1436, name = "Aberdeen Global - Japanese Smaller Companies Fund A2 Hedged EUR Acc" });
            result.Add(new StockListItem { Id = 1183, name = "Aberdeen Global - Japanese Smaller Companies Fund A2 JPY Acc" });
            result.Add(new StockListItem { Id = 1267, name = "Aberdeen Global - Japanese Smaller Companies Fund S2 JPY Acc" });
            result.Add(new StockListItem { Id = 1447, name = "Aberdeen Global - Latin American Equity Fund A2 Hedged EUR Acc" });
            result.Add(new StockListItem { Id = 1256, name = "Aberdeen Global - Latin American Equity Fund A2 USD Acc" });
            result.Add(new StockListItem { Id = 1432, name = "Aberdeen Global - Latin American Equity Fund E2 EUR Acc" });
            result.Add(new StockListItem { Id = 1259, name = "Aberdeen Global - Latin American Equity Fund S2 USD Acc" });
            result.Add(new StockListItem { Id = 1532, name = "Aberdeen Global - Multi-Manager World Equity Fund A2 EUR Acc" });
            result.Add(new StockListItem { Id = 1184, name = "Aberdeen Global - Responsible World Equity Fund A2 USD Acc" });
            result.Add(new StockListItem { Id = 1279, name = "Aberdeen Global - Russian Equity Fund A2 EUR Acc" });
            result.Add(new StockListItem { Id = 1288, name = "Aberdeen Global - Russian Equity Fund S2 EUR Acc" });
            result.Add(new StockListItem { Id = 1310, name = "Aberdeen Global - Select Emerging Markets Bond Fund A2 Hedged CHF Acc" });
            result.Add(new StockListItem { Id = 1309, name = "Aberdeen Global - Select Emerging Markets Bond Fund A2 Hedged EUR Acc" });
            result.Add(new StockListItem { Id = 1173, name = "Aberdeen Global - Select Emerging Markets Bond Fund A2 USD Acc" });
            result.Add(new StockListItem { Id = 1176, name = "Aberdeen Global - Select Euro High Yield Bond Fund A2 EUR Acc" });
            result.Add(new StockListItem { Id = 1178, name = "Aberdeen Global - Select Euro High Yield Bond Fund A2 Hedged GBP Acc" });
            result.Add(new StockListItem { Id = 1177, name = "Aberdeen Global - Select Euro High Yield Bond Fund A2 Hedged USD Acc" });
            result.Add(new StockListItem { Id = 1506, name = "Aberdeen Global - Select Global Credit Bond Fund A2 Hedged USD Acc" });
            result.Add(new StockListItem { Id = 1185, name = "Aberdeen Global - Select Sterling Financials Bond Fund A2 GBP Acc" });
            result.Add(new StockListItem { Id = 1186, name = "Aberdeen Global - Technology Equity Fund A2 USD Acc" });
            result.Add(new StockListItem { Id = 1270, name = "Aberdeen Global - Technology Equity Fund S2 USD Acc" });
            result.Add(new StockListItem { Id = 1187, name = "Aberdeen Global - UK Equity Fund A2 GBP Acc" });
            result.Add(new StockListItem { Id = 1188, name = "Aberdeen Global - World Equity Fund A2 USD Acc" });
            result.Add(new StockListItem { Id = 1439, name = "Aberdeen Global - World Equity Fund E2 EUR Acc" });
            result.Add(new StockListItem { Id = 1276, name = "Aberdeen Global - World Resources Equity Fund A2 Hedged CHF Acc" });
            result.Add(new StockListItem { Id = 1277, name = "Aberdeen Global - World Resources Equity Fund A2 Hedged EUR Acc" });
            result.Add(new StockListItem { Id = 1275, name = "Aberdeen Global - World Resources Equity Fund A2 USD Acc" });
            result.Add(new StockListItem { Id = 1440, name = "Aberdeen Global - World Resources Equity Fund E2 EUR Acc" });
            result.Add(new StockListItem { Id = 1286, name = "Aberdeen Global - World Resources Equity Fund S2 Hedged EUR Acc" });
            result.Add(new StockListItem { Id = 1284, name = "Aberdeen Global - World Resources Equity Fund S2 USD Acc" });
            result.Add(new StockListItem { Id = 1322, name = "Aberdeen Global II - Asia Pacific Multi Asset Fund A2 Hedged CHF Acc" });
            result.Add(new StockListItem { Id = 1323, name = "Aberdeen Global II - Asia Pacific Multi Asset Fund A2 Hedged EUR Acc" });
            result.Add(new StockListItem { Id = 1321, name = "Aberdeen Global II - Asia Pacific Multi Asset Fund A2 USD Acc" });
            result.Add(new StockListItem { Id = 1313, name = "Aberdeen Global II - Asian Bond Fund A2 Hedged EUR Acc" });
            result.Add(new StockListItem { Id = 1312, name = "Aberdeen Global II - Asian Bond Fund A2 USD Acc" });
            result.Add(new StockListItem { Id = 1315, name = "Aberdeen Global II - Australian Dollar Bond Fund A2 AUD Acc" });
            result.Add(new StockListItem { Id = 1316, name = "Aberdeen Global II - Canadian Dollar Bond Fund A2 CAD Acc" });
            result.Add(new StockListItem { Id = 1318, name = "Aberdeen Global II - Emerging Europe Bond Fund A2 EUR Acc" });
            result.Add(new StockListItem { Id = 1320, name = "Aberdeen Global II - Euro Government Bond Fund A2 EUR Acc" });
            result.Add(new StockListItem { Id = 1314, name = "Aberdeen Global II - Euro High Yield Bond Fund A2 EUR Acc" });
            result.Add(new StockListItem { Id = 1324, name = "Aberdeen Global II - Euro Short Term Bond Fund A2 EUR Acc" });
            result.Add(new StockListItem { Id = 1317, name = "Aberdeen Global II - European Convertibles Bond Fund A2 EUR Acc" });
            result.Add(new StockListItem { Id = 1308, name = "Aberdeen Global II - Sterling Bond Fund A2 GBP Acc" });
            result.Add(new StockListItem { Id = 1319, name = "Aberdeen Global II - US Dollar Bond Fund A2 USD Acc" });
            result.Add(new StockListItem { Id = 1311, name = "Aberdeen Global II - US Dollar Short Term Bond Fund A2 USD Acc" });
            result.Add(new StockListItem { Id = 2456, name = "Accorde Abacus Alap" });
            result.Add(new StockListItem { Id = 2797, name = "Accorde Abszolút Hozamú Kötvény Alapok Alapja A sorozat" });
            result.Add(new StockListItem { Id = 2799, name = "Accorde Abszolút Hozamú Kötvény Alapok Alapja B sorozat" });
            result.Add(new StockListItem { Id = 2460, name = "Accorde CVK2 Alapok Alapja A sorozat" });
            result.Add(new StockListItem { Id = 2459, name = "Accorde CVK2 Alapok Alapja B sorozat" });
            result.Add(new StockListItem { Id = 2547, name = "Accorde CVK2 Alapok Alapja C sorozat" });
            result.Add(new StockListItem { Id = 2461, name = "Accorde CVK3 Alapok Alapja A sorozat" });
            result.Add(new StockListItem { Id = 2462, name = "Accorde CVK3 Alapok Alapja B sorozat" });
            result.Add(new StockListItem { Id = 2548, name = "Accorde CVK3 Alapok Alapja C sorozat" });
            result.Add(new StockListItem { Id = 2702, name = "Accorde Első Román Részvényalap A sorozat" });
            result.Add(new StockListItem { Id = 2703, name = "Accorde Első Román Részvényalap B sorozat" });
            result.Add(new StockListItem { Id = 2704, name = "Accorde Első Román Részvényalap I sorozat" });
            result.Add(new StockListItem { Id = 2458, name = "Accorde Global Alap" });
            result.Add(new StockListItem { Id = 2598, name = "Accorde Omega Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 2457, name = "Accorde Prizma Alap" });
            result.Add(new StockListItem { Id = 2454, name = "Accorde Prémium Alapok Alapja A sorozat" });
            result.Add(new StockListItem { Id = 2453, name = "Accorde Prémium Alapok Alapja B sorozat" });
            result.Add(new StockListItem { Id = 2455, name = "Accorde Prémium Alapok Alapja C sorozat" });
            result.Add(new StockListItem { Id = 2597, name = "Accorde Resources Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 2475, name = "Accorde Selection Részvény Alap" });
            result.Add(new StockListItem { Id = 1363, name = "Aegon BondMaxx Abszolút Hozamú Kötvény Befektetési Alap A sorozat" });
            result.Add(new StockListItem { Id = 2605, name = "Aegon BondMaxx Abszolút Hozamú Kötvény Befektetési Alap C sorozat" });
            result.Add(new StockListItem { Id = 1359, name = "Aegon BondMaxx Abszolút Hozamú Kötvény Befektetési Alap I sorozat" });
            result.Add(new StockListItem { Id = 1702, name = "Aegon BondMaxx Abszolút Hozamú Kötvény Befektetési Alap P sorozat" });
            result.Add(new StockListItem { Id = 1690, name = "Aegon BondMaxx Abszolút Hozamú Kötvény Befektetési Alap R sorozat" });
            result.Add(new StockListItem { Id = 914, name = "Aegon Feltörekvő Európa Kötvény Befektetési Alap A sorozat" });
            result.Add(new StockListItem { Id = 2643, name = "Aegon Feltörekvő Európa Kötvény Befektetési Alap I sorozat" });
            result.Add(new StockListItem { Id = 2644, name = "Aegon Feltörekvő Európa Kötvény Befektetési Alap U sorozat" });
            result.Add(new StockListItem { Id = 2340, name = "Aegon Maraton Aktív Vegyes Befektetési Alap A sorozat HUF" });
            result.Add(new StockListItem { Id = 2471, name = "Aegon Maraton Aktív Vegyes Befektetési Alap C sorozat" });
            result.Add(new StockListItem { Id = 2366, name = "Aegon Maraton Aktív Vegyes Befektetési Alap E sorozat EUR" });
            result.Add(new StockListItem { Id = 2338, name = "Aegon Maraton Aktív Vegyes Befektetési Alap I sorozat HUF" });
            result.Add(new StockListItem { Id = 2368, name = "Aegon Maraton Aktív Vegyes Befektetési Alap P sorozat PLN" });
            result.Add(new StockListItem { Id = 2369, name = "Aegon Maraton Aktív Vegyes Befektetési Alap R sorozat HUF" });
            result.Add(new StockListItem { Id = 2367, name = "Aegon Maraton Aktív Vegyes Befektetési Alap U sorozat USD" });
            result.Add(new StockListItem { Id = 2477, name = "Aegon Prémium Dynamic Alapokba Fektető Részalap" });
            result.Add(new StockListItem { Id = 2478, name = "Aegon Prémium Everest Alapokba Fektető Részalap" });
            result.Add(new StockListItem { Id = 2476, name = "Aegon Prémium Expert Alapokba Fektető Részalap" });
            result.Add(new StockListItem { Id = 2262, name = "Aegon Tempó Allegro 10 Alapokba Fektető Részalap" });
            result.Add(new StockListItem { Id = 2260, name = "Aegon Tempó Allegro 8 Alapokba Fektető Részalap" });
            result.Add(new StockListItem { Id = 2261, name = "Aegon Tempó Allegro 9 Alapokba Fektető Részalap" });
            result.Add(new StockListItem { Id = 2253, name = "Aegon Tempó Andante 1 Alapokba Fektető Részalap" });
            result.Add(new StockListItem { Id = 2254, name = "Aegon Tempó Andante 2 Alapokba Fektető Részalap" });
            result.Add(new StockListItem { Id = 2255, name = "Aegon Tempó Andante 3 Alapokba Fektető Részalap" });
            result.Add(new StockListItem { Id = 2256, name = "Aegon Tempó Moderato 4 Alapokba Fektető Részalap" });
            result.Add(new StockListItem { Id = 2257, name = "Aegon Tempó Moderato 5 Alapokba Fektető Részalap" });
            result.Add(new StockListItem { Id = 2258, name = "Aegon Tempó Moderato 6 Alapokba Fektető Részalap" });
            result.Add(new StockListItem { Id = 2259, name = "Aegon Tempó Moderato 7 Alapokba Fektető Részalap" });
            result.Add(new StockListItem { Id = 2031, name = "Allianz Euro High Yield Bond" });
            result.Add(new StockListItem { Id = 2041, name = "Allianz Global EcoTrends - AT - EUR" });
            result.Add(new StockListItem { Id = 1217, name = "Allianz Indexkövető Részvény Befektetési Alap" });
            result.Add(new StockListItem { Id = 1199, name = "Allianz Kötvény Befektetési Alap" });
            result.Add(new StockListItem { Id = 1974, name = "Allianz PIMCO Euro Bond - AT EUR" });
            result.Add(new StockListItem { Id = 1067, name = "Allianz Pénzpiaci Befektetési Alap" });
            result.Add(new StockListItem { Id = 1967, name = "Allianz RCM Asia Pacific - CT EUR" });
            result.Add(new StockListItem { Id = 1966, name = "Allianz RCM Best Styles Euroland - CT EUR" });
            result.Add(new StockListItem { Id = 1971, name = "Allianz RCM Bric Equity - CT EUR" });
            result.Add(new StockListItem { Id = 1970, name = "Allianz RCM Euroland Equity Growth - CT EUR" });
            result.Add(new StockListItem { Id = 1969, name = "Allianz RCM Europe Equity Growth - CT EUR" });
            result.Add(new StockListItem { Id = 2004, name = "Allianz RCM European Equity Dividend- CT EUR" });
            result.Add(new StockListItem { Id = 1975, name = "Allianz RCM Global Agricultural Trends - AT USD" });
            result.Add(new StockListItem { Id = 2005, name = "Allianz RCM Global Agricultural Trends- CT EUR" });
            result.Add(new StockListItem { Id = 1968, name = "Allianz RCM Global EcoTrends - CT EUR" });
            result.Add(new StockListItem { Id = 1973, name = "Allianz RCM Global Equity - AT USD" });
            result.Add(new StockListItem { Id = 1965, name = "Allianz RCM Global Equity - CT EUR" });
            result.Add(new StockListItem { Id = 1976, name = "Allianz RCM Growing Markets Protect AT EUR" });
            result.Add(new StockListItem { Id = 1972, name = "Allianz RCM US Equity - AT USD" });
            result.Add(new StockListItem { Id = 1964, name = "Allianz RCM US Equity - CT EUR" });
            result.Add(new StockListItem { Id = 1327, name = "Allianz Stabilitás Gold Befektetési Alap" });
            result.Add(new StockListItem { Id = 301, name = "Alpok Nyíltvégű Ingatlan Alapok Alapja" });
            result.Add(new StockListItem { Id = 471, name = "Alpok Nyíltvégű Tőke- és Hozamvédett Befektetési Alap" });
            result.Add(new StockListItem { Id = 754, name = "Alpok Válogatott Alap" });
            result.Add(new StockListItem { Id = 1561, name = "Alpok Árfolyamvédett Nyíltvégű Kötvény Alap" });
            result.Add(new StockListItem { Id = 696, name = "Amundi Aranysárkány Ázsiai Alapok Alapja" });
            result.Add(new StockListItem { Id = 1459, name = "Amundi Aranysárkány Ázsiai Alapok Alapja I Sorozat" });
            result.Add(new StockListItem { Id = 1463, name = "Amundi Horizont 2020 Alap A Sorozat" });
            result.Add(new StockListItem { Id = 1508, name = "Amundi Horizont 2020 Alap K Sorozat" });
            result.Add(new StockListItem { Id = 1464, name = "Amundi Horizont 2025 Alap A Sorozat" });
            result.Add(new StockListItem { Id = 1465, name = "Amundi Horizont 2030 Alap A Sorozat" });
            result.Add(new StockListItem { Id = 2638, name = "Amundi IDEA Alap A sorozat" });
            result.Add(new StockListItem { Id = 2639, name = "Amundi IDEA Alap B sorozat" });
            result.Add(new StockListItem { Id = 2234, name = "Amundi Konzervatív Vegyes Alap A Sorozat" });
            result.Add(new StockListItem { Id = 2283, name = "Amundi Konzervatív Vegyes Alap I sorozat" });
            result.Add(new StockListItem { Id = 33, name = "Amundi Közép-Európai Részvény Alap" });
            result.Add(new StockListItem { Id = 1218, name = "Amundi Közép-európai Részvény Alap I Sorozat" });
            result.Add(new StockListItem { Id = 35, name = "Amundi Magyar Indexkövető Részvény Alap" });
            result.Add(new StockListItem { Id = 1563, name = "Amundi Magyar Indexkövető Részvény Alap I sorozat" });
            result.Add(new StockListItem { Id = 32, name = "Amundi Magyar Kötvény Alap A sorozat" });
            result.Add(new StockListItem { Id = 1207, name = "Amundi Magyar Kötvény Alap I sorozat" });
            result.Add(new StockListItem { Id = 34, name = "Amundi Magyar Pénzpiaci Alap" });
            result.Add(new StockListItem { Id = 2210, name = "Amundi Magyar Pénzpiaci Alap C Sorozat C Sorozat" });
            result.Add(new StockListItem { Id = 1208, name = "Amundi Magyar Pénzpiaci Alap I Sorozat" });
            result.Add(new StockListItem { Id = 2608, name = "Amundi My Portfolio Alapok Alapja" });
            result.Add(new StockListItem { Id = 36, name = "Amundi Nemzetközi Vegyes Alapk Alapja - D sorozat" });
            result.Add(new StockListItem { Id = 1114, name = "Amundi Nemzetközi Vegyes Alapok Alapja A Sorozat" });
            result.Add(new StockListItem { Id = 1610, name = "Amundi Regatta Abszolút Hozamú Alap A sorozat" });
            result.Add(new StockListItem { Id = 2751, name = "Amundi Regatta Abszolút Hozamú Alap C sorozat" });
            result.Add(new StockListItem { Id = 1768, name = "Amundi Regatta Abszolút Hozamú Alap I Sorozat I Sorozat" });
            result.Add(new StockListItem { Id = 1772, name = "Amundi Rövid Kötvény Alap A Sorozat A Sorozat" });
            result.Add(new StockListItem { Id = 1773, name = "Amundi Rövid Kötvény Alap H Sorozat H sorozat" });
            result.Add(new StockListItem { Id = 2827, name = "Amundi Rövid Kötvény Alap I Sorozat" });
            result.Add(new StockListItem { Id = 37, name = "Amundi Selecta Európai Részvény Alapok Alapja" });
            result.Add(new StockListItem { Id = 1364, name = "Amundi Selecta Európai Részvény Alapok Alapja I sorozat" });
            result.Add(new StockListItem { Id = 31, name = "Amundi USA Devizarészvény Alapok Alapja" });
            result.Add(new StockListItem { Id = 1365, name = "Amundi USA Devizarészvény Alapok Alapja I sorozat" });
            result.Add(new StockListItem { Id = 1062, name = "BF Money Balancovany Alap" });
            result.Add(new StockListItem { Id = 883, name = "BF Money Chráneny Nyíltvégű Befektetési Alap" });
            result.Add(new StockListItem { Id = 1047, name = "BF Money EMEA Nyíltvégű Részvény Befektetési Alap" });
            result.Add(new StockListItem { Id = 1412, name = "BF Money EMEA Részvény Alap CZK sorozat" });
            result.Add(new StockListItem { Id = 1424, name = "BF Money EMEA Részvény Alap HUF sorozat" });
            result.Add(new StockListItem { Id = 1738, name = "BF Money EMEA Részvény Alap U sorozat U sorozat" });
            result.Add(new StockListItem { Id = 27, name = "BF Money Fejlett Piaci Részvény Alap" });
            result.Add(new StockListItem { Id = 2248, name = "BF Money Fejlett Piaci Részvény Alap U sorozat" });
            result.Add(new StockListItem { Id = 2394, name = "BF Money Fejlett Piaci Részvény Alap USD sorozat" });
            result.Add(new StockListItem { Id = 1737, name = "BF Money Feltörekvő Kötvény Alap U sorozat U sorozat" });
            result.Add(new StockListItem { Id = 1415, name = "BF Money Feltörekvő Piaci DevizaKötvény Alap CZK sorozat" });
            result.Add(new StockListItem { Id = 1567, name = "BF Money Feltörekvő Piaci DevizaKötvény Alap USD sorozat" });
            result.Add(new StockListItem { Id = 1237, name = "BF Money Feltörekvő Piaci Devizakötvény Alap" });
            result.Add(new StockListItem { Id = 1238, name = "BF Money Feltörekvő Piaci Részvény Alap" });
            result.Add(new StockListItem { Id = 1414, name = "BF Money Feltörekvő Piaci Részvény Alap CZK sorozat" });
            result.Add(new StockListItem { Id = 1731, name = "BF Money Feltörekvő Részvény Alap U sorozat U sorozat" });
            result.Add(new StockListItem { Id = 1353, name = "BF Money Konzervatini Fond" });
            result.Add(new StockListItem { Id = 1413, name = "BF Money Közép-Európai Részvény Alap CZK sorozat" });
            result.Add(new StockListItem { Id = 935, name = "BF Money Közép-Európai Részvény Alap EUR sorozat" });
            result.Add(new StockListItem { Id = 2626, name = "BF Money Közép-Európai Részvény Alap I sorozat" });
            result.Add(new StockListItem { Id = 1730, name = "BF Money Közép-Európai Részvény Alap U sorozat U sorozat" });
            result.Add(new StockListItem { Id = 28, name = "BF Money Közép-európai Részvény Alap" });
            result.Add(new StockListItem { Id = 1728, name = "BF Money Nyersanyag Alap U sorozat U sorozat" });
            result.Add(new StockListItem { Id = 484, name = "BF Money Nyersanyag Alapok Alapja" });
            result.Add(new StockListItem { Id = 2114, name = "BGF Asia Pacific EQ INC FD E2 EUR" });
            result.Add(new StockListItem { Id = 2115, name = "BGF Asian Tiger Bond E2 EUR" });
            result.Add(new StockListItem { Id = 2116, name = "BGF Asian Tiger Bond E2 USD" });
            result.Add(new StockListItem { Id = 2117, name = "BGF Continental European Flex E2 EUR" });
            result.Add(new StockListItem { Id = 2118, name = "BGF Emerging Markets Bond E2 EUR" });
            result.Add(new StockListItem { Id = 2119, name = "BGF Emerging Markets Bond E2 USD" });
            result.Add(new StockListItem { Id = 2120, name = "BGF Emerging Markets E2 EUR" });
            result.Add(new StockListItem { Id = 2144, name = "BGF Emerging Markets E2 USD" });
            result.Add(new StockListItem { Id = 2121, name = "BGF Euro Bond E2 EUR" });
            result.Add(new StockListItem { Id = 2122, name = "BGF Euro Corporate Bond E2 EUR" });
            result.Add(new StockListItem { Id = 2145, name = "BGF Euro Short Duration Bond E2 EUR" });
            result.Add(new StockListItem { Id = 2146, name = "BGF European Focus E2 EUR" });
            result.Add(new StockListItem { Id = 2123, name = "BGF European Special Situations Fund E2 EUR" });
            result.Add(new StockListItem { Id = 2124, name = "BGF European Special Situations Fund E2 USD" });
            result.Add(new StockListItem { Id = 2125, name = "BGF Flexible Multi-Asset E2 EUR" });
            result.Add(new StockListItem { Id = 2131, name = "BGF Global  Equity E2 EUR" });
            result.Add(new StockListItem { Id = 2132, name = "BGF Global  Equity E2 USD" });
            result.Add(new StockListItem { Id = 2126, name = "BGF Global Allocation E2 EUR" });
            result.Add(new StockListItem { Id = 2127, name = "BGF Global Allocation E2 USD" });
            result.Add(new StockListItem { Id = 2128, name = "BGF Global Corporate Bond E2 USD" });
            result.Add(new StockListItem { Id = 2129, name = "BGF Global Dynamic Equity EUR E2" });
            result.Add(new StockListItem { Id = 2130, name = "BGF Global Dynamic Equity USD E2" });
            result.Add(new StockListItem { Id = 2134, name = "BGF Global High Yield Bond E2 EUR" });
            result.Add(new StockListItem { Id = 2133, name = "BGF Global High Yield Bond E2 USD" });
            result.Add(new StockListItem { Id = 2147, name = "BGF Global High Yield Bond Hedged E2 EUR" });
            result.Add(new StockListItem { Id = 2135, name = "BGF Global Opportunities E2 EUR" });
            result.Add(new StockListItem { Id = 2136, name = "BGF Global Opportunities E2 USD" });
            result.Add(new StockListItem { Id = 2148, name = "BGF Latin American E2 USD" });
            result.Add(new StockListItem { Id = 2137, name = "BGF Local EMG MKTS Short Dur Bond E2 EUR" });
            result.Add(new StockListItem { Id = 2149, name = "BGF Local EMG MKTS Short Dur Bond E2 USD" });
            result.Add(new StockListItem { Id = 2138, name = "BGF Renminbi Bond E2 EUR" });
            result.Add(new StockListItem { Id = 2151, name = "BGF US Flexible Equity E2 USD" });
            result.Add(new StockListItem { Id = 2140, name = "BGF US Growth E2 EUR" });
            result.Add(new StockListItem { Id = 2141, name = "BGF US Growth E2 USD" });
            result.Add(new StockListItem { Id = 2139, name = "BGF USD High Yield Bond E2 EUR" });
            result.Add(new StockListItem { Id = 2150, name = "BGF USD High Yield Bond E2 USD" });
            result.Add(new StockListItem { Id = 2142, name = "BGF World Bond E2 EUR" });
            result.Add(new StockListItem { Id = 2143, name = "BGF World Bond E2 USD" });
            result.Add(new StockListItem { Id = 2152, name = "BGF World Energy E2 USD" });
            result.Add(new StockListItem { Id = 2153, name = "BGF World Mining A2 USD" });
            result.Add(new StockListItem { Id = 2154, name = "BGF World Mining E2 USD" });
            result.Add(new StockListItem { Id = 174, name = "BLB Convertible Bond (AL)" });
            result.Add(new StockListItem { Id = 198, name = "BLB Convertible Bond (TNL)" });
            result.Add(new StockListItem { Id = 231, name = "BLB Corporate Bond (AL)" });
            result.Add(new StockListItem { Id = 176, name = "BLB Corporate Bond (TL)" });
            result.Add(new StockListItem { Id = 177, name = "BLB Corporate Bond (TNL)" });
            result.Add(new StockListItem { Id = 185, name = "BLB Short Term" });
            result.Add(new StockListItem { Id = 2214, name = "BNP Paribas L1 Equity World Quality Focus" });
            result.Add(new StockListItem { Id = 2215, name = "BNP Paribas L1 Equity World Quality Focus Classic" });
            result.Add(new StockListItem { Id = 2038, name = "BNPP L1 Obam Equity World HUF" });
            result.Add(new StockListItem { Id = 739, name = "BNPPL1 EQUITY TURKEY" });
            result.Add(new StockListItem { Id = 2204, name = "BSF Americas Div Eq Abs Return H A2 EUR" });
            result.Add(new StockListItem { Id = 2741, name = "Biggeorge 10. Ingatlanfejlesztő Ingatlanbefektetési Alap A sorozat" });
            result.Add(new StockListItem { Id = 2755, name = "Biggeorge 11. Ingatlanfejlesztő Ingatlanbefektetési Alap A sorozat" });
            result.Add(new StockListItem { Id = 908, name = "Biggeorge 4. Ingatlanfejlesztő Ingatlanbefektető Alap" });
            result.Add(new StockListItem { Id = 2610, name = "Biggeorge 5 Ingatlanfejlesztő Ingatlanbefektetési Alap A sorozat" });
            result.Add(new StockListItem { Id = 2714, name = "Biggeorge 7. Ingatlanfejlesztő Ingatlanbefektetési Alap A sorozat" });
            result.Add(new StockListItem { Id = 2737, name = "Biggeorge 8. Ingatlanfejlesztő Ingatlanbefektetési Alap A sorozat" });
            result.Add(new StockListItem { Id = 925, name = "Budapest 2016 Alapok Alapja" });
            result.Add(new StockListItem { Id = 42, name = "Budapest Abszolút Hozam Származtatott Alap" });
            result.Add(new StockListItem { Id = 1491, name = "Budapest Abszolút Kötvény Alapok Alapja" });
            result.Add(new StockListItem { Id = 1336, name = "Budapest Arany Alapok Alapja" });
            result.Add(new StockListItem { Id = 1735, name = "Budapest Arany Alapok Alapja U sorozat U sorozat" });
            result.Add(new StockListItem { Id = 29, name = "Budapest Befektetési Kártya Alap" });
            result.Add(new StockListItem { Id = 25, name = "Budapest Bonitas Alap" });
            result.Add(new StockListItem { Id = 142, name = "Budapest Bonitas Plus Alap" });
            result.Add(new StockListItem { Id = 1211, name = "Budapest Bonitas Plus Alap D sorozat" });
            result.Add(new StockListItem { Id = 1633, name = "Budapest Dollár Rövid Kötvény Alap" });
            result.Add(new StockListItem { Id = 1729, name = "Budapest Dollár Rövid Kötvény Alap U sorozat" });
            result.Add(new StockListItem { Id = 2219, name = "Budapest Egyensúly Alap" });
            result.Add(new StockListItem { Id = 2391, name = "Budapest Egyensúly Alap I sorozat" });
            result.Add(new StockListItem { Id = 26, name = "Budapest Euró Rövid Kötvény Alap" });
            result.Add(new StockListItem { Id = 936, name = "Budapest Euró Rövid Kötvény Alap E sorozat" });
            result.Add(new StockListItem { Id = 2631, name = "Budapest Euró Rövid Kötvény Alap I sorozat" });
            result.Add(new StockListItem { Id = 1482, name = "Budapest Franklin Templeton Selections Alapok Alapja" });
            result.Add(new StockListItem { Id = 1733, name = "Budapest Global100 Plusz Alap" });
            result.Add(new StockListItem { Id = 1732, name = "Budapest Global90 Plusz Alap" });
            result.Add(new StockListItem { Id = 149, name = "Budapest Ingatlan Alapok Alapja" });
            result.Add(new StockListItem { Id = 1716, name = "Budapest Ingatlan Alapok Alapja IL sorozat" });
            result.Add(new StockListItem { Id = 2405, name = "Budapest Kontroll Abszolút Hozam Származtatott Alap I sorozat" });
            result.Add(new StockListItem { Id = 1739, name = "Budapest Kontroll Alap U sorozat U sorozat" });
            result.Add(new StockListItem { Id = 24, name = "Budapest Kötvény Alap" });
            result.Add(new StockListItem { Id = 1736, name = "Budapest Kötvény Alap U sorozat U sorozat" });
            result.Add(new StockListItem { Id = 1882, name = "Budapest Paradigma Alap" });
            result.Add(new StockListItem { Id = 2392, name = "Budapest Paradigma Alap USD" });
            result.Add(new StockListItem { Id = 2390, name = "Budapest Paradigma Plusz Alap" });
            result.Add(new StockListItem { Id = 2614, name = "Budapest Paradigma Plusz Alap I Sorozat" });
            result.Add(new StockListItem { Id = 2546, name = "Budapest Prémium Dinamikus Részalap A" });
            result.Add(new StockListItem { Id = 2545, name = "Budapest Prémium Dinamikus Részalap A sorozat" });
            result.Add(new StockListItem { Id = 2544, name = "Budapest Prémium Konzervatív Részalap A sorozat" });
            result.Add(new StockListItem { Id = 2389, name = "Budapest Prémium Portfólió Alapok Alap" });
            result.Add(new StockListItem { Id = 2543, name = "Budapest Prémium Progresszív Részalap A sorozat" });
            result.Add(new StockListItem { Id = 1694, name = "Budapest US100 Plusz Hozamvédett Alap" });
            result.Add(new StockListItem { Id = 1722, name = "Budapest US95 Plusz" });
            result.Add(new StockListItem { Id = 1698, name = "Budapest US95 Plusz Alap" });
            result.Add(new StockListItem { Id = 23, name = "Budapest Állampapír Alap" });
            result.Add(new StockListItem { Id = 2399, name = "Budapest Állampapír Befektetési Alap I sorozat" });
            result.Add(new StockListItem { Id = 2712, name = "Budapest Állampapír Befektetési Alap U sorozat" });
            result.Add(new StockListItem { Id = 1427, name = "CIB Algoritmus Alapok Alapja" });
            result.Add(new StockListItem { Id = 2795, name = "CIB Arany Alapok Alapja" });
            result.Add(new StockListItem { Id = 2331, name = "CIB Autógyártók Tőkevédett Származtatott Alapja" });
            result.Add(new StockListItem { Id = 2717, name = "CIB Babatermékgyártók Származtatott Alapja" });
            result.Add(new StockListItem { Id = 2279, name = "CIB Balance Vegyes Alapok Alapja" });
            result.Add(new StockListItem { Id = 2303, name = "CIB Biztos Pont Tőkevédett Származtatott Alap" });
            result.Add(new StockListItem { Id = 2601, name = "CIB Dollár Megtakarítási Alap" });
            result.Add(new StockListItem { Id = 302, name = "CIB Euro Pénzpiaci Alap" });
            result.Add(new StockListItem { Id = 2332, name = "CIB Euró Autógyártók Tőkevédett Származtatott Alapja" });
            result.Add(new StockListItem { Id = 2719, name = "CIB Euró Babatermékgyártók Származtatott Alapja" });
            result.Add(new StockListItem { Id = 2334, name = "CIB Euró Balance Vegyes Alapok Alapja" });
            result.Add(new StockListItem { Id = 1686, name = "CIB Euró Gyógyszergyártók Tőkevédett Származtatott Alapja" });
            result.Add(new StockListItem { Id = 1687, name = "CIB Euró Gyógyszergyártók Tőkevédett Származtatott Alapja EUR" });
            result.Add(new StockListItem { Id = 2278, name = "CIB Euró Luxusmárkák 2 Tőkevédett Származtatott Alapja" });
            result.Add(new StockListItem { Id = 2813, name = "CIB Euró Reflex Vegyes Alapok Alapja" });
            result.Add(new StockListItem { Id = 2354, name = "CIB Euró Relax Vegyes Alap" });
            result.Add(new StockListItem { Id = 2735, name = "CIB Euró Talentum Total Return Alapok Alapja" });
            result.Add(new StockListItem { Id = 2633, name = "CIB Euró Tiszta Amerika 2 Származtatott Alap" });
            result.Add(new StockListItem { Id = 2374, name = "CIB Euró Világmárkák Származtatott Alapja" });
            result.Add(new StockListItem { Id = 2350, name = "CIB Euró WebWilág 2 Származtatott Alap" });
            result.Add(new StockListItem { Id = 2591, name = "CIB Euró Ázsiai Részvény Származtatott Alap" });
            result.Add(new StockListItem { Id = 40, name = "CIB Fejlett Részvénypiaci Alapok Alapja" });
            result.Add(new StockListItem { Id = 2791, name = "CIB Fejlett Részvénypiaci Alapok Alapja HUF-I" });
            result.Add(new StockListItem { Id = 955, name = "CIB Feltörekvő Részvénypiaci Alapok Alapja" });
            result.Add(new StockListItem { Id = 243, name = "CIB Hozamgarantált Betét Alap" });
            result.Add(new StockListItem { Id = 215, name = "CIB Indexkövetö Részvény Alap" });
            result.Add(new StockListItem { Id = 38, name = "CIB Kincsem Kötvény Alap" });
            result.Add(new StockListItem { Id = 2313, name = "CIB Kötvény Plusz Vegyes Alap" });
            result.Add(new StockListItem { Id = 39, name = "CIB Közép-európai Részvény Alap" });
            result.Add(new StockListItem { Id = 2793, name = "CIB Közép-európai Részvény Alap HUF-I" });
            result.Add(new StockListItem { Id = 2277, name = "CIB Luxusmárkák 2 Tőkevédett Származtatott Alapja" });
            result.Add(new StockListItem { Id = 461, name = "CIB Nyersanyag Alapok Alapja" });
            result.Add(new StockListItem { Id = 2771, name = "CIB Olajvállalatok Származtatott Alapja" });
            result.Add(new StockListItem { Id = 41, name = "CIB Pénzpiaci Alap" });
            result.Add(new StockListItem { Id = 2433, name = "CIB Reflex Vegyes Alapok Alapja" });
            result.Add(new StockListItem { Id = 2353, name = "CIB Relax Vegyes Alap" });
            result.Add(new StockListItem { Id = 2451, name = "CIB Stabil Európa 2 Származtatott Alap" });
            result.Add(new StockListItem { Id = 2421, name = "CIB Szabadidő Származtatott Alap" });
            result.Add(new StockListItem { Id = 2697, name = "CIB Talentum Total Return Alapok Alapja" });
            result.Add(new StockListItem { Id = 2632, name = "CIB Tiszta Amerika 2 Származtatott Alap" });
            result.Add(new StockListItem { Id = 2373, name = "CIB Világmárkák Származtatott Alapja" });
            result.Add(new StockListItem { Id = 2349, name = "CIB WebWilág 2 Származtatott Alap" });
            result.Add(new StockListItem { Id = 2590, name = "CIB Ázsiai Részvény Származtatott Alap" });
            result.Add(new StockListItem { Id = 2749, name = "CIB Édességek Származtatott Alapja" });
            result.Add(new StockListItem { Id = 1674, name = "CONCORDE HOZAMKERESŐ EURÓPAI SZÁRMAZTATOTT RÉSZVÉNY BEFEKTETÉSI ALAP " });
            result.Add(new StockListItem { Id = 2322, name = "CREDIT SUISSE (LUX) GLOBAL PRESTIGE EQUITY FUND B EUR" });
            result.Add(new StockListItem { Id = 2321, name = "CREDIT SUISSE (LUX) GLOBAL PRESTIGE EQUITY FUND BH USD" });
            result.Add(new StockListItem { Id = 1680, name = "CS Equity Fund (Lux) USA Value R EUR" });
            result.Add(new StockListItem { Id = 1614, name = "CSF (Lux) Commodity Index Plus (US$) R EUR" });
            result.Add(new StockListItem { Id = 1612, name = "CSF (Lux) Target Volatility (Euro) R CHF" });
            result.Add(new StockListItem { Id = 1613, name = "CSF (Lux) Target Volatility (Euro) R USD" });
            result.Add(new StockListItem { Id = 202, name = "Capitol Nyíltvégű Ingatlan Befektetési Alap" });
            result.Add(new StockListItem { Id = 1149, name = "Citadella Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 2642, name = "Citadella Származtatott Befektetési Alap B sorozat" });
            result.Add(new StockListItem { Id = 43, name = "Concorde 2000 Nyíltvégű Befektetési Alap" });
            result.Add(new StockListItem { Id = 2398, name = "Concorde 3000 Nyíltvégű Befektetési Alap" });
            result.Add(new StockListItem { Id = 864, name = "Concorde Columbus Globális Értékalapú Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 2388, name = "Concorde Euro PB2 Alapok Alapja" });
            result.Add(new StockListItem { Id = 897, name = "Concorde Euro Pénzpiaci Alap" });
            result.Add(new StockListItem { Id = 2629, name = "Concorde Forte EUR Alapokba Fekteto Részalap PRÉM sorozat" });
            result.Add(new StockListItem { Id = 2480, name = "Concorde Forte HUF Alapokba Fektető Részalap PRÉM sorozat" });
            result.Add(new StockListItem { Id = 1423, name = "Concorde Hold Alapok Alapja" });
            result.Add(new StockListItem { Id = 1678, name = "Concorde Hold Euro Alapok Alapja" });
            result.Add(new StockListItem { Id = 1745, name = "Concorde KOGA Alapok Alapja" });
            result.Add(new StockListItem { Id = 2309, name = "Concorde KOGA Euro Alapok Alapja" });
            result.Add(new StockListItem { Id = 46, name = "Concorde Kötvény Befektetési Alap" });
            result.Add(new StockListItem { Id = 926, name = "Concorde Közép Európai Részvény Alap" });
            result.Add(new StockListItem { Id = 2542, name = "Concorde Max Euro Származtatott Nyíltvégű Befektetési Alap" });
            result.Add(new StockListItem { Id = 2541, name = "Concorde Max USD Származtatott Nyíltvégű Befektetési Alap" });
            result.Add(new StockListItem { Id = 2640, name = "Concorde Mezzo EUR Alapokba Fekteto Részalap PRÉM sorozat" });
            result.Add(new StockListItem { Id = 2481, name = "Concorde Mezzo HUF Alapokba Fektető Részalap PRÉM sorozat" });
            result.Add(new StockListItem { Id = 2483, name = "Concorde Molto Forte EUR Alapokba Fektető Részalap PVK sorozat" });
            result.Add(new StockListItem { Id = 2628, name = "Concorde Molto Forte HUF Alapokba Fekteto Részalap PVK sorozat" });
            result.Add(new StockListItem { Id = 134, name = "Concorde Nemzetközi Részvény Alapok Alapja" });
            result.Add(new StockListItem { Id = 517, name = "Concorde PB1 Alapok Alapja" });
            result.Add(new StockListItem { Id = 518, name = "Concorde PB2 Alapok Alapja" });
            result.Add(new StockListItem { Id = 519, name = "Concorde PB3 Alapok Alapja" });
            result.Add(new StockListItem { Id = 2630, name = "Concorde Piano EUR Alapokba Fekteto Részalap PRÉM sorozat" });
            result.Add(new StockListItem { Id = 2482, name = "Concorde Piano HUF Alapokba Fektető Részalap PRÉM sorozat" });
            result.Add(new StockListItem { Id = 45, name = "Concorde Pénzpiaci Befektetési Alap" });
            result.Add(new StockListItem { Id = 1094, name = "Concorde Rubicon Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 47, name = "Concorde Részvény Befektetési Alap" });
            result.Add(new StockListItem { Id = 186, name = "Concorde Rövid Futamidejű Kötvény Befektetési Alap" });
            result.Add(new StockListItem { Id = 2387, name = "Concorde USD PB2 Alapok Alapja" });
            result.Add(new StockListItem { Id = 2357, name = "Concorde USD PB3 Alapok Alapja" });
            result.Add(new StockListItem { Id = 889, name = "Concorde USD Pénzpiaci Alap" });
            result.Add(new StockListItem { Id = 304, name = "Concorde VM Abszolút Származtatott Alap" });
            result.Add(new StockListItem { Id = 2641, name = "Concorde VM Abszolút Származtatott Alap B sorozat" });
            result.Add(new StockListItem { Id = 2757, name = "Concorde VM Abszolút Származtatott Alap C sorozat" });
            result.Add(new StockListItem { Id = 1246, name = "Concorde-VM Euro Alapok Alapja" });
            result.Add(new StockListItem { Id = 509, name = "Credit Suisse Bond Fund (Lux) High Yield USD B" });
            result.Add(new StockListItem { Id = 820, name = "Credit Suisse Bond Fund (Lux) Inflation Linked (Euro) A" });
            result.Add(new StockListItem { Id = 291, name = "Credit Suisse Bond Fund (Lux) Inflation Linked (Euro) B**" });
            result.Add(new StockListItem { Id = 821, name = "Credit Suisse Bond Fund (Lux) Inflation Linked (Sfr) A" });
            result.Add(new StockListItem { Id = 822, name = "Credit Suisse Bond Fund (Lux) Inflation Linked (Sfr) B" });
            result.Add(new StockListItem { Id = 823, name = "Credit Suisse Bond Fund (Lux) Inflation Linked (US$) A" });
            result.Add(new StockListItem { Id = 292, name = "Credit Suisse Bond Fund (Lux) Inflation Linked USD B*" });
            result.Add(new StockListItem { Id = 824, name = "Credit Suisse Bond Fund (Lux) Sfr A" });
            result.Add(new StockListItem { Id = 825, name = "Credit Suisse Bond Fund (Lux) Sfr B" });
            result.Add(new StockListItem { Id = 828, name = "Credit Suisse Bond Fund (Lux) Short-Term Sfr A" });
            result.Add(new StockListItem { Id = 829, name = "Credit Suisse Bond Fund (Lux) Short-Term Sfr B" });
            result.Add(new StockListItem { Id = 836, name = "Credit Suisse Bond Fund (Lux) TOPS (Euro) A" });
            result.Add(new StockListItem { Id = 837, name = "Credit Suisse Bond Fund (Lux) TOPS (Sfr) A" });
            result.Add(new StockListItem { Id = 838, name = "Credit Suisse Bond Fund (Lux) TOPS (Sfr) B" });
            result.Add(new StockListItem { Id = 839, name = "Credit Suisse Bond Fund (Lux) TOPS (US$) A" });
            result.Add(new StockListItem { Id = 293, name = "Credit Suisse Bond Fund (Lux) TOPS Euro B**" });
            result.Add(new StockListItem { Id = 294, name = "Credit Suisse Bond Fund (Lux) TOPS USD B**" });
            result.Add(new StockListItem { Id = 358, name = "Credit Suisse Equity Fund (Lux) European Property B**" });
            result.Add(new StockListItem { Id = 498, name = "Credit Suisse Equity Fund (Lux) Global Prestige B" });
            result.Add(new StockListItem { Id = 759, name = "Credit Suisse Equity Fund (Lux) Global Prestige R USD" });
            result.Add(new StockListItem { Id = 643, name = "Credit Suisse Equity Fund (Lux) Italy B" });
            result.Add(new StockListItem { Id = 650, name = "Credit Suisse Equity Fund (Lux) SC Germany B" });
            result.Add(new StockListItem { Id = 361, name = "Credit Suisse Equity Fund (Lux) Small Cap Europe B**" });
            result.Add(new StockListItem { Id = 138, name = "Credit Suisse Equity Fund (Lux) USA" });
            result.Add(new StockListItem { Id = 661, name = "Credit Suisse Equity Fund (Lux) USA R EUR" });
            result.Add(new StockListItem { Id = 662, name = "Credit Suisse Equity Fund (Lux) USA Value B" });
            result.Add(new StockListItem { Id = 364, name = "Credit Suisse Equity Fund (Lux) World EUR**" });
            result.Add(new StockListItem { Id = 663, name = "Credit Suisse Equity Fund (Lux) World I EUR" });
            result.Add(new StockListItem { Id = 664, name = "Credit Suisse Equity Fund (Lux) World R CHF" });
            result.Add(new StockListItem { Id = 564, name = "Credit Suisse Equity Fund (Lux) World R USD" });
            result.Add(new StockListItem { Id = 593, name = "Credit Suisse Fund (Lux) DJ-AIG Commodity Index Plus (Sfr) B" });
            result.Add(new StockListItem { Id = 594, name = "Credit Suisse Fund (Lux) DJ-AIG Commodity Index Plus (US$) B" });
            result.Add(new StockListItem { Id = 1303, name = "Credit Suisse Fund (Lux) Money Market Sfr B" });
            result.Add(new StockListItem { Id = 596, name = "Credit Suisse Fund (Lux) Relative Return Engineered (Euro) B" });
            result.Add(new StockListItem { Id = 601, name = "Credit Suisse Fund (Lux) Total Return Global (Euro) B" });
            result.Add(new StockListItem { Id = 571, name = "Credit Suisse Money Market Fund (Lux) Can$ B" });
            result.Add(new StockListItem { Id = 573, name = "Credit Suisse Money Market Fund (Lux) Euro B" });
            result.Add(new StockListItem { Id = 575, name = "Credit Suisse Money Market Fund (Lux) Euro I" });
            result.Add(new StockListItem { Id = 569, name = "Credit Suisse Money Market Fund (Lux) GBP B" });
            result.Add(new StockListItem { Id = 577, name = "Credit Suisse Money Market Fund (Lux) US$ B" });
            result.Add(new StockListItem { Id = 579, name = "Credit Suisse Money Market Fund (Lux) US$ I" });
            result.Add(new StockListItem { Id = 1023, name = "Credit Suisse SICAV One (Lux) Equity Global Property B" });
            result.Add(new StockListItem { Id = 1024, name = "Credit Suisse SICAV One (Lux) Equity Global Property R CHF" });
            result.Add(new StockListItem { Id = 1025, name = "Credit Suisse SICAV One (Lux) Equity Global Property R EUR" });
            result.Add(new StockListItem { Id = 1681, name = "Credit Suisse SICAV One (Lux) Equity Global Security -B- USD" });
            result.Add(new StockListItem { Id = 1683, name = "Credit Suisse SICAV One (Lux) Equity Global Security -R - EUR" });
            result.Add(new StockListItem { Id = 1682, name = "Credit Suisse SICAV One (Lux) Equity Global Security -R- CHF" });
            result.Add(new StockListItem { Id = 1408, name = "Credit Suisse SICAV One (Lux) European Equity Dividend Plus B" });
            result.Add(new StockListItem { Id = 996, name = "Credit Suisse Solutions (Lux) CS Tremont AllHedge Index B" });
            result.Add(new StockListItem { Id = 997, name = "Credit Suisse Solutions (Lux) CS Tremont AllHedge Index R CHF" });
            result.Add(new StockListItem { Id = 998, name = "Credit Suisse Solutions (Lux) CS Tremont AllHedge Index R EUR" });
            result.Add(new StockListItem { Id = 1405, name = "Credit Suisse Solutions (Lux) Prima Multi-Strategy B EUR" });
            result.Add(new StockListItem { Id = 1407, name = "Credit Suisse Solutions (Lux) Prima Multi-Strategy R CHF" });
            result.Add(new StockListItem { Id = 1406, name = "Credit Suisse Solutions (Lux) Prima Multi-Strategy R USD" });
            result.Add(new StockListItem { Id = 953, name = "DIALÓG Konvergencia Részvény Befektetési Alap" });
            result.Add(new StockListItem { Id = 2432, name = "DIALÓG Konzervatív EURÓ Befektetési Alap A sorozat" });
            result.Add(new StockListItem { Id = 954, name = "DIALÓG Likviditási Befektetési Alap" });
            result.Add(new StockListItem { Id = 2314, name = "DIALÓG Likviditási Befektetési Alap I sorozat" });
            result.Add(new StockListItem { Id = 2306, name = "DIALÓG Octopus Származtatott Befektetési Alap EUR sorozat" });
            result.Add(new StockListItem { Id = 2307, name = "DIALÓG USD Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 946, name = "DSK Pénzpiaci Alap (bolgár)" });
            result.Add(new StockListItem { Id = 1236, name = "Dialóg EURÓ Származtatott Deviza Befektetési Alap" });
            result.Add(new StockListItem { Id = 952, name = "Dialóg Expander Részvény Alap" });
            result.Add(new StockListItem { Id = 1304, name = "Dialóg Octopus Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 1123, name = "Dialóg Származtatott Deviza Befektetési Alap" });
            result.Add(new StockListItem { Id = 2426, name = "Diófa Ingatlan Befektetési Alap A sorozat" });
            result.Add(new StockListItem { Id = 1551, name = "Diófa Ingatlan Befektetési Alap I Sorozat" });
            result.Add(new StockListItem { Id = 2346, name = "Diófa Optimus I Befektetési Alap A sorozat" });
            result.Add(new StockListItem { Id = 2344, name = "Diófa Optimus I Befektetési Alap I sorozat" });
            result.Add(new StockListItem { Id = 2347, name = "Diófa Optimus II Befektetési Alap A sorozat" });
            result.Add(new StockListItem { Id = 2345, name = "Diófa Optimus II Befektetési Alap I sorozat" });
            result.Add(new StockListItem { Id = 2238, name = "Diófa WM-1 Befektetési Részalap" });
            result.Add(new StockListItem { Id = 2239, name = "Diófa WM-2 Befektetési Részalap" });
            result.Add(new StockListItem { Id = 2240, name = "Diófa WM-3 Befektetési Részalap" });
            result.Add(new StockListItem { Id = 2422, name = "EQUILOR Dinamikus Portfolió Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 2325, name = "EQUILOR Közép-európai Részvény Befektetési Alap" });
            result.Add(new StockListItem { Id = 2333, name = "EQUILOR Magnus EUR Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 2326, name = "EQUILOR Noé Nemzetközi Részvény Befektetési Alap" });
            result.Add(new StockListItem { Id = 2411, name = "EQUILOR Optimus Befektetési Alapba Fektető Alap" });
            result.Add(new StockListItem { Id = 1646, name = "EQUILOR Private Wealth Management Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 1229, name = "ERSTE Abszolút Hozamú Eszközallokációs Alapok Alapja" });
            result.Add(new StockListItem { Id = 1227, name = "ERSTE DPM Globális Részvény Alapok Alapja" });
            result.Add(new StockListItem { Id = 1228, name = "ERSTE Európai Részvény Befektetési Alap" });
            result.Add(new StockListItem { Id = 693, name = "ERSTE Korvett Kötvény Alapok Alapja" });
            result.Add(new StockListItem { Id = 303, name = "ERSTE Likviditási Alap" });
            result.Add(new StockListItem { Id = 1488, name = "ERSTE Nyíltvégű Abszolút Hozamú Kötvény Befektetési Alap" });
            result.Add(new StockListItem { Id = 2823, name = "ERSTE Nyíltvégű Abszolút Hozamú Kötvény Befektetési Alap D sorozat" });
            result.Add(new StockListItem { Id = 890, name = "ERSTE Nyíltvégű Dollár Pénzpiaci Befektetési Alap" });
            result.Add(new StockListItem { Id = 1202, name = "ERSTE Nyíltvégű Euro Ingatlan Befektetési Alap" });
            result.Add(new StockListItem { Id = 199, name = "ERSTE Nyíltvégű Ingatlan Befektetési Alap" });
            result.Add(new StockListItem { Id = 9, name = "ERSTE Nyíltvégű Közép-Európai Részvény Befektetési Alap" });
            result.Add(new StockListItem { Id = 8, name = "ERSTE Nyíltvégű Pénzpiaci Befektetési Alap" });
            result.Add(new StockListItem { Id = 1552, name = "ERSTE Nyíltvégű Tartós Árfolyamvédett Kötvény Alap" });
            result.Add(new StockListItem { Id = 2821, name = "ERSTE Nyíltvégű XL Kötvény Befektetési Alap D sorozat" });
            result.Add(new StockListItem { Id = 1118, name = "ERSTE XL Kötvény Befektetési Alap" });
            result.Add(new StockListItem { Id = 346, name = "ESPA BOND DANUBIA (VT)" });
            result.Add(new StockListItem { Id = 340, name = "ESPA BOND EMERGING-MARKETS (VT)" });
            result.Add(new StockListItem { Id = 347, name = "ESPA BOND EURO-CORPORATE (VT)" });
            result.Add(new StockListItem { Id = 342, name = "ESPA BOND EUROPE (VT)" });
            result.Add(new StockListItem { Id = 343, name = "ESPA BOND INTERNATIONAL (VT)" });
            result.Add(new StockListItem { Id = 425, name = "ESPA Bond Danubia (VT)" });
            result.Add(new StockListItem { Id = 348, name = "ESPA Bond Dollar (VT)**" });
            result.Add(new StockListItem { Id = 349, name = "ESPA Bond Dollar-Corporate (VT)**" });
            result.Add(new StockListItem { Id = 2108, name = "ESPA Bond Emerging Markets Corporate" });
            result.Add(new StockListItem { Id = 421, name = "ESPA Bond Emerging-Markets (VT)" });
            result.Add(new StockListItem { Id = 2112, name = "ESPA Bond Euro-Rent" });
            result.Add(new StockListItem { Id = 2109, name = "ESPA Bond Europe-High Yield" });
            result.Add(new StockListItem { Id = 2110, name = "ESPA Bond Local Emerging" });
            result.Add(new StockListItem { Id = 2111, name = "ESPA Bond USA-High Yield" });
            result.Add(new StockListItem { Id = 493, name = "ESPA CASH EMERGING-MARKETS (VT) EUR" });
            result.Add(new StockListItem { Id = 341, name = "ESPA CASH EURO-PLUS (VT)" });
            result.Add(new StockListItem { Id = 475, name = "ESPA Cash Corporate-Plus (T)**" });
            result.Add(new StockListItem { Id = 481, name = "ESPA Cash Corporate-Plus (VT)**" });
            result.Add(new StockListItem { Id = 510, name = "ESPA Cash Emerging-Markets (VT) 2" });
            result.Add(new StockListItem { Id = 2385, name = "ESPA PORTFOLIO BOND EUROPE" });
            result.Add(new StockListItem { Id = 2263, name = "ESPA STOCK BIOTEC EUR" });
            result.Add(new StockListItem { Id = 2264, name = "ESPA STOCK BIOTEC HUF" });
            result.Add(new StockListItem { Id = 494, name = "ESPA STOCK BRICK (VT)" });
            result.Add(new StockListItem { Id = 1051, name = "ESPA STOCK EUROPE-ACTIVE -EUR" });
            result.Add(new StockListItem { Id = 1050, name = "ESPA STOCK EUROPE-ACTIVE-HUF" });
            result.Add(new StockListItem { Id = 345, name = "ESPA STOCK EUROPE-EMERGING (VT)" });
            result.Add(new StockListItem { Id = 1977, name = "ESPA Stock Adriatic EUR" });
            result.Add(new StockListItem { Id = 1978, name = "ESPA Stock Adriatic HUF" });
            result.Add(new StockListItem { Id = 1979, name = "ESPA Stock Agriculture  EUR" });
            result.Add(new StockListItem { Id = 1980, name = "ESPA Stock Agriculture  HUF" });
            result.Add(new StockListItem { Id = 289, name = "ESPA Stock America (VT)" });
            result.Add(new StockListItem { Id = 286, name = "ESPA Stock America Alap" });
            result.Add(new StockListItem { Id = 1981, name = "ESPA Stock Asia-Infrastructure EUR" });
            result.Add(new StockListItem { Id = 1982, name = "ESPA Stock Asia-Infrastructure HUF" });
            result.Add(new StockListItem { Id = 511, name = "ESPA Stock Brick (VT) 2" });
            result.Add(new StockListItem { Id = 1983, name = "ESPA Stock Commodities EUR" });
            result.Add(new StockListItem { Id = 1984, name = "ESPA Stock Commodtities HUF" });
            result.Add(new StockListItem { Id = 476, name = "ESPA Stock Europe-Property (VT)**" });
            result.Add(new StockListItem { Id = 287, name = "ESPA Stock Global (VT)" });
            result.Add(new StockListItem { Id = 478, name = "ESPA Stock Global-Emerging Markets (VT)" });
            result.Add(new StockListItem { Id = 477, name = "ESPA Stock Global-Emerging Markets (VT)**" });
            result.Add(new StockListItem { Id = 1985, name = "ESPA Stock Istanbul EUR" });
            result.Add(new StockListItem { Id = 1986, name = "ESPA Stock Istanbul HUF" });
            result.Add(new StockListItem { Id = 426, name = "ESPA Stock Japan (VT)" });
            result.Add(new StockListItem { Id = 350, name = "ESPA Stock Japan (VT)**" });
            result.Add(new StockListItem { Id = 1987, name = "ESPA Stock Pharma EUR" });
            result.Add(new StockListItem { Id = 1988, name = "ESPA Stock Pharma HUF" });
            result.Add(new StockListItem { Id = 1989, name = "ESPA Stock Russia EUR" });
            result.Add(new StockListItem { Id = 1990, name = "ESPA Stock Russia HUF" });
            result.Add(new StockListItem { Id = 1991, name = "ESPA Stock Techno EUR" });
            result.Add(new StockListItem { Id = 1992, name = "ESPA Stock Techno HUF" });
            result.Add(new StockListItem { Id = 1645, name = "Equilor Afrika Zártkörű Befektetési Alap" });
            result.Add(new StockListItem { Id = 1647, name = "Equilor Fregatt Prémium Kötvény Befektetési Alap" });
            result.Add(new StockListItem { Id = 1648, name = "Equilor Pillars Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 1649, name = "Equilor Primus Alapok Alapja" });
            result.Add(new StockListItem { Id = 1697, name = "Erste Bond Emerging Markets Corporate HUF Alapok Alapja" });
            result.Add(new StockListItem { Id = 480, name = "Erste Globál Aktív 10 Alapok Alapja" });
            result.Add(new StockListItem { Id = 1417, name = "Erste Globál Aktív 30 Alapok Alapja" });
            result.Add(new StockListItem { Id = 1416, name = "Erste Globál Aktív 50 Alapok Alapja" });
            result.Add(new StockListItem { Id = 416, name = "Erste Hazai Indexkövető Részvény Alap" });
            result.Add(new StockListItem { Id = 1196, name = "Erste Kamatoptimum Nyíltvégű Befektetési Alap" });
            result.Add(new StockListItem { Id = 479, name = "Erste Megtakarítási Alapok Alapja" });
            result.Add(new StockListItem { Id = 692, name = "Erste Multistrategy Abszolút Hozamú Alapok Alapja" });
            result.Add(new StockListItem { Id = 694, name = "Erste Nyíltvégű DPM Alternatív Alapok Alapja" });
            result.Add(new StockListItem { Id = 1636, name = "Erste Nyíltvégű DPM Nemzetközi Kötvény Alapok Alapja" });
            result.Add(new StockListItem { Id = 2599, name = "Erste Nyíltvégű Dollár Ingatlan Befektetési Alapok Alapja" });
            result.Add(new StockListItem { Id = 891, name = "Erste Nyíltvégű Euro Pénzpiaci Befektetési Alap" });
            result.Add(new StockListItem { Id = 2600, name = "Erste Nyíltvégű Hazai Dollár Kötvény Befektetési Alap" });
            result.Add(new StockListItem { Id = 2825, name = "Erste Nyíltvégű Hazai Dollár Kötvény Befektetési Alap D sorozat" });
            result.Add(new StockListItem { Id = 7, name = "Erste Nyíltvégű Rövid Kötvény Befektetési Alap" });
            result.Add(new StockListItem { Id = 470, name = "Erste Nyíltvégű Tőke- és Hozamvédett Befektetési Alap" });
            result.Add(new StockListItem { Id = 1696, name = "Erste Stock Global HUF Alapok Alapja" });
            result.Add(new StockListItem { Id = 1993, name = "Erste WWF Stock Umwelt EUR" });
            result.Add(new StockListItem { Id = 1994, name = "Erste WWF Stock Umwelt HUF" });
            result.Add(new StockListItem { Id = 2008, name = "Eurizon EasyFund Bond Corporate EUR Short Term R" });
            result.Add(new StockListItem { Id = 2007, name = "Eurizon EasyFund Bond EUR Long Term R" });
            result.Add(new StockListItem { Id = 2006, name = "Eurizon EasyFund Bond EUR Medium Term R" });
            result.Add(new StockListItem { Id = 2013, name = "Eurizon EasyFund Bond EUR Short Term R" });
            result.Add(new StockListItem { Id = 2010, name = "Eurizon EasyFund Bond Emerging Markets R" });
            result.Add(new StockListItem { Id = 2011, name = "Eurizon EasyFund Bond High Yield R" });
            result.Add(new StockListItem { Id = 2009, name = "Eurizon EasyFund Bond JPY R" });
            result.Add(new StockListItem { Id = 2012, name = "Eurizon EasyFund Cash EUR R" });
            result.Add(new StockListItem { Id = 2001, name = "Eurizon EasyFund Equity Emerging Markets Asia R" });
            result.Add(new StockListItem { Id = 1999, name = "Eurizon EasyFund Equity Energy &amp; Materials R" });
            result.Add(new StockListItem { Id = 1998, name = "Eurizon EasyFund Equity Financial R" });
            result.Add(new StockListItem { Id = 1996, name = "Eurizon EasyFund Equity High Tech R" });
            result.Add(new StockListItem { Id = 2000, name = "Eurizon EasyFund Equity Industrials R" });
            result.Add(new StockListItem { Id = 1995, name = "Eurizon EasyFund Equity Italy R" });
            result.Add(new StockListItem { Id = 1997, name = "Eurizon EasyFund Equity Latin America R" });
            result.Add(new StockListItem { Id = 200292 - 05, name = "Eurizon EasyFund Equity Small Cap Europe R" });
            result.Add(new StockListItem { Id = 1136, name = "Európa Ingatlanbefektetési Alap" });
            result.Add(new StockListItem { Id = 2155, name = "FF - Asean Fund A-Acc-Usd" });
            result.Add(new StockListItem { Id = 2317, name = "FRANKLIN K2 ALTERNATIVE STRATEGIES FUND N (ACC) EUR H1" });
            result.Add(new StockListItem { Id = 2318, name = "FRANKLIN K2 ALTERNATIVE STRATEGIES FUND N (ACC) HUF H1" });
            result.Add(new StockListItem { Id = 2319, name = "FRANKLIN K2 ALTERNATIVE STRATEGIES FUND N (ACC) USD" });
            result.Add(new StockListItem { Id = 1213, name = "FUNDQUEST INTERNATIONAL TARGET RETURN CONSERVATIVE (EURO)" });
            result.Add(new StockListItem { Id = 2320, name = "FWR Titánium Euró Alapok Alapja" });
            result.Add(new StockListItem { Id = 939, name = "FirstFund Intézményi Ingatlanbefektetési Alap A sorozat" });
            result.Add(new StockListItem { Id = 1917, name = "Franklin Asian Flex Cap Fund N USD" });
            result.Add(new StockListItem { Id = 1918, name = "Franklin Biotechnology Discovery Fund N" });
            result.Add(new StockListItem { Id = 1908, name = "Franklin Euro Government Bond Fund N" });
            result.Add(new StockListItem { Id = 1924, name = "Franklin Euro High Yield Fund N" });
            result.Add(new StockListItem { Id = 1919, name = "Franklin European Growth Fund N" });
            result.Add(new StockListItem { Id = 1920, name = "Franklin European Small-Mid Cap Growth Fund N EUR" });
            result.Add(new StockListItem { Id = 1937, name = "Franklin Global Growth And Value Fund N" });
            result.Add(new StockListItem { Id = 1921, name = "Franklin Global Growth Fund N" });
            result.Add(new StockListItem { Id = 1922, name = "Franklin Global Real Estate (Usd) Fund N" });
            result.Add(new StockListItem { Id = 1923, name = "Franklin Global Small-Mid Cap Growth Fund N" });
            result.Add(new StockListItem { Id = 1925, name = "Franklin High Yield Fund N" });
            result.Add(new StockListItem { Id = 1926, name = "Franklin Income Fund N" });
            result.Add(new StockListItem { Id = 1927, name = "Franklin India Fund N" });
            result.Add(new StockListItem { Id = 1928, name = "Franklin India Fund N EUR" });
            result.Add(new StockListItem { Id = 1938, name = "Franklin Japan Fund N EUR" });
            result.Add(new StockListItem { Id = 1929, name = "Franklin Mutual Beacon Fund N EUR" });
            result.Add(new StockListItem { Id = 1957, name = "Franklin Mutual Beacon Fund N EUR-H1" });
            result.Add(new StockListItem { Id = 1930, name = "Franklin Mutual Beacon Fund N USD" });
            result.Add(new StockListItem { Id = 1931, name = "Franklin Mutual European Fund N EUR" });
            result.Add(new StockListItem { Id = 1932, name = "Franklin Mutual European Fund N USD" });
            result.Add(new StockListItem { Id = 1933, name = "Franklin Mutual Global Discovery Fund N" });
            result.Add(new StockListItem { Id = 1934, name = "Franklin Mutual Global Discovery Fund N EUR" });
            result.Add(new StockListItem { Id = 1956, name = "Franklin Mutual Global Discovery Fund N EUR-H1" });
            result.Add(new StockListItem { Id = 2042, name = "Franklin Natural Resources Fund A (acc) USD" });
            result.Add(new StockListItem { Id = 1935, name = "Franklin Technology Fund N EUR" });
            result.Add(new StockListItem { Id = 1936, name = "Franklin Technology Fund N USD" });
            result.Add(new StockListItem { Id = 1953, name = "Franklin U.S. Dollar Liquid Reserve Fund N" });
            result.Add(new StockListItem { Id = 1939, name = "Franklin U.S. Equity Fund N EUR" });
            result.Add(new StockListItem { Id = 1940, name = "Franklin U.S. Equity Fund N USD" });
            result.Add(new StockListItem { Id = 1941, name = "Franklin U.S. Government Fund N" });
            result.Add(new StockListItem { Id = 1942, name = "Franklin U.S. Opportunities Fund N" });
            result.Add(new StockListItem { Id = 1943, name = "Franklin U.S. Opportunities Fund N  EUR" });
            result.Add(new StockListItem { Id = 1944, name = "Franklin U.S. Small-Mid Cap Growth Fund N" });
            result.Add(new StockListItem { Id = 166, name = "Futura Pénzpiaci Alap B sorozat" });
            result.Add(new StockListItem { Id = 355, name = "Futura Pénzpiaci Alap I sorozat" });
            result.Add(new StockListItem { Id = 54, name = "GENERALI Hazai Kötvény Alap B sorozat" });
            result.Add(new StockListItem { Id = 1331, name = "Generali Amazonas Latin-Amerikai Részvény V/E Befektetési Alap" });
            result.Add(new StockListItem { Id = 164, name = "Generali Arany Oroszlán Nemzetközi Részvény Alap" });
            result.Add(new StockListItem { Id = 1565, name = "Generali Arany Oroszlán Nemzetközi Részvény Alap" });
            result.Add(new StockListItem { Id = 909, name = "Generali Cash Befektetési Alap A sorozat" });
            result.Add(new StockListItem { Id = 167, name = "Generali Cash Befektetési Alap B sorozat" });
            result.Add(new StockListItem { Id = 1028, name = "Generali Fejlődő Piaci Részvény Alapok Alapja" });
            result.Add(new StockListItem { Id = 1332, name = "Generali Főnix Távol-Keleti Részvény V/E Befektetési Alapok Alapja" });
            result.Add(new StockListItem { Id = 1048, name = "Generali Gold Közép-kelet-európai Részvény Alap A sorozat" });
            result.Add(new StockListItem { Id = 1492, name = "Generali Gold Közép-kelet-európai Részvény Alap B sorozat" });
            result.Add(new StockListItem { Id = 1333, name = "Generali Greenergy Abszolút Hozamú Alap" });
            result.Add(new StockListItem { Id = 910, name = "Generali Hazai Kötvény Alap A sorozat" });
            result.Add(new StockListItem { Id = 1334, name = "Generali IC Ázsiai Részvény V/E Befektetési Alapok Alapja" });
            result.Add(new StockListItem { Id = 1026, name = "Generali IPO Abszolút Hozam Alap" });
            result.Add(new StockListItem { Id = 2343, name = "Generali IPO Abszolút Hozam Alap B sorozat" });
            result.Add(new StockListItem { Id = 1027, name = "Generali Infrastrukturális Abszolút Hozamú Alap" });
            result.Add(new StockListItem { Id = 774, name = "Generali Mustang Amerikai Részvény Alap" });
            result.Add(new StockListItem { Id = 1564, name = "Generali Mustang Amerikai Részvény Alap" });
            result.Add(new StockListItem { Id = 1029, name = "Generali Spirit Abszolút Származtatott Alap" });
            result.Add(new StockListItem { Id = 2336, name = "Generali Triumph Euró Abszolút Származtatott Alap" });
            result.Add(new StockListItem { Id = 1530, name = "Gránit Bank Betét Alap" });
            result.Add(new StockListItem { Id = 1800, name = "HFT Származtatott Nyíltvégű Befektetési Alap" });
            result.Add(new StockListItem { Id = 2444, name = "HFT Származtatott Nyíltvégű Befektetési Alap A sorozat" });
            result.Add(new StockListItem { Id = 2443, name = "HFT Származtatott Nyíltvégű Befektetési Alap IL sorozat" });
            result.Add(new StockListItem { Id = 2044, name = "HSBC GIF Korean Equity A Cap" });
            result.Add(new StockListItem { Id = 100, name = "Horizon Nemzetközi Részvény Alap" });
            result.Add(new StockListItem { Id = 844, name = "ING (L) INVEST BANKING &amp; INSURANCE X CAP (USD)" });
            result.Add(new StockListItem { Id = 533, name = "ING (L) INVEST COMPUTER TECHNOLOGIES P" });
            result.Add(new StockListItem { Id = 548, name = "ING (L) INVEST COMPUTER TECHNOLOGIES X" });
            result.Add(new StockListItem { Id = 842, name = "ING (L) INVEST EMERGING EUROPE X CAP (EUR)" });
            result.Add(new StockListItem { Id = 412, name = "ING (L) INVEST ENERGY P" });
            result.Add(new StockListItem { Id = 391, name = "ING (L) INVEST ENERGY X" });
            result.Add(new StockListItem { Id = 402, name = "ING (L) INVEST EURO HIGH DIVIDEND P" });
            result.Add(new StockListItem { Id = 381, name = "ING (L) INVEST EURO HIGH DIVIDEND X" });
            result.Add(new StockListItem { Id = 858, name = "ING (L) INVEST EURO INCOME X CAP (EUR)" });
            result.Add(new StockListItem { Id = 403, name = "ING (L) INVEST EUROPE HIGH DIVIDEND P" });
            result.Add(new StockListItem { Id = 382, name = "ING (L) INVEST EUROPE HIGH DIVIDEND X" });
            result.Add(new StockListItem { Id = 406, name = "ING (L) INVEST EUROPEAN EQUITY P" });
            result.Add(new StockListItem { Id = 385, name = "ING (L) INVEST EUROPEAN EQUITY X" });
            result.Add(new StockListItem { Id = 411, name = "ING (L) INVEST EUROPEAN REAL ESTATE P" });
            result.Add(new StockListItem { Id = 390, name = "ING (L) INVEST EUROPEAN REAL ESTATE X" });
            result.Add(new StockListItem { Id = 846, name = "ING (L) INVEST FOOD &amp; BEVERAGES X CAP (USD)" });
            result.Add(new StockListItem { Id = 379, name = "ING (L) INVEST GLOBAL HIGH DIVIDEND (EUR) X" });
            result.Add(new StockListItem { Id = 380, name = "ING (L) INVEST GLOBAL HIGH DIVIDEND (USD) X" });
            result.Add(new StockListItem { Id = 547, name = "ING (L) INVEST GLOBAL REAL ESTATE X" });
            result.Add(new StockListItem { Id = 409, name = "ING (L) INVEST GREATER CHINA P" });
            result.Add(new StockListItem { Id = 388, name = "ING (L) INVEST GREATER CHINA X" });
            result.Add(new StockListItem { Id = 413, name = "ING (L) INVEST HEALTH CARE P" });
            result.Add(new StockListItem { Id = 392, name = "ING (L) INVEST HEALTH CARE X" });
            result.Add(new StockListItem { Id = 854, name = "ING (L) INVEST INDUSTRIALS X CAP (EUR)" });
            result.Add(new StockListItem { Id = 529, name = "ING (L) INVEST JAPAN P" });
            result.Add(new StockListItem { Id = 545, name = "ING (L) INVEST JAPAN X" });
            result.Add(new StockListItem { Id = 534, name = "ING (L) INVEST LATIN AMERICA P" });
            result.Add(new StockListItem { Id = 543, name = "ING (L) INVEST LATIN AMERICA X" });
            result.Add(new StockListItem { Id = 855, name = "ING (L) INVEST MATERIALS X CAP (USD)" });
            result.Add(new StockListItem { Id = 410, name = "ING (L) INVEST NEW ASIA P" });
            result.Add(new StockListItem { Id = 389, name = "ING (L) INVEST NEW ASIA X" });
            result.Add(new StockListItem { Id = 528, name = "ING (L) INVEST PRESTIGE P" });
            result.Add(new StockListItem { Id = 544, name = "ING (L) INVEST PRESTIGE X" });
            result.Add(new StockListItem { Id = 856, name = "ING (L) INVEST SUSTAINABLE GROWTH X CAP (EUR)" });
            result.Add(new StockListItem { Id = 848, name = "ING (L) INVEST TELECOM X CAP (USD)" });
            result.Add(new StockListItem { Id = 407, name = "ING (L) INVEST US (Enhanced Core Concentrated) P" });
            result.Add(new StockListItem { Id = 386, name = "ING (L) INVEST US (Enhanced Core Concentrated) X" });
            result.Add(new StockListItem { Id = 404, name = "ING (L) INVEST US HIGH DIVIDEND P" });
            result.Add(new StockListItem { Id = 383, name = "ING (L) INVEST US HIGH DIVIDEND X" });
            result.Add(new StockListItem { Id = 532, name = "ING (L) INVEST UTILITIES P" });
            result.Add(new StockListItem { Id = 539, name = "ING (L) INVEST UTILITIES X" });
            result.Add(new StockListItem { Id = 1072, name = "ING (L) Invest Alternative Beta" });
            result.Add(new StockListItem { Id = 841, name = "ING (L) Invest EURO Equity - X Cap EUR" });
            result.Add(new StockListItem { Id = 2040, name = "ING (L) Invest Emerging Market High Dividend Equity fund" });
            result.Add(new StockListItem { Id = 882, name = "ING (L) Invest Emerging Markets High Dividend - X Cap EUR" });
            result.Add(new StockListItem { Id = 1595, name = "ING (L) Invest Emerging Markets High Dividend P EUR Acc" });
            result.Add(new StockListItem { Id = 2023, name = "ING (L) Invest Energy HUF" });
            result.Add(new StockListItem { Id = 1195, name = "ING (L) Invest Europe Opportunities" });
            result.Add(new StockListItem { Id = 1962, name = "ING (L) Invest Food &amp; Beverages EUR" });
            result.Add(new StockListItem { Id = 2030, name = "ING (L) Invest Global High Dividend HUF" });
            result.Add(new StockListItem { Id = 556, name = "ING (L) Invest Global Opportunities - P Cap EUR" });
            result.Add(new StockListItem { Id = 562, name = "ING (L) Invest Global Opportunities - X Cap EUR" });
            result.Add(new StockListItem { Id = 2024, name = "ING (L) Invest Global Opportunities HUF" });
            result.Add(new StockListItem { Id = 2025, name = "ING (L) Invest Greater China HUF" });
            result.Add(new StockListItem { Id = 2034, name = "ING (L) Invest Health Care EUR" });
            result.Add(new StockListItem { Id = 2022, name = "ING (L) Invest Information Technology HUF" });
            result.Add(new StockListItem { Id = 2026, name = "ING (L) Invest Latin America HUF" });
            result.Add(new StockListItem { Id = 2027, name = "ING (L) Invest Materials HUF" });
            result.Add(new StockListItem { Id = 1537, name = "ING (L) Invest Middle East &amp; North Africa P Cap EUR" });
            result.Add(new StockListItem { Id = 1104, name = "ING (L) Invest Middle East &amp; North Africa X CAP EUR" });
            result.Add(new StockListItem { Id = 1963, name = "ING (L) Invest Telecom EUR" });
            result.Add(new StockListItem { Id = 1893, name = "ING (L) Renta Fund Asian Debt Hard Currency" });
            result.Add(new StockListItem { Id = 189291 - 18, name = "ING (L) Renta Fund Dollar" });
            result.Add(new StockListItem { Id = 2020, name = "ING (L) Renta Fund Emerging Market Debt Hard Currency HUF" });
            result.Add(new StockListItem { Id = 1887, name = "ING (L) Renta Fund Emerging Markets Debt Hard Currency EUR" });
            result.Add(new StockListItem { Id = 1888, name = "ING (L) Renta Fund Emerging Markets Debt Local Currency" });
            result.Add(new StockListItem { Id = 1894, name = "ING (L) Renta Fund Euro" });
            result.Add(new StockListItem { Id = 1890, name = "ING (L) Renta Fund Euro Credit" });
            result.Add(new StockListItem { Id = 1891, name = "ING (L) Renta Fund Euromix Bond" });
            result.Add(new StockListItem { Id = 1889, name = "ING (L) Renta Fund Global High Yield EUR Hedged" });
            result.Add(new StockListItem { Id = 2021, name = "ING (L) Renta Fund Global High Yield HUF" });
            result.Add(new StockListItem { Id = 2033, name = "ING (L) Renta Fund US Credit USD" });
            result.Add(new StockListItem { Id = 189591 - 18, name = "ING (L) Renta Fund World EUR" });
            result.Add(new StockListItem { Id = 2032, name = "ING (L) Renta Fund World EUR Hedged" });
            result.Add(new StockListItem { Id = 1605, name = "ING Afrika és Közel-Kelet Részvény P" });
            result.Add(new StockListItem { Id = 1601, name = "ING Alternatív Béta Alap" });
            result.Add(new StockListItem { Id = 1602, name = "ING Alternatív Béta Alap USD" });
            result.Add(new StockListItem { Id = 1603, name = "ING Alternatív Béta Alap X" });
            result.Add(new StockListItem { Id = 1596, name = "ING Bankok és Biztosítók Részvény" });
            result.Add(new StockListItem { Id = 1608, name = "ING Bankok és Biztosítók Részvény P" });
            result.Add(new StockListItem { Id = 1598, name = "ING Energia Részvény" });
            result.Add(new StockListItem { Id = 1583, name = "ING Euro Hozam Részvény" });
            result.Add(new StockListItem { Id = 1585, name = "ING Euro Hozam Részvény" });
            result.Add(new StockListItem { Id = 1584, name = "ING Euro Hozam Részvényalap" });
            result.Add(new StockListItem { Id = 1606, name = "ING Eurozóna Részvény P" });
            result.Add(new StockListItem { Id = 1587, name = "ING Európai Lehetőségek Részvény" });
            result.Add(new StockListItem { Id = 1607, name = "ING Fejlődő Európai Részvény P" });
            result.Add(new StockListItem { Id = 1592, name = "ING Fejlődő Ázsiai Részvény" });
            result.Add(new StockListItem { Id = 1576, name = "ING Fenntartható Növekedés Részvény" });
            result.Add(new StockListItem { Id = 1586, name = "ING Globális Ingatlanrészvény" });
            result.Add(new StockListItem { Id = 1580, name = "ING Globális Magas Osztalékhozamú Részvény" });
            result.Add(new StockListItem { Id = 1582, name = "ING Globális Magas Osztalékhozamú Részvényalap" });
            result.Add(new StockListItem { Id = 1597, name = "ING IT Részvény" });
            result.Add(new StockListItem { Id = 2029, name = "ING International Converging Europe Equity HUF" });
            result.Add(new StockListItem { Id = 1581, name = "ING Ipari Részvény" });
            result.Add(new StockListItem { Id = 1590, name = "ING Japán Részvény" });
            result.Add(new StockListItem { Id = 1589, name = "ING Kínai Részvény" });
            result.Add(new StockListItem { Id = 1591, name = "ING Latin-Amerikai Részvény" });
            result.Add(new StockListItem { Id = 1599, name = "ING Nyersanyagok Részvény" });
            result.Add(new StockListItem { Id = 1609, name = "ING Nyersanyagok Részvény P" });
            result.Add(new StockListItem { Id = 1577, name = "ING Telekom Részvény" });
            result.Add(new StockListItem { Id = 1594, name = "ING USA Magas Osztalékú Részvény" });
            result.Add(new StockListItem { Id = 1593, name = "ING USA Részvény" });
            result.Add(new StockListItem { Id = 1575, name = "ING Élelmiszeripari Részvény" });
            result.Add(new StockListItem { Id = 1578, name = "ING Öt Kontinens Részvény" });
            result.Add(new StockListItem { Id = 1579, name = "ING Öt Kontinens Részvényalap" });
            result.Add(new StockListItem { Id = 1600, name = "ING Új Európa Részvény" });
            result.Add(new StockListItem { Id = 2611, name = "Impact Lakóingatlan Befektetési Alap" });
            result.Add(new StockListItem { Id = 2053, name = "JPM America Large Cap D (acc) - USD" });
            result.Add(new StockListItem { Id = 2054, name = "JPM Brazil Equity D (acc) - USD" });
            result.Add(new StockListItem { Id = 2063, name = "JPM EUROPE EQUITY PLUS FUND D (ACC) - EUR" });
            result.Add(new StockListItem { Id = 2055, name = "JPM Emerging Markets Bond D (acc) - USD" });
            result.Add(new StockListItem { Id = 2056, name = "JPM Emerging Markets Corporate Bond D (acc) - EUR (hedged)" });
            result.Add(new StockListItem { Id = 2057, name = "JPM Emerging Markets Infrastructure Equity D (acc) - USD" });
            result.Add(new StockListItem { Id = 2058, name = "JPM Emerging Markets Local Currency Debt D (acc) - USD" });
            result.Add(new StockListItem { Id = 2059, name = "JPM Emerging Markets Opportunities A (acc) - USD" });
            result.Add(new StockListItem { Id = 2060, name = "JPM Emerging Markets Small Cap D (acc) - USD" });
            result.Add(new StockListItem { Id = 2061, name = "JPM Emerging Middle East Equity D (acc) - USD" });
            result.Add(new StockListItem { Id = 2062, name = "JPM Euro Corporate Bond D (acc) - EUR" });
            result.Add(new StockListItem { Id = 2065, name = "JPM Europe Dynamic D (acc) - EUR" });
            result.Add(new StockListItem { Id = 2066, name = "JPM Europe Focus D (acc) - EUR" });
            result.Add(new StockListItem { Id = 2067, name = "JPM Europe Small Cap D (acc) - EUR" });
            result.Add(new StockListItem { Id = 2068, name = "JPM Europe Strategic Dividend D (acc) - EUR" });
            result.Add(new StockListItem { Id = 2069, name = "JPM Europe Strategic Growth D (acc) - EUR" });
            result.Add(new StockListItem { Id = 2070, name = "JPM Europe Strategic Value D (acc) - EUR" });
            result.Add(new StockListItem { Id = 2071, name = "JPM Europe Technology D (acc) - EUR" });
            result.Add(new StockListItem { Id = 2364, name = "JPM GLOBAL CAPITAL PRESERVATION D (ACC) - EUR (HEDGED)" });
            result.Add(new StockListItem { Id = 2072, name = "JPM Germany Equity D (acc) - EUR" });
            result.Add(new StockListItem { Id = 2073, name = "JPM Global Absolute Return Bond D (acc) - USD" });
            result.Add(new StockListItem { Id = 2074, name = "JPM Global Agriculture A (acc) - USD" });
            result.Add(new StockListItem { Id = 2075, name = "JPM Global Balanced (EUR) D (acc) - EUR" });
            result.Add(new StockListItem { Id = 2076, name = "JPM Global Capital Appreciation D (acc) - EUR" });
            result.Add(new StockListItem { Id = 2078, name = "JPM Global Capital Preservation (USD) D (acc) - USD" });
            result.Add(new StockListItem { Id = 2077, name = "JPM Global Conservative Balanced D (ACC) - EUR" });
            result.Add(new StockListItem { Id = 2079, name = "JPM Global Consumer Trends D (acc) - EUR" });
            result.Add(new StockListItem { Id = 2080, name = "JPM Global Convertibles (EUR) D (acc) - EUR" });
            result.Add(new StockListItem { Id = 2081, name = "JPM Global Corporate Bond D (acc) - EUR (hedged)" });
            result.Add(new StockListItem { Id = 2082, name = "JPM Global Corporate Bond D (acc) - USD" });
            result.Add(new StockListItem { Id = 2083, name = "JPM Global Dividend D (acc) - USD" });
            result.Add(new StockListItem { Id = 2084, name = "JPM Global Dynamic D (acc) - USD" });
            result.Add(new StockListItem { Id = 2085, name = "JPM Global Financials D (acc) - USD" });
            result.Add(new StockListItem { Id = 2086, name = "JPM Global Focus D (acc) - EUR" });
            result.Add(new StockListItem { Id = 2087, name = "JPM Global Focus D (acc) - EUR (hedged)" });
            result.Add(new StockListItem { Id = 2088, name = "JPM Global Healthcare D (acc) - USD" });
            result.Add(new StockListItem { Id = 2089, name = "JPM Global Infrastructure Trends A (acc) - EUR" });
            result.Add(new StockListItem { Id = 2090, name = "JPM Global Merger Arbitrage A (acc) - EUR (hedged)" });
            result.Add(new StockListItem { Id = 2091, name = "JPM Global Merger Arbitrage A (acc) - USD" });
            result.Add(new StockListItem { Id = 2092, name = "JPM Global Mining D (acc) - EUR" });
            result.Add(new StockListItem { Id = 2093, name = "JPM Global Real Estate Securities (USD) D (acc) - USD" });
            result.Add(new StockListItem { Id = 2094, name = "JPM Global Strategic Bond D (acc) - EUR (hedged)" });
            result.Add(new StockListItem { Id = 2156, name = "JPM HIGHBRIDGE US STEEP FUND" });
            result.Add(new StockListItem { Id = 2095, name = "JPM Highbridge Diversified Commodities D (acc) - USD" });
            result.Add(new StockListItem { Id = 2096, name = "JPM Highbridge Europe STEEP D (acc) - EUR" });
            result.Add(new StockListItem { Id = 2097, name = "JPM Highbridge Statistical Market Neutral D (acc) - EUR" });
            result.Add(new StockListItem { Id = 2098, name = "JPM Highbridge US STEEP D (acc) - USD" });
            result.Add(new StockListItem { Id = 2099, name = "JPM Latin America Equity D (acc) - EUR" });
            result.Add(new StockListItem { Id = 2100, name = "JPM Latin America Equity D (acc) - USD" });
            result.Add(new StockListItem { Id = 2064, name = "JPM Turkey Equity Fund D (acc) - EUR" });
            result.Add(new StockListItem { Id = 2101, name = "JPM US Growth D (acc) - EUR (hedged)" });
            result.Add(new StockListItem { Id = 2102, name = "JPM US Growth D (acc) - USD" });
            result.Add(new StockListItem { Id = 2103, name = "JPM US Small Cap Growth D (acc) - USD" });
            result.Add(new StockListItem { Id = 2104, name = "JPM US Smaller Companies D (acc) - USD" });
            result.Add(new StockListItem { Id = 2105, name = "JPM US Technology D (acc) - USD" });
            result.Add(new StockListItem { Id = 2106, name = "JPM US Value D (acc) - EUR (hedged)" });
            result.Add(new StockListItem { Id = 2107, name = "JPM US Value D (acc) - USD" });
            result.Add(new StockListItem { Id = 2046, name = "JPMORGAN Asia Equity Fund A (acc) - USD" });
            result.Add(new StockListItem { Id = 2045, name = "JPMorgan Asean Equity D (acc) - USD" });
            result.Add(new StockListItem { Id = 2047, name = "JPMorgan China D (acc) - USD" });
            result.Add(new StockListItem { Id = 2048, name = "JPMorgan India D (acc) - USD" });
            result.Add(new StockListItem { Id = 2049, name = "JPMorgan Pacific Equity D (acc) - USD" });
            result.Add(new StockListItem { Id = 2050, name = "JPMorgan Pacific Technology D (acc) - USD" });
            result.Add(new StockListItem { Id = 2051, name = "JPMorgan Singapore D (acc) - USD" });
            result.Add(new StockListItem { Id = 2052, name = "JPMorgan Taiwan D (acc) - USD" });
            result.Add(new StockListItem { Id = 2223, name = "K&H  fellendülő Európa tőkevédett nyíltvégű alap " });
            result.Add(new StockListItem { Id = 1684, name = "K&H  kettős kosár tőkevédett nyíltvégű alap" });
            result.Add(new StockListItem { Id = 1693, name = "K&H  plusz technológia nyíltvégű alap " });
            result.Add(new StockListItem { Id = 125, name = "K&H Amerika Alapok Nyíltvégű Befektetési Alapja HUF sorozat" });
            result.Add(new StockListItem { Id = 2554, name = "K&H Amerika Alapok Nyíltvégű Befektetési Alapja USD sorozat" });
            result.Add(new StockListItem { Id = 2301, name = "K&H Amerika-Európa 2 származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2252, name = "K&H Amerika-európa származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 126, name = "K&H Aranykosár Nyíltvégű Befektetési Alap" });
            result.Add(new StockListItem { Id = 1074, name = "K&H Feltörekvő Piaci Alapok Nyíltvégű Befektetési Alapja" });
            result.Add(new StockListItem { Id = 1675, name = "K&H Fix Plusz Világcégek Származtatott Zártvégű Alap" });
            result.Add(new StockListItem { Id = 132, name = "K&H Ingatlanpiaci Alapok Nyíltvégű Értékpapír Befektetési Alapja" });
            result.Add(new StockListItem { Id = 68, name = "K&H Kötvény Nyíltvégű Befektetési Alap" });
            result.Add(new StockListItem { Id = 70, name = "K&H Közép Európai Részvény Nyíltvégű Befektetési Alap" });
            result.Add(new StockListItem { Id = 128, name = "K&H Navigátor Indexkövető Nyíltvégű Befektetési Alap" });
            result.Add(new StockListItem { Id = 1676, name = "K&H Negatív is Pozitív Nyíltvégű Alap" });
            result.Add(new StockListItem { Id = 1190, name = "K&H Nyersanyag Alapok Nyíltvégű Befektetési Alapja HUF" });
            result.Add(new StockListItem { Id = 1191, name = "K&H Nyersanyag Alapok Nyíltvégű Befektetési Alapja USD" });
            result.Add(new StockListItem { Id = 2282, name = "K&H Szikra abszolút hozamú származtatott alap" });
            result.Add(new StockListItem { Id = 2288, name = "K&H Temze származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2287, name = "K&H Temze tőkevédett származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 670, name = "K&H Tőkevédett Dollár Pénzpiaci Nyíltvégű Értékpapír Befektetési Alap" });
            result.Add(new StockListItem { Id = 353, name = "K&H Tőkevédett Euró Pénzpiaci Nyíltvégű Befektetési Alap" });
            result.Add(new StockListItem { Id = 1111, name = "K&H Tőkevédett Forint Pénzpiaci Alap" });
            result.Add(new StockListItem { Id = 69, name = "K&H Tőkevédett Forint Pénzpiaci Nyíltvégű Befektetési Alap" });
            result.Add(new StockListItem { Id = 2330, name = "K&H Tőkevédett Forint Pénzpiaci Nyíltvégű Befektetési Alap F sorozat" });
            result.Add(new StockListItem { Id = 2393, name = "K&H Tőkét Négy Részletben Fizető Nyíltvégű Alap" });
            result.Add(new StockListItem { Id = 2251, name = "K&H Tőkét Négy Részletben Fizető Származtatott" });
            result.Add(new StockListItem { Id = 1204, name = "K&H Unió Alapok Nyíltvégű Befektetési Alapja EUR" });
            result.Add(new StockListItem { Id = 129, name = "K&H Unió Alapok Nyíltvégű Értékpapír Befektetési Alapja" });
            result.Add(new StockListItem { Id = 901, name = "K&H Vagyonvédett Portfolió - december alapok alapja" });
            result.Add(new StockListItem { Id = 217, name = "K&H Válogatott I. Alapok Nyíltvégű Értékpapír Befektetési Alapja" });
            result.Add(new StockListItem { Id = 220, name = "K&H Válogatott II. Alapok Nyíltvégű Értékpapír Befektetési Alapja" });
            result.Add(new StockListItem { Id = 218, name = "K&H Válogatott III. Alapok Nyíltvégű Értékpapír Befektetési Alapja" });
            result.Add(new StockListItem { Id = 219, name = "K&H Válogatott IV. Alap" });
            result.Add(new StockListItem { Id = 2250, name = "K&H autóipari tőkevédett származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2400, name = "K&H dollár rmegtakarítási cél - augusztus nyiltvégű alapok alapja" });
            result.Add(new StockListItem { Id = 2312, name = "K&H erős Amerika származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2401, name = "K&H euro változó portfolió- október nyiltvégű  alapok alapja" });
            result.Add(new StockListItem { Id = 2211, name = "K&H euró megtakarítási cél - április nyíltvégű alapok alapja" });
            result.Add(new StockListItem { Id = 2351, name = "K&H európai körverseny származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 1661, name = "K&H forintsáv tőkevédett származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2396, name = "K&H gyógyszeripari 2 származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2249, name = "K&H hazai deviza kötvény 3 zártvégű forint alap" });
            result.Add(new StockListItem { Id = 1574, name = "K&H hazai euró kötvény zártvégű euró befektetési alap" });
            result.Add(new StockListItem { Id = 1573, name = "K&H hazai euró kötvény zártvégű forint befektetési alap" });
            result.Add(new StockListItem { Id = 2221, name = "K&H hozamlépcső 2 származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 1486, name = "K&H hozamváltó 5 származtatott zártvégű értékpapír befektetési alap" });
            result.Add(new StockListItem { Id = 1559, name = "K&H háromszor fizető emlékező 5 származtatott nyíltvégű értékpapír befektetési alap" });
            result.Add(new StockListItem { Id = 2383, name = "K&H jövő autói származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2225, name = "K&H kettős kosár 3 tőkevédett származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 1662, name = "K&H likviditási elszámoló nyíltvégű alap" });
            result.Add(new StockListItem { Id = 1665, name = "K&H megtakarítási cél - február nyíltvégű alapok alapja" });
            result.Add(new StockListItem { Id = 2377, name = "K&H megtakarítási cél - február nyíltvégű alapok részalapja" });
            result.Add(new StockListItem { Id = 1627, name = "K&H megtakarítási cél - június nyíltvégű alapok alapja" });
            result.Add(new StockListItem { Id = 2378, name = "K&H megtakarítási cél - június nyíltvégű alapok részalapja" });
            result.Add(new StockListItem { Id = 1725, name = "K&H megtakarítási cél - október nyíltvégű alapok alapja" });
            result.Add(new StockListItem { Id = 2379, name = "K&H megtakarítási cél - október nyíltvégű alapok részalapja" });
            result.Add(new StockListItem { Id = 2276, name = "K&H mozdulj! tőkevédett származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 1329, name = "K&H nemzetközi vegyes alapok nyíltvégű  befektetési alapja EUR sorozat" });
            result.Add(new StockListItem { Id = 2618, name = "K&H nemzetközi vegyes alapok nyíltvégű befektetési alapja USD sorozat" });
            result.Add(new StockListItem { Id = 1033, name = "K&H négyszer fizető euró származtatott nyíltvégű befektetési alap" });
            result.Add(new StockListItem { Id = 1568, name = "K&H növekedés plusz 2 nyíltvégű befektetési alap" });
            result.Add(new StockListItem { Id = 2327, name = "K&H olajipari származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 1617, name = "K&H olimpia plusz nyíltvégű befektetési alap" });
            result.Add(new StockListItem { Id = 1650, name = "K&H plusz német-svájci származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2817, name = "K&H privátbanki exkluzív komfort alapok nyíltvégű alapja" });
            result.Add(new StockListItem { Id = 2819, name = "K&H privátbanki exkluzív lendület alapok nyíltvégű alapja" });
            result.Add(new StockListItem { Id = 2775, name = "K&H privátbanki olajipari rugalmas 2 származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2472, name = "K&H prémium európai exportőrök származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2809, name = "K&H prémium európai tőzsdék rugalmas  származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2429, name = "K&H prémium fogyasztói javak tőkevédett származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2552, name = "K&H prémium gondoskodás származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2540, name = "K&H prémium gyermekközpontú származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2607, name = "K&H prémium gyógyszer és világcégek származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2473, name = "K&H prémium gyógyszeripari 3 származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2747, name = "K&H prémium gyógyszeripari 4 származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2625, name = "K&H prémium információbiztonság származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2711, name = "K&H prémium ingatlanpiac és világcégek származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2721, name = "K&H prémium nemzetközi csapat 2 származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2710, name = "K&H prémium nemzetközi csapat származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2592, name = "K&H prémium rangadó származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2763, name = "K&H prémium ráadás generációs vállalatok származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2723, name = "K&H prémium ráadás származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2783, name = "K&H prémium sportszponzorok származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 1507, name = "K&H prémium tőkevédett Latin-Amerika származtatott értékpapír befektetési ala" });
            result.Add(new StockListItem { Id = 1543, name = "K&H prémium tőkevédett orosz &amp; török származtatott zártvégű értékpapír befektetési alap" });
            result.Add(new StockListItem { Id = 2745, name = "K&H prémium többször termő dollár 2 származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2669, name = "K&H prémium többször termő dollár származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2613, name = "K&H prémium világcégek 10 származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2624, name = "K&H prémium világcégek 11 származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2539, name = "K&H prémium világcégek 7 származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2606, name = "K&H prémium világcégek 9 származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2428, name = "K&H prémium élelmiszeripari származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 1570, name = "K&H szakaszos hozamú 6 származtatott zártvégű értékpapír befektetési alap" });
            result.Add(new StockListItem { Id = 2384, name = "K&H szakaszos hozamú 7  tőkevédett származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2672, name = "K&H tartós befektetés 2021 alapok nyiltvégű befektetési alapja" });
            result.Add(new StockListItem { Id = 2789, name = "K&H tartós befektetés 2022 alapok nyiltvégű befektetési alapja" });
            result.Add(new StockListItem { Id = 1255, name = "K&H tőkevédett forint pénzpiaci nyíltvégű befektetési alap" });
            result.Add(new StockListItem { Id = 1724, name = "K&H tőkét négy részletben fizető származtatott zártvégű" });
            result.Add(new StockListItem { Id = 1691, name = "K&H tőkét részben előre fizető 2 nyíltvégű alap" });
            result.Add(new StockListItem { Id = 2304, name = "K&H tőkét részben előre fizető 4 származtatott zártvégű" });
            result.Add(new StockListItem { Id = 2217, name = "K&H tőkét részben előre fizető 4 származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 1663, name = "K&H tőkét részben előre fizető származtatott zártvégű alap 5000" });
            result.Add(new StockListItem { Id = 2222, name = "K&H többször termő 4 nyíltvégű alap" });
            result.Add(new StockListItem { Id = 2291, name = "K&H többször termő 5 nyíltvégű alap" });
            result.Add(new StockListItem { Id = 1699, name = "K&H vagyonvédett portfólió - augusztus nyíltvégű alapok alapja" });
            result.Add(new StockListItem { Id = 2212, name = "K&H vagyonvédett portfólió - április nyíltvégű alapok alapja" });
            result.Add(new StockListItem { Id = 2352, name = "K&H világcégek 4 származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 2302, name = "K&H világcégek tőkevédett 2 származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 1879, name = "K&H világcégek tőkevédett nyíltvégű alap" });
            result.Add(new StockListItem { Id = 2381, name = "K&H változó portfolió - augusztus nyíltvégű alapok részalapja" });
            result.Add(new StockListItem { Id = 2382, name = "K&H változó portfolió - december nyíltvégű alapok részalapja" });
            result.Add(new StockListItem { Id = 2380, name = "K&H változó portfólió - április nyíltvégű alapok részalapja" });
            result.Add(new StockListItem { Id = 2311, name = "K&H változó Ázsia tőkevédett származtatott zártvégű alap" });
            result.Add(new StockListItem { Id = 466, name = "K&H Ázsia Nyíltvégű Értékpapír Befektetési Alap" });
            result.Add(new StockListItem { Id = 770, name = "K&H Öko Alapok Nyíltvégű Értékpapír Befektetési Alapja" });
            result.Add(new StockListItem { Id = 2743, name = "K&H állampapír nyíltvégű alap F sorozat" });
            result.Add(new StockListItem { Id = 2397, name = "K&H állampapír nyíltvégű alap I sorozat" });
            result.Add(new StockListItem { Id = 1717, name = "K&H állampapír nyíltvégű alap normál sorozat" });
            result.Add(new StockListItem { Id = 2414, name = "Kamra Abszolút Hozamú Alap A sorozat" });
            result.Add(new StockListItem { Id = 2365, name = "Kamra Abszolút Hozamú Alap I sorozat" });
            result.Add(new StockListItem { Id = 2370, name = "MARKETPROG Bond Derivatív Kötvény Származtatott Alap A sorozat" });
            result.Add(new StockListItem { Id = 2801, name = "MARKETPROG Bond Derivatív Kötvény Származtatott Alap C sorozat" });
            result.Add(new StockListItem { Id = 2594, name = "MARKETPROG Bond Derivatív Kötvény Származtatott Alap EUR sorozat" });
            result.Add(new StockListItem { Id = 2372, name = "MARKETPROG Bond Derivatív Kötvény Származtatott Alap I sorozat" });
            result.Add(new StockListItem { Id = 2371, name = "MARKETPROG Technics Abszolút Hozamú Származtatott Alap A sorozat" });
            result.Add(new StockListItem { Id = 1651, name = "MKB 24 Karát III. Tőkevédett Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 2316, name = "MKB 24 Karát Likvid Tőkevédett Likviditási Befektetési Alap" });
            result.Add(new StockListItem { Id = 2360, name = "MKB Adaptív Kötvény Abszolút Hozamú Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 2412, name = "MKB Adaptív Kötvény Dollár Alapba Fektető Alap" });
            result.Add(new StockListItem { Id = 2413, name = "MKB Adaptív Kötvény Euró Alapba Fektető Alap" });
            result.Add(new StockListItem { Id = 2265, name = "MKB Aktív Alfa Abszolút Hozamú Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 2376, name = "MKB Aktív Alfa Dollár Alapba Fektető Alap" });
            result.Add(new StockListItem { Id = 2358, name = "MKB Aktív Alfa Euró Alapba Fektető Alap" });
            result.Add(new StockListItem { Id = 1673, name = "MKB Ambíció Nyíltvégű Befektetési Alap" });
            result.Add(new StockListItem { Id = 2609, name = "MKB Beszédes Hozam Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 19, name = "MKB Bonus Közép-Európai Részvény Befektetési Alap" });
            result.Add(new StockListItem { Id = 1553, name = "MKB Brazil Teljesítmény Tőkevédett Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 2375, name = "MKB Bázis Dollár Alapba Fektető Alap" });
            result.Add(new StockListItem { Id = 2359, name = "MKB Bázis Euró Alapba Fektető Alap" });
            result.Add(new StockListItem { Id = 1671, name = "MKB Bázis Nyíltvégű Befektetési Alap" });
            result.Add(new StockListItem { Id = 1166, name = "MKB Dollár Likviditási Alap" });
            result.Add(new StockListItem { Id = 1057, name = "MKB EURO Likviditási Alap" });
            result.Add(new StockListItem { Id = 2315, name = "MKB Egyensúly Dollár Alapba Fektető Alap" });
            result.Add(new StockListItem { Id = 2294, name = "MKB Egyensúly Euró Alapba Fektető Alap" });
            result.Add(new StockListItem { Id = 1672, name = "MKB Egyensúly Nyíltvégű Befektetési Alap" });
            result.Add(new StockListItem { Id = 1302, name = "MKB Energia Tőke- és Hozamvédett Származtatott Alap" });
            result.Add(new StockListItem { Id = 72, name = "MKB Európai Részvény Befektetési Alap" });
            result.Add(new StockListItem { Id = 1615, name = "MKB Feltörekvő Kína 2. Tőkevédett Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 2335, name = "MKB Feltörekvő Kína 3. Tőkevédett Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 1481, name = "MKB Feltörekvő Kína Tőkevédett Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 698, name = "MKB Forint Tőkevédett Likviditási Alap" });
            result.Add(new StockListItem { Id = 2593, name = "MKB Hozamdoktor Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 613, name = "MKB Ingatlan Alapok Alapja A sorozat" });
            result.Add(new StockListItem { Id = 2247, name = "MKB Kelet-Európai Négyes Tőkevédett Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 2409, name = "MKB Medicina Tőkevédett Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 1490, name = "MKB Megújuló Energia II. Tőke- és Hozamvédett Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 2245, name = "MKB Megújuló Energia Plusz Tőkevédett Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 1461, name = "MKB Megújuló Energia Tőke- és Hozamvédett Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 2337, name = "MKB Megújuló Energia Tőkevédett Likviditási Befektetési Alap" });
            result.Add(new StockListItem { Id = 2270, name = "MKB Momentum II. Tőkevédett Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 2244, name = "MKB Momentum Tőkevédett Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 1151, name = "MKB Nyersanyag Alapok Alapja" });
            result.Add(new StockListItem { Id = 2431, name = "MKB Nyersanyag Plusz Tőkevédett Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 2474, name = "MKB Német Részvények Tőkevédett Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 2266, name = "MKB PB TOP Abszolút Hozamú Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 1628, name = "MKB Premium Selection Zártvégű Nyilvános Befektetési Alap" });
            result.Add(new StockListItem { Id = 1557, name = "MKB Premium Selection Zártvégű Zártkörű Befektetési Alap" });
            result.Add(new StockListItem { Id = 1352, name = "MKB Primátus Tőke- és Hozamvédett Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 93, name = "MKB Prémium Rövid Kötvény Befektetési Alap" });
            result.Add(new StockListItem { Id = 1253, name = "MKB Természeti Kincsek Hozam Plusz Tőke- és Hozamvédett Származtatott Alap" });
            result.Add(new StockListItem { Id = 1419, name = "MKB Természeti Kincsek III. Tőke- és Hozamvédett Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 2290, name = "MKB Triumvirátus Plusz Tőkevédett Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 1339, name = "MKB Triász Tőke- és Hozamvédett Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 2305, name = "MKB Vezető Olajvállalatok Tőkevédett Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 2356, name = "MKB Világháló Tőkevédett Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 2623, name = "MKB e-Hoz@m Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 71, name = "MKB Állampapír Befektetési Alap" });
            result.Add(new StockListItem { Id = 2551, name = "MKB Élhető Jövő Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 1347, name = "MKB Észak-Amerikai Részvény Befektetési Alap" });
            result.Add(new StockListItem { Id = 2246, name = "MKB Ötvözet Tőkevédett Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 1886, name = "Magyar Posta Ingatlan Alap" });
            result.Add(new StockListItem { Id = 1801, name = "Magyar Posta Pénzpiaci Befektetési Alap" });
            result.Add(new StockListItem { Id = 2243, name = "Magyar Posta Rövid Kötvény Befektetési Alap" });
            result.Add(new StockListItem { Id = 2436, name = "Magyar Posta Takarék Harmónia Vegyes Befektetési Alap" });
            result.Add(new StockListItem { Id = 102, name = "Magyar Posta Takarék Hosszú Kötvény Befektetési Alap" });
            result.Add(new StockListItem { Id = 2284, name = "Magyar Posta Takarék Ingatlan Befektetési Alap I sorozat" });
            result.Add(new StockListItem { Id = 2300, name = "Mont Blanc Nemzetközi Kötvény Alap A sorozat" });
            result.Add(new StockListItem { Id = 2275, name = "Mont Blanc Nemzetközi Kötvény Alap B sorozat" });
            result.Add(new StockListItem { Id = 2698, name = "NHC Safe Kötvény Nyíltvégű Befektetési Alap" });
            result.Add(new StockListItem { Id = 2196, name = "OTP Abszolút Hozam Euró Alapba Fektető Alap" });
            result.Add(new StockListItem { Id = 472, name = "OTP Abszolút Hozam Nyíltvégű Származtatott Alap" });
            result.Add(new StockListItem { Id = 1349, name = "OTP Afrika Részvény Alap A sorozat" });
            result.Add(new StockListItem { Id = 1351, name = "OTP Afrika Részvény Alap B sorozat" });
            result.Add(new StockListItem { Id = 1350, name = "OTP Afrika Részvény Alap C sorozat" });
            result.Add(new StockListItem { Id = 2235, name = "OTP Aktív Fix Hozamvédett Zártvégű Alap" });
            result.Add(new StockListItem { Id = 2603, name = "OTP Arany Válogatott Tőkevédett Zártvégű Alap" });
            result.Add(new StockListItem { Id = 1292, name = "OTP Aranygól Nyíltvégű Alap" });
            result.Add(new StockListItem { Id = 2617, name = "OTP Dollár Ingatlan Alapba Fektető Alap" });
            result.Add(new StockListItem { Id = 130, name = "OTP Dollár Pénzpiaci Alap" });
            result.Add(new StockListItem { Id = 987, name = "OTP EMDA Származtatott Alap" });
            result.Add(new StockListItem { Id = 1535, name = "OTP EMEA Kötvény Alap" });
            result.Add(new StockListItem { Id = 2627, name = "OTP EMEA Kötvény Alap I sorozat" });
            result.Add(new StockListItem { Id = 2616, name = "OTP Euró Ingatlan Alapba Fektető Alap" });
            result.Add(new StockListItem { Id = 131, name = "OTP Euró Pénzpiaci Alap" });
            result.Add(new StockListItem { Id = 2479, name = "OTP Európa Sprint II. Zártvégű Alap" });
            result.Add(new StockListItem { Id = 2452, name = "OTP Európa Sprint Nyíltvégű Alap" });
            result.Add(new StockListItem { Id = 2324, name = "OTP Fejlett Világ I. Tőkevédett Zártvégű Alap" });
            result.Add(new StockListItem { Id = 2362, name = "OTP Fejlett Világ II. Tőkevédett Zártvégű Alap" });
            result.Add(new StockListItem { Id = 2192, name = "OTP Fundman Részvény Alap A sor. A sor." });
            result.Add(new StockListItem { Id = 2193, name = "OTP Fundman Részvény Alap B sor. B sor." });
            result.Add(new StockListItem { Id = 2194, name = "OTP Fundman Részvény Alap C sor. C sor." });
            result.Add(new StockListItem { Id = 1110, name = "OTP Föld Kincsei Árupiaci Alapok Alapja A sorozat" });
            result.Add(new StockListItem { Id = 1380, name = "OTP Föld Kincsei Árupiaci Alapok Alapja B sorozat" });
            result.Add(new StockListItem { Id = 2445, name = "OTP Föld Kincsei Árupiaci Alapok Alapja I sorozat" });
            result.Add(new StockListItem { Id = 921, name = "OTP G10 Euró Származtatott Alap" });
            result.Add(new StockListItem { Id = 1460, name = "OTP G10 Euró Származtatott Alap B sorozat" });
            result.Add(new StockListItem { Id = 2435, name = "OTP Globál Mix Tőkevédett Zártvégű Alap" });
            result.Add(new StockListItem { Id = 145, name = "OTP Ingatlanbefektetési Alap" });
            result.Add(new StockListItem { Id = 2437, name = "OTP Ingatlanvilág Alapok Alapja" });
            result.Add(new StockListItem { Id = 1106, name = "OTP Jubileum Dinamikus Tőkevédett Nyíltvégű Származtatott Alap" });
            result.Add(new StockListItem { Id = 920, name = "OTP Klímaváltozás 130/30 Származtatott Részvény Alap A sorozat" });
            result.Add(new StockListItem { Id = 1379, name = "OTP Klímaváltozás 130/30 Származtatott Részvény Alap B sorozat" });
            result.Add(new StockListItem { Id = 315, name = "OTP Közép-Európai Részvény Alap" });
            result.Add(new StockListItem { Id = 81, name = "OTP Maxima Kötvény Alap" });
            result.Add(new StockListItem { Id = 2295, name = "OTP Maxima Kötvény Alap B sorozat" });
            result.Add(new StockListItem { Id = 1377, name = "OTP Omega Fejlett Piaci Részvény Alapok Alapja" });
            result.Add(new StockListItem { Id = 82, name = "OTP Optima Tőkegarantált Kötvény Alap" });
            result.Add(new StockListItem { Id = 2296, name = "OTP Optima Tőkegarantált Kötvény Alap B sorozat" });
            result.Add(new StockListItem { Id = 1294, name = "OTP Orosz Részvény Alap A sorozat" });
            result.Add(new StockListItem { Id = 1382, name = "OTP Orosz Részvény Alap B sorozat" });
            result.Add(new StockListItem { Id = 1295, name = "OTP Orosz Részvény Alap C sorozat" });
            result.Add(new StockListItem { Id = 2671, name = "OTP PRIME Ingatlanbefektetési Alap" });
            result.Add(new StockListItem { Id = 83, name = "OTP Paletta Nyíltvégű Értékpapír Alap" });
            result.Add(new StockListItem { Id = 764, name = "OTP Planéta Feltörekvő Piaci Részvény Alapok Alapja A sorozat" });
            result.Add(new StockListItem { Id = 763, name = "OTP Planéta Feltörekvő Piaci Részvény Alapok Alapja B sorozat" });
            result.Add(new StockListItem { Id = 2403, name = "OTP Prémium Aktív Klasszikus Alapok Alapja" });
            result.Add(new StockListItem { Id = 582, name = "OTP Prémium Euró Alap" });
            result.Add(new StockListItem { Id = 584, name = "OTP Prémium Kiegyensúlyozott Alap" });
            result.Add(new StockListItem { Id = 583, name = "OTP Prémium Klasszikus Alap" });
            result.Add(new StockListItem { Id = 581, name = "OTP Prémium Növekedési Alap" });
            result.Add(new StockListItem { Id = 1430, name = "OTP Prémium Származtatott Alapok Alapja" });
            result.Add(new StockListItem { Id = 2701, name = "OTP Prémium Származtatott Euró Alapok Alapja" });
            result.Add(new StockListItem { Id = 2402, name = "OTP Prémium Trend Klasszikus Alapok Alapja" });
            result.Add(new StockListItem { Id = 1714, name = "OTP Prémium Tőkegarantált Pénzpiaci Alap A sorozat" });
            result.Add(new StockListItem { Id = 85, name = "OTP Quality Nyíltvégű Részvény Alap" });
            result.Add(new StockListItem { Id = 958, name = "OTP Quality Nyíltvégű Részvény Alap B sorozat" });
            result.Add(new StockListItem { Id = 1775, name = "OTP REMIX20 Alap" });
            result.Add(new StockListItem { Id = 1778, name = "OTP REMIX20 II. Alap" });
            result.Add(new StockListItem { Id = 2341, name = "OTP Reál Alfa II. Tőkevédett Zártvégű Alap" });
            result.Add(new StockListItem { Id = 2361, name = "OTP Reál Alfa III. Tőkevédett Alap" });
            result.Add(new StockListItem { Id = 2408, name = "OTP Reál Alfa IV. Tőkevédett Alap" });
            result.Add(new StockListItem { Id = 2713, name = "OTP Reál Alfa Plusz II. Zártvégű Alap" });
            result.Add(new StockListItem { Id = 2767, name = "OTP Reál Alfa Plusz III. Tőkevédett Zártvégű Alap" });
            result.Add(new StockListItem { Id = 2557, name = "OTP Reál Alfa Plusz Tőkevédett Alap" });
            result.Add(new StockListItem { Id = 2323, name = "OTP Reál Alfa Tőkevédett Zártvégű Alap" });
            result.Add(new StockListItem { Id = 2622, name = "OTP Reál Alfa V. Zártvégű Alap" });
            result.Add(new StockListItem { Id = 2271, name = "OTP Reál Futam II. Nyilvános Tőkevédett Zártvégű Származtatott Alap" });
            result.Add(new StockListItem { Id = 2308, name = "OTP Reál Futam III. Tőkevédett Alap" });
            result.Add(new StockListItem { Id = 2434, name = "OTP Reál Futam IV. Tőkevédett Alap" });
            result.Add(new StockListItem { Id = 2242, name = "OTP Reál Futam Nyilvános Tőkevédett Zártvégű Származtatott Alap" });
            result.Add(new StockListItem { Id = 2602, name = "OTP Reál Futam V. Tőkevédett Zártvégű Alap" });
            result.Add(new StockListItem { Id = 2699, name = "OTP Reál Futam VI. Zártvégű Alap" });
            result.Add(new StockListItem { Id = 2237, name = "OTP Reál Fókusz 4+ Alap" });
            result.Add(new StockListItem { Id = 2227, name = "OTP Reál Fókusz II Nyilvános Hozamvédett Zártvégű Származtatott Alap" });
            result.Add(new StockListItem { Id = 2231, name = "OTP Reál Fókusz III Nyilvános Hozamvédett Zártvégű Származtatott Alap" });
            result.Add(new StockListItem { Id = 2292, name = "OTP Reál Globális Top Nyilvános Tőkevédett Zártvégű Származtatott Alap" });
            result.Add(new StockListItem { Id = 2236, name = "OTP Rio 2014 Hozamvédett Zártvégű Alap" });
            result.Add(new StockListItem { Id = 2447, name = "OTP Sigma Származtatott Alap A sorozat" });
            result.Add(new StockListItem { Id = 2446, name = "OTP Sigma Származtatott Alap I sorozat" });
            result.Add(new StockListItem { Id = 2670, name = "OTP Supra Dollár Alapba Fektető Alap" });
            result.Add(new StockListItem { Id = 2195, name = "OTP Supra Euró Alapba Fektető Alap" });
            result.Add(new StockListItem { Id = 988, name = "OTP Supra Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 1664, name = "OTP Szinergia III. Nyíltvégű Alap" });
            result.Add(new StockListItem { Id = 1677, name = "OTP Szinergia IV. Nyíltvégű Alap" });
            result.Add(new StockListItem { Id = 2342, name = "OTP Szinergia IX. Tőkevédett Zártvégű Alap" });
            result.Add(new StockListItem { Id = 1692, name = "OTP Szinergia Plussz Hozamvédett Zártvégű Alap" });
            result.Add(new StockListItem { Id = 1707, name = "OTP Szinergia Plussz II. Nyíltvégű Alap" });
            result.Add(new StockListItem { Id = 2241, name = "OTP Szinergia V. Tőkevédett Zártvégű Alap" });
            result.Add(new StockListItem { Id = 2272, name = "OTP Szinergia VI. Tőkevédett Zártvégű Alap" });
            result.Add(new StockListItem { Id = 2293, name = "OTP Szinergia VII. Tőkevédett Zártvégű Alap" });
            result.Add(new StockListItem { Id = 2310, name = "OTP Szinergia VIII. Tőkevédett Zártvégű Alap" });
            result.Add(new StockListItem { Id = 2410, name = "OTP Szinergia X. Tőkevédett  Zártvégű Alap" });
            result.Add(new StockListItem { Id = 2612, name = "OTP Szinergia XI. Tőkevédett Zártvégű Alap" });
            result.Add(new StockListItem { Id = 2700, name = "OTP Szinergia XII. Tőkevédett Zártvégű Alap" });
            result.Add(new StockListItem { Id = 2715, name = "OTP Szinergia XIII. Tőkevédett Zártvégű Alap" });
            result.Add(new StockListItem { Id = 2739, name = "OTP Szinergia XIV. Tőkevédett Zártvégű Alap" });
            result.Add(new StockListItem { Id = 2765, name = "OTP Szinergia XV. Tőkevédett Zártvégű Alap" });
            result.Add(new StockListItem { Id = 2811, name = "OTP Szinergia XVI. Tőkevédett Zártvégű Alap" });
            result.Add(new StockListItem { Id = 1534, name = "OTP Trend Nemzetközi Részvény Alap A sorozat" });
            result.Add(new StockListItem { Id = 1536, name = "OTP Trend Nemzetközi Részvény Alap B sorozat" });
            result.Add(new StockListItem { Id = 239, name = "OTP Tőkegarantált Pénzpiaci Alap" });
            result.Add(new StockListItem { Id = 567, name = "OTP Tőzsdén Kereskedett BUX Indexkövető Alap" });
            result.Add(new StockListItem { Id = 1293, name = "OTP Török Részvény Alap A sorozat" });
            result.Add(new StockListItem { Id = 1381, name = "OTP Török Részvény Alap B sorozat" });
            result.Add(new StockListItem { Id = 1296, name = "OTP Török Részvény Alap C sorozat" });
            result.Add(new StockListItem { Id = 986, name = "OTP Ázsiai Ingatlan és Infrastruktúra Értékpapír Alapok Alapja" });
            result.Add(new StockListItem { Id = 902, name = "OTP Új Európa Nyíltvégű Értékpapír Alap A sorozat" });
            result.Add(new StockListItem { Id = 1378, name = "OTP Új Európa Nyíltvégű Értékpapír Alap B sorozat" });
            result.Add(new StockListItem { Id = 2229, name = "OTP Új Világ Fix Hozamvédett Zártvégű Alap" });
            result.Add(new StockListItem { Id = 2197, name = "OTP Új-Európa Euró Alapba Fektető Alap" });
            result.Add(new StockListItem { Id = 159, name = "PARVEST BOND USD GOVERNMENT" });
            result.Add(new StockListItem { Id = 2014, name = "Parvest Bond USD Classic H EUR" });
            result.Add(new StockListItem { Id = 2039, name = "Parvest Equity Japan EUR Hedged" });
            result.Add(new StockListItem { Id = 2003, name = "Parvest Step 80 World Emerging (EUR)" });
            result.Add(new StockListItem { Id = 2015, name = "Parvest US High Yield Bond Classic H EUR" });
            result.Add(new StockListItem { Id = 2016, name = "Parvest US Mid Cap Classic H EUR" });
            result.Add(new StockListItem { Id = 2017, name = "Parvest USA Classic H EUR" });
            result.Add(new StockListItem { Id = 512, name = "Platina Alfa Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 513, name = "Platina Béta Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 515, name = "Platina Delta Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 514, name = "Platina Gamma Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 516, name = "Platina Pí Származtatott Befektetési Alap A sorozat" });
            result.Add(new StockListItem { Id = 1385, name = "Platina Pí Származtatott Befektetési Alap B sorozat" });
            result.Add(new StockListItem { Id = 1713, name = "Q1 Ingatlanfejlesztő Befektetési Alap" });
            result.Add(new StockListItem { Id = 2232, name = "QUANTIS Abszolút Hozamú Alapok Alapja" });
            result.Add(new StockListItem { Id = 1121, name = "QUANTIS Dél-Amerika Részvény Alap" });
            result.Add(new StockListItem { Id = 1390, name = "QUANTIS Dél-Amerika Részvény Alap N EUR sorozat" });
            result.Add(new StockListItem { Id = 1369, name = "QUANTIS Dél-Amerika Részvény Alap N HUF sorozat" });
            result.Add(new StockListItem { Id = 1391, name = "QUANTIS Dél-Amerika Részvény Alap N USD sorozat" });
            result.Add(new StockListItem { Id = 1119, name = "QUANTIS Globális Fejlett Piaci Részvény Alap A sorozat" });
            result.Add(new StockListItem { Id = 1387, name = "QUANTIS Globális Fejlett Piaci Részvény Alap N EUR sorozat" });
            result.Add(new StockListItem { Id = 1368, name = "QUANTIS Globális Fejlett Piaci Részvény Alap N HUF sorozat" });
            result.Add(new StockListItem { Id = 1386, name = "QUANTIS Globális Fejlett Piaci Részvény Alap N USD sorozat" });
            result.Add(new StockListItem { Id = 1165, name = "QUANTIS Globális Fejlődő Piaci Részvény Alap" });
            result.Add(new StockListItem { Id = 1395, name = "QUANTIS Globális Fejlődő Piaci Részvény Alap N EUR sorozat" });
            result.Add(new StockListItem { Id = 1374, name = "QUANTIS Globális Fejlődő Piaci Részvény Alap N HUF sorozat" });
            result.Add(new StockListItem { Id = 1394, name = "QUANTIS Globális Fejlődő Piaci Részvény Alap N USD sorozat" });
            result.Add(new StockListItem { Id = 2226, name = "QUANTIS Globális Átváltoztatható Kötvény Alapok Alapja A sorozat" });
            result.Add(new StockListItem { Id = 1389, name = "QUANTIS Globális Átváltoztatható Kötvény Alapok Alapja N EUR sorozat" });
            result.Add(new StockListItem { Id = 1373, name = "QUANTIS Globális Átváltoztatható Kötvény Alapok Alapja N HUF sorozat" });
            result.Add(new StockListItem { Id = 1388, name = "QUANTIS Globális Átváltoztatható Kötvény Alapok Alapja N USD sorozat" });
            result.Add(new StockListItem { Id = 1107, name = "QUANTIS HUF Likviditási Alap" });
            result.Add(new StockListItem { Id = 1371, name = "QUANTIS HUF Likviditási Alap N HUF Sorozat" });
            result.Add(new StockListItem { Id = 1108, name = "QUANTIS India USD Részvény Alapok Alapja" });
            result.Add(new StockListItem { Id = 1397, name = "QUANTIS India USD Részvény Alapok Alapja N EUR sorozat" });
            result.Add(new StockListItem { Id = 1360, name = "QUANTIS India USD Részvény Alapok Alapja N HUF sorozat" });
            result.Add(new StockListItem { Id = 1396, name = "QUANTIS India USD Részvény Alapok Alapja N USD sorozat" });
            result.Add(new StockListItem { Id = 1120, name = "QUANTIS Kelet-Európa Részvény Alap" });
            result.Add(new StockListItem { Id = 1392, name = "QUANTIS Kelet-Európa Részvény Alap N EUR sorozat" });
            result.Add(new StockListItem { Id = 1370, name = "QUANTIS Kelet-Európa Részvény Alap N HUF Sorozat" });
            result.Add(new StockListItem { Id = 1393, name = "QUANTIS Kelet-Európa Részvény Alap N USD sorozat" });
            result.Add(new StockListItem { Id = 1399, name = "QUANTIS Kína  Részvény Alap N EUR sorozat" });
            result.Add(new StockListItem { Id = 1109, name = "QUANTIS Kína Részvény Alap" });
            result.Add(new StockListItem { Id = 1376, name = "QUANTIS Kína Részvény Alap N HUF sorozat" });
            result.Add(new StockListItem { Id = 1398, name = "QUANTIS Kína Részvény Alap N USD sorozat" });
            result.Add(new StockListItem { Id = 1164, name = "QUANTIS Növekedési HUF Vegyes Alapok Alapja" });
            result.Add(new StockListItem { Id = 1375, name = "QUANTIS Növekedési HUF Vegyes Alapok Alapja N HUF sorozat" });
            result.Add(new StockListItem { Id = 1383, name = "QUANTIS Oroszország Részvény Alap A sorozat" });
            result.Add(new StockListItem { Id = 1400, name = "QUANTIS Oroszország Részvény Alap N EUR sorozat" });
            result.Add(new StockListItem { Id = 1372, name = "QUANTIS Oroszország Részvény Alap N HUF sorozat" });
            result.Add(new StockListItem { Id = 1401, name = "QUANTIS Oroszország Részvény Alap N USD sorozat" });
            result.Add(new StockListItem { Id = 18, name = "Quaestor Aranytallér Vegyes Nyíltvégű Befektetési Alap" });
            result.Add(new StockListItem { Id = 21, name = "Quaestor Borostyán Nyíltvégű Kötvény Befektetési Alap" });
            result.Add(new StockListItem { Id = 112, name = "Quaestor Deviza Nyíltvégű Befektetési Alap" });
            result.Add(new StockListItem { Id = 67, name = "Quaestor Kurázsi Pénzpiaci Nyíltvégű Befektetési Alap" });
            result.Add(new StockListItem { Id = 106, name = "Quaestor Tallér Részvény Nyíltvégű Befektetési Alap" });
            result.Add(new StockListItem { Id = 133, name = "Raiffeisen Alapok Alapja - Konvergencia" });
            result.Add(new StockListItem { Id = 427, name = "Raiffeisen Csendes-Óceáni Részvény Alap" });
            result.Add(new StockListItem { Id = 1224, name = "Raiffeisen Euró Prémium Rövid Kötvény Alap " });
            result.Add(new StockListItem { Id = 2018, name = "Raiffeisen Európa Magas Hozamú Kötvény Alap" });
            result.Add(new StockListItem { Id = 2019, name = "Raiffeisen Feltörekvő Piaci Lokális Kötvény Alap" });
            result.Add(new StockListItem { Id = 2113, name = "Raiffeisen Global Allocation Strategies Plus" });
            result.Add(new StockListItem { Id = 327, name = "Raiffeisen Hozam Prémium Származtatott Alap" });
            result.Add(new StockListItem { Id = 2807, name = "Raiffeisen Hozam Prémium Származtatott Alap R sorozat" });
            result.Add(new StockListItem { Id = 285, name = "Raiffeisen Index Prémium Származtatott Alap" });
            result.Add(new StockListItem { Id = 1330, name = "Raiffeisen Ingatlan Alap A sorozat" });
            result.Add(new StockListItem { Id = 2619, name = "Raiffeisen Ingatlan Alap B sorozat" });
            result.Add(new StockListItem { Id = 2620, name = "Raiffeisen Ingatlan Alap C sorozat" });
            result.Add(new StockListItem { Id = 2621, name = "Raiffeisen Ingatlan Alap D sorozat" });
            result.Add(new StockListItem { Id = 2773, name = "Raiffeisen Ingatlan Alap U sorozat" });
            result.Add(new StockListItem { Id = 13, name = "Raiffeisen Kamat Prémium Rövid Kötvény Alap" });
            result.Add(new StockListItem { Id = 14, name = "Raiffeisen Kötvény Alap" });
            result.Add(new StockListItem { Id = 2634, name = "Raiffeisen Kötvény Alap B sorozat" });
            result.Add(new StockListItem { Id = 2647, name = "Raiffeisen Kötvény Alap I sorozat" });
            result.Add(new StockListItem { Id = 772, name = "Raiffeisen Megoldás Alapok Alapja" });
            result.Add(new StockListItem { Id = 773, name = "Raiffeisen Megoldás Plusz Alapok Alapja" });
            result.Add(new StockListItem { Id = 2707, name = "Raiffeisen Megoldás Plusz Alapok Alapja A sorozat" });
            result.Add(new StockListItem { Id = 2708, name = "Raiffeisen Megoldás Pro Alapok Alapja B sorozat" });
            result.Add(new StockListItem { Id = 2705, name = "Raiffeisen Megoldás Pro Alapok Alapja E sorozat" });
            result.Add(new StockListItem { Id = 2709, name = "Raiffeisen Megoldás Start Alapok Alapja B sorozat" });
            result.Add(new StockListItem { Id = 2706, name = "Raiffeisen Megoldás Start Alapok Alapja E sorozat" });
            result.Add(new StockListItem { Id = 2803, name = "Raiffeisen Nemzetközi Kötvény Alapok Alapja A sorozat" });
            result.Add(new StockListItem { Id = 2787, name = "Raiffeisen Nemzetközi Kötvény Alapok Alapja F sorozat" });
            result.Add(new StockListItem { Id = 17, name = "Raiffeisen Nemzetközi Részvény Alapok Alapja" });
            result.Add(new StockListItem { Id = 284, name = "Raiffeisen Nyersanyag Alapok Alapja" });
            result.Add(new StockListItem { Id = 971, name = "Raiffeisen Oroszország Részvény Alap" });
            result.Add(new StockListItem { Id = 893, name = "Raiffeisen Private Banking Rajna Alapok Alapja" });
            result.Add(new StockListItem { Id = 691, name = "Raiffeisen Private Pannonia Alapok Alapja" });
            result.Add(new StockListItem { Id = 15, name = "Raiffeisen Részvény Alap" });
            result.Add(new StockListItem { Id = 2785, name = "Raiffeisen Részvény Alap B sorozat" });
            result.Add(new StockListItem { Id = 2805, name = "Raiffeisen Részvény Alap R sorozat" });
            result.Add(new StockListItem { Id = 965, name = "Raiffeisen-Abszolút hozam-Kiegyensúlyozott-Globális Alapok Alapja" });
            result.Add(new StockListItem { Id = 429, name = "Raiffeisen-Dollár-RövidLejáratúKötvény Alap" });
            result.Add(new StockListItem { Id = 430, name = "Raiffeisen-Energia-Részvény Alap" });
            result.Add(new StockListItem { Id = 431, name = "Raiffeisen-Euro-RövidLejáratúKötvény Alap" });
            result.Add(new StockListItem { Id = 261, name = "Raiffeisen-Euro-Vállalati Kötvény Alap" });
            result.Add(new StockListItem { Id = 257, name = "Raiffeisen-EuroPlusz-Kötvény Alap" });
            result.Add(new StockListItem { Id = 262, name = "Raiffeisen-Eurázsia-Részvény Alap" });
            result.Add(new StockListItem { Id = 263, name = "Raiffeisen-Európa-Részvény Alap" });
            result.Add(new StockListItem { Id = 433, name = "Raiffeisen-Európa-SmallCap Alap" });
            result.Add(new StockListItem { Id = 1137, name = "Raiffeisen-FeltörekvőPiacok-Infrastruktúra Alap" });
            result.Add(new StockListItem { Id = 434, name = "Raiffeisen-FeltörekvőPiacok-Részvény Alap" });
            result.Add(new StockListItem { Id = 256, name = "Raiffeisen-Globál-Kötvény Alap" });
            result.Add(new StockListItem { Id = 264, name = "Raiffeisen-Globál-Mix Alap" });
            result.Add(new StockListItem { Id = 265, name = "Raiffeisen-Globál-Részvény Alap" });
            result.Add(new StockListItem { Id = 435, name = "Raiffeisen-HealthCare-Részvény Alap" });
            result.Add(new StockListItem { Id = 432, name = "Raiffeisen-KeletEurópa-Kötvény Alap" });
            result.Add(new StockListItem { Id = 260, name = "Raiffeisen-KeletEurópa-Részvény Alap" });
            result.Add(new StockListItem { Id = 968, name = "Raiffeisen-MagasOsztalékú-Részvény Alap" });
            result.Add(new StockListItem { Id = 963, name = "Raiffeisen-Topselection-Garantált-Befektetési Alap" });
            result.Add(new StockListItem { Id = 267, name = "Raiffeisen-USA-Részvény Alap" });
            result.Add(new StockListItem { Id = 1880, name = "SUI GENERIS 1.1 Származtatott Alap" });
            result.Add(new StockListItem { Id = 221, name = "Sberbank Pénzpiaci Befektetési Alap MEGSZŰNÉSI ELJÁRÁS ALATT." });
            result.Add(new StockListItem { Id = 2753, name = "Sequoia Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 1746, name = "Superposition Származtatott Alap" });
            result.Add(new StockListItem { Id = 2759, name = "Superposition Származtatott Befektetési Alap B sorozat" });
            result.Add(new StockListItem { Id = 2761, name = "Superposition Származtatott Befektetési Alap C sorozat" });
            result.Add(new StockListItem { Id = 1328, name = "Szuper 8 Tőkevédett Származtatott Alap" });
            result.Add(new StockListItem { Id = 2815, name = "Takarék Adria Közép-Európai Részvény Befektetési Alap" });
            result.Add(new StockListItem { Id = 1160, name = "Takarék FHB Abszolút Hozamú Befektetési Alap " });
            result.Add(new StockListItem { Id = 2425, name = "Takarék FHB Apollo Származtatott Részvény Befektetési Alap" });
            result.Add(new StockListItem { Id = 2329, name = "Takarék FHB Euró Ingatlan Alapok Alapja" });
            result.Add(new StockListItem { Id = 1712, name = "Takarék FHB Rövid Kötvény Befektetési Alap" });
            result.Add(new StockListItem { Id = 2339, name = "Takarék FHB Származtatott Befektetési Alap" });
            result.Add(new StockListItem { Id = 1896, name = "Templeton Asian Bond Fund N" });
            result.Add(new StockListItem { Id = 1897, name = "Templeton Asian Bond Fund N EUR" });
            result.Add(new StockListItem { Id = 1898, name = "Templeton Asian Growth Fund N" });
            result.Add(new StockListItem { Id = 1899, name = "Templeton Asian Growth Fund N EUR" });
            result.Add(new StockListItem { Id = 1955, name = "Templeton Asian Growth Fund N EUR-H1" });
            result.Add(new StockListItem { Id = 2037, name = "Templeton Asian Growth Fund N HUF" });
            result.Add(new StockListItem { Id = 2043, name = "Templeton Asian Smaller Companies Fund A (acc) EUR" });
            result.Add(new StockListItem { Id = 1900, name = "Templeton Bric Fund N" });
            result.Add(new StockListItem { Id = 1901, name = "Templeton Bric Fund N EUR" });
            result.Add(new StockListItem { Id = 1902, name = "Templeton China Fund N" });
            result.Add(new StockListItem { Id = 1903, name = "Templeton Eastern Europe Fund N" });
            result.Add(new StockListItem { Id = 1904, name = "Templeton Emerging Markets Bond Fund N USD" });
            result.Add(new StockListItem { Id = 1905, name = "Templeton Emerging Markets Fund N EUR" });
            result.Add(new StockListItem { Id = 1906, name = "Templeton Emerging Markets Fund N USD" });
            result.Add(new StockListItem { Id = 1960, name = "Templeton Emerging Markets Smaller Co. N USD" });
            result.Add(new StockListItem { Id = 1907, name = "Templeton Euro Liquid Reserve Fund N" });
            result.Add(new StockListItem { Id = 1909, name = "Templeton Euroland Fund N" });
            result.Add(new StockListItem { Id = 1910, name = "Templeton European Fund N EUR" });
            result.Add(new StockListItem { Id = 1911, name = "Templeton European Total Return Fund N" });
            result.Add(new StockListItem { Id = 1912, name = "Templeton Global (EURO) Fund N" });
            result.Add(new StockListItem { Id = 1913, name = "Templeton Global Balanced Fund N EUR" });
            result.Add(new StockListItem { Id = 1954, name = "Templeton Global Balanced Fund N EUR-H1" });
            result.Add(new StockListItem { Id = 1914, name = "Templeton Global Bond (EURO) Fund N" });
            result.Add(new StockListItem { Id = 1915, name = "Templeton Global Bond Fund N EUR" });
            result.Add(new StockListItem { Id = 1958, name = "Templeton Global Bond Fund N EUR-H1" });
            result.Add(new StockListItem { Id = 2035, name = "Templeton Global Bond Fund N HUF" });
            result.Add(new StockListItem { Id = 1916, name = "Templeton Global Bond Fund N USD" });
            result.Add(new StockListItem { Id = 1945, name = "Templeton Global Fund N" });
            result.Add(new StockListItem { Id = 1961, name = "Templeton Global High Yield Fund Class N EUR" });
            result.Add(new StockListItem { Id = 1946, name = "Templeton Global Income Fund N USD" });
            result.Add(new StockListItem { Id = 1947, name = "Templeton Global Total Return Fund N" });
            result.Add(new StockListItem { Id = 1948, name = "Templeton Global Total Return Fund N EUR" });
            result.Add(new StockListItem { Id = 1959, name = "Templeton Global Total Return Fund N EUR-H1" });
            result.Add(new StockListItem { Id = 2036, name = "Templeton Global Total Return Fund N HUF" });
            result.Add(new StockListItem { Id = 1949, name = "Templeton Growth (EURO) Fund N" });
            result.Add(new StockListItem { Id = 1950, name = "Templeton Korea Fund N" });
            result.Add(new StockListItem { Id = 1951, name = "Templeton Latin America Fund N" });
            result.Add(new StockListItem { Id = 1952, name = "Templeton Thailand Fund N" });
            result.Add(new StockListItem { Id = 2348, name = "Torony Ingatlan Befektetési Alap" });
            result.Add(new StockListItem { Id = 1495, name = "Trendváltó Plusz Tőkevédett Származtatott Alap A Sorozat" });
            result.Add(new StockListItem { Id = 1496, name = "Trendváltó Plusz Tőkevédett Származtatott Alap K Sorozat" });
            result.Add(new StockListItem { Id = 1004, name = "VB-GoEast-Bond" });
            result.Add(new StockListItem { Id = 1006, name = "VB-Pacific-Invest" });
            result.Add(new StockListItem { Id = 2596, name = "Vertex Származtatott  Befektetési Alap" });
            result.Add(new StockListItem { Id = 1291, name = "Volksbank-Smile" });
            result.Add(new StockListItem { Id = 2269, name = "YOU Invest Kiegyensúlyozott EUR Alapok Alapja" });
            result.Add(new StockListItem { Id = 2268, name = "YOU Invest Stabil EUR Alapok Alapja" });
            result.Add(new StockListItem { Id = 2267, name = "You Invest Dinamikus EUR Alapok Alapja" });
            result.Add(new StockListItem { Id = 148, name = "iCASH Dynamic FX Származtatott Nyíltvégű Befektetési Alap" });
            result.Add(new StockListItem { Id = 554, name = "iCash Conservative Nyíltvégű Befektetési Alap" });
            #endregion
            return result;
        }

    }
}

/*
select name,ticker,state,count(*)as Count from portfolio_hunstock_data
join portfolio_hunstock on portfolio_hunstock.id  = portfolio_hunstock_data.StockId
group by portfolio_hunstock_data.StockId

select name,ticker,state,count(*)as Count from portfolio_hunfund_data
join portfolio_hunfunds on portfolio_hunfunds.id  = portfolio_hunfund_data.fundid
group by portfolio_hunfund_data.fundid;
*/
