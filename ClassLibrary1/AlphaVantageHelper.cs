﻿using ClassLibrary1.ResponseClasses;
using CsvHelper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Models
{
    public static class AlphaVantageHelper {

        public static List<object> getCachedAlphaVantageData<T>(string symbol, DateTime dtFrom, DateTime dtTo)
        {
            List<object> response = new List<object>();
            try
            {
                var AvFunctionType = typeof(T);
                switch (AvFunctionType.Name)
                {
                    case "TIME_SERIES_DAILY_ADJUSTED":
                        response = (from tsda in Db.Instance.av_time_series_daily_adjusted
                                    join s in Db.Instance.av_time_series_daily_adjusted_symbol on tsda.SymbolId equals s.Id
                                    where s.Symbol == symbol
                                    select new TIME_SERIES_DAILY_ADJUSTED
                                    {
                                        timestamp = tsda.Day,
                                        adjusted_close = tsda.AdjustedClose.HasValue ? tsda.AdjustedClose.Value : 0,
                                        close = tsda.AdjustedClose.HasValue ? tsda.AdjustedClose.Value : 0,
                                        high = tsda.High.HasValue ? tsda.High.Value : 0,
                                        low = tsda.Low.HasValue ? tsda.Low.Value : 0,
                                        open = tsda.Open.HasValue ? tsda.Open.Value : 0,
                                        volume = tsda.Volume.HasValue ? tsda.Volume.Value : 0,
                                        dividend_amount = tsda.DividendAmount.HasValue ? tsda.DividendAmount.Value : 0,
                                        split_coefficient = 0,
                                        capitalization = 0
                                    }).ToList().Cast<object>().ToList();
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response;
        }

        public static List<KeyValuePair<string, string>> KVPBuilder(string symbol, string function = "TIME_SERIES_DAILY_ADJUSTED", string datatype = "csv", string outputsize = "full")
        {
            List<KeyValuePair<string, string>> kvpListCsv = new List<KeyValuePair<string, string>>();
            kvpListCsv.Add(new KeyValuePair<string, string>("symbol", symbol));
            kvpListCsv.Add(new KeyValuePair<string, string>("function", function));
            kvpListCsv.Add(new KeyValuePair<string, string>("datatype", datatype));
            kvpListCsv.Add(new KeyValuePair<string, string>("outputsize", outputsize));
            return kvpListCsv;
        }

        public static string QueryBuilder(List<KeyValuePair<string, string>> kvpList)
        {
            string result = "/query?";
            foreach (var kvp in kvpList)
            {
                result += kvp.Key + "=" + kvp.Value + "&"; 
            }
            result += "apikey=" + Constants.AVKey;            
            return result;
        }

        public static async Task<string> GetResponse(string av_query)
        {
            string av_responseCsv = await Http.Get("http://www.alphavantage.co", av_query);
            return av_responseCsv;
        }

    }

}
