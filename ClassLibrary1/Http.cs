﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Net.Http;
using ClassLibrary1.Models;
using Newtonsoft.Json;

namespace ClassLibrary1
{
    public class Selection
    {
        [JsonProperty("category")]
        public string category { get; set; }

        [JsonProperty("selectedInstruments")]
        public List<SelectedInstrument> selectedInstruments { get; set; }        
    }

    public class SelectedInstrument
    {
        [JsonProperty("id")]
        public string id { get; set; }

        [JsonProperty("code")]
        public string code { get; set; }
    }

    public class BETPOST
    {
        [JsonProperty("currentCategory")]
        public string currentCategory { get; set; } = "W_RESZVENYA";

        [JsonProperty("endingValue")]
        public string endingValue { get; set; } = "2017.12.20.";

        [JsonProperty("format")]
        public string format { get; set; } = "XLSX";

        [JsonProperty("market")]
        public string market { get; set; } = "PROMT";

        [JsonProperty("resolution")]
        public string resolution { get; set; } = "DAY_TO_DAY";

        [JsonProperty("startingValue")]
        public string startingValue { get; set; } = "2017.01.01.";

        [JsonProperty("type")]
        public string type { get; set; } = "DETAILED";

        [JsonProperty("selectionList")]
        public List<Selection> selectionList { get; set; }
    }

    public static class Http
    {
        public static async Task<string> Post2(string url)
        {

            try
            {
                BETPOST payload = new BETPOST()
                {
                    selectionList = new List<Selection>() {
                     new Selection(){
                         category = "Részvény Prémium",
                         selectedInstruments = new List<SelectedInstrument>(){
                              new SelectedInstrument (){  id = "518", code = "MOL"},
                              //new SelectedInstrument (){  id = 528, code = "OTP"},
                              //new SelectedInstrument (){  id = 608, code = "RICHTER"}
                         }
                     }
                 }
                };

                // Serialize our concrete class into a JSON String
                var stringPayload = JsonConvert.SerializeObject(payload);

                // Wrap our JSON inside a StringContent which then can be used by the HttpClient class
                var httpContent = new StringContent(stringPayload, Encoding.UTF8, "application/json");

                using (var httpClient = new HttpClient())
                {

                    // Do the actual request and await the response
                    var httpResponse = await httpClient.PostAsync(url, httpContent);

                    // If the response contains content we want to read it!
                    if (httpResponse.Content != null)
                    {
                        var responseContent = await httpResponse.Content.ReadAsStringAsync();

                        // From here on you could deserialize the ResponseContent back again to a concrete C# type using Json.Net
                        return responseContent;
                    }
                }
            }
            catch (Exception e) {
                return e.Message;
            }
            return "";
        }

        public static async Task<string> Post3(string baseAddr)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddr);
                BETPOST payload = new BETPOST()
                {
                    selectionList = new List<Selection>() {
                     new Selection(){
                         category = "Részvény Prémium",
                         selectedInstruments = new List<SelectedInstrument>(){
                              new SelectedInstrument (){  id = "518", code = "MOL"},
                              //new SelectedInstrument (){  id = 528, code = "OTP"},
                              //new SelectedInstrument (){  id = 608, code = "RICHTER"}
                         }
                     }
                 }
                };

                // Serialize our concrete class into a JSON String
                var stringPayload = JsonConvert.SerializeObject(payload);

                // Wrap our JSON inside a StringContent which then can be used by the HttpClient class
                var httpContent = new StringContent(stringPayload, Encoding.UTF8, "application/json");

                //HttpResponseMessage response = await client.PostAsync(baseAddr, httpContent);
                //string resultContent = await response.Content.ReadAsStringAsync();
                HttpResponseMessage response = client.PostAsync(baseAddr, httpContent).Result;
                string resultContent = response.Content.ReadAsStringAsync().Result;
                int start = resultContent.IndexOf("<JSON>");
                int end = resultContent.IndexOf("</JSON>");
                if (start > 0 && end > 0)
                    return resultContent.Substring(start + 6, end - start - 6);
                else
                    return resultContent;
            }
        }

        public static async Task<string> Get(string baseAddr, string uri)
        {
            try
            {
                HttpClient client = new HttpClient();
                HttpResponseMessage response = await client.GetAsync(baseAddr + uri);
                //HttpResponseMessage response = client.GetAsync(baseAddr + uri).Result;
                HttpContent content = response.Content;
                string result = await content.ReadAsStringAsync();
                int start = result.IndexOf("<JSON>");
                int end = result.IndexOf("</JSON>");
                if (start > 0 && end > 0)
                    return result.Substring(start + 6, end - start - 6);
                else
                    return result;                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static async Task<string> Post(string baseAddr, string uri, List<KeyValuePair<string, string>> kvpList)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddr);
                FormUrlEncodedContent content = new FormUrlEncodedContent(kvpList);                
                HttpResponseMessage response = await client.PostAsync(uri, content);
                string resultContent = await response.Content.ReadAsStringAsync();
                int start = resultContent.IndexOf("<JSON>");
                int end = resultContent.IndexOf("</JSON>");                
                if (start > 0 && end > 0)
                    return resultContent.Substring(start + 6, end - start - 6);
                else
                    return resultContent;
            }
        }

    }
}
