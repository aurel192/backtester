﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Net.Http;
using ClassLibrary1.Models;
using Newtonsoft.Json;

namespace ClassLibrary1
{
    public class OtpFund
    {
        public int id { get; set; }
        public string name { get; set; }
        public string issn { get; set; }
        public string currency { get; set; }
        public DateTime lastupdate { get; set; }
        public int state { get; set; }
        public int number { get; set; }
    }

    public static class RSQL
    {
        public static List<OtpFund> otpfund = new List<OtpFund>();

        public static void InitRemoteSql()
        {            
            otpfund = RemoteSQL.Select(Constants.host, "SELECT * FROM otpfund", new List<OtpFund>());
        }
    }

    public static class RemoteSQL
    {
     
        public static T Select<T>(string addr, string query, T res)
        {         
            string jsonString = Http.Get(Constants.host, "/Remotesql/select/?first=false&query=" + query).Result;
            Type type = res.GetType();
            res = JsonConvert.DeserializeObject<T>(jsonString);
            return res;
        }

        public static T SelectFirst<T>(string addr, string query, T res)
        {            
            string jsonString = Http.Get(Constants.host, "/Remotesql/select/?first=true&query=" + query).Result;
            Type type = res.GetType();
            res = JsonConvert.DeserializeObject<T>(jsonString);
            return res;
        }

    }
}
