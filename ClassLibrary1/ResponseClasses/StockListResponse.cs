﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.ResponseClasses
{
    public enum stockType { OtpFunds = 1, Equities = 2, Indicies = 3, Forex = 4, Commodities = 5, HungarianEquities = 6, HungarianMutualFunds = 7, HungarianEquitiesBET = 8, HungarianMaxIndexes = 9 }

    public class StockListResponse
    {
        public List<StockListItem> list { get; set; }

        public StockListResponse()
        {
            this.list = new List<StockListItem>();
        }
    }

    public class StockListItem
    {
        public int Id { get; set; }
        public string name { get; set; }
        public DateTime min { get; set; } = new DateTime(1900, 1, 1);
        public DateTime max { get; set; } = DateTime.Today;
    }
}
