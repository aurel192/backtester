﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.ResponseClasses
{
    public class TIME_SERIES_DAILY_ADJUSTED
    {
        public DateTime timestamp { get; set; }
        public double open { get; set; }
        public double high { get; set; }
        public double low { get; set; }
        public double close { get; set; }
        public double adjusted_close { get; set; }
        public double volume { get; set; }
        public double dividend_amount { get; set; }
        public double split_coefficient { get; set; }
        public double capitalization { get; set; }
    }

    public class TIME_SERIES_DAILY_ADJUSTED_MIN
    {
        public DateTime timestamp { get; set; }
        public double adjusted_close { get; set; }
        public double volume { get; set; }
        public double dividend_amount { get; set; }
    }

}
