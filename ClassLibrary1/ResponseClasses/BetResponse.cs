﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.ResponseClasses
{
    public class BET_RESZVENY_NAPI_RESPONSE
    {
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public double? Close { get; set; }
        public double? Open { get; set; }
        public double? Low { get; set; }
        public double? High { get; set; }
        public double? Avg { get; set; }
        public double? Kapitalizacio { get; set; }
        public double? ForgalomDb { get; set; }
        public double? ForgalomHUF { get; set; }
        public double? ForgalomEUR { get; set; }
        public int? Kotesek { get; set; }
        public string Deviza { get; set; }
    }

    public class BET_RESZVENY_NAPI_RESPONSE_STRING
    {
        public string Name { get; set; }
        public string Date { get; set; }
        public string Close { get; set; }
        public string Open { get; set; }
        public string Low { get; set; }
        public string High { get; set; }
        public string Avg { get; set; }
        public string Kapitalizacio { get; set; }
        public string ForgalomDb { get; set; }
        public string ForgalomHUF { get; set; }
        public string ForgalomEUR { get; set; }
        public string Kotesek { get; set; }
        public string Deviza { get; set; }
    }
}
