﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.ResponseClasses
{
    public class PORFOLIO_HUN_EQUITY
    {
        public DateTime Date { get; set; }
        public double Open { get; set; }
        public double High { get; set; }
        public double Low { get; set; }
        public double Close { get; set; }
        public double Avg { get; set; }
        public double Volume { get; set; }
        public double VolumeCount { get; set; }
    }

    public class PORFOLIO_HUN_MUTUALFUND
    {
        public DateTime Date { get; set; }
        public double Capitalization { get; set; } // Nettó eszközérték
        public double Cashflow { get; set; }
        public double Price { get; set; } // Egy jegyre jutó nettó eszközérték
        public double YearlyYield { get; set; }
    }
}