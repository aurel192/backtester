﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.ResponseClasses
{
    public class TSDAListItem
    {
        public List<TIME_SERIES_DAILY_ADJUSTED_MIN> datas { get; set; }
        public string name { get; set; }
    }

    public class TSDACListItem
    {
        public List<TIME_SERIES_DAILY_ADJUSTED> datas { get; set; }
        public string name { get; set; }
    }

    public class CompareResponse
    {
        public DateTimeData DateTimeData { get; set; }
        public List<PercentData> PercentData { get; set; }
        public List<RankingItem> RankingData { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public double min { get; set; }
        public double max { get; set; }
        public string comment { get; set; } = "";
        public CompareResponse()
        {
            this.DateTimeData = new DateTimeData();
            this.PercentData = new List<PercentData>();
            this.RankingData = new List<RankingItem>();
        }
    }

    public class ChartResponse
    {
        public DateTimeData DateTimeData { get; set; }
        public TimeData TimeData { get; set; }

        public PriceData PriceData { get; set; }
        public PercentData PercentData { get; set; }
        public CapitalizationData CapitalizationData { get; set; }
        public VolumeData VolumeData { get; set; }
        
        public RsiData RsiData { get; set; }
        public MacdData MacdData { get; set; }

        public SignalData SignalData { get; set; }
        public PositionsData PositionsData { get; set; }

        public TimingData TimingData { get; set; }

        public RankingItem Ranking { get; set; }

        public string Name { get; set; }
        public string ShortName { get; set; }
        public string type { get; set; }        

        //public TimeSpan From { get; set; }
        //public TimeSpan To { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }

        public ChartResponse(string Name, string ShortName, TimeSpan From, TimeSpan To, bool InitPriceData = true, bool InitVolumeData = true, bool InitRsiData = true, bool InitMacdData = true, bool InitSignalData = true, bool InitPositionsData = true, string type = "intraday")
        {
            this.Name = Name;
            this.ShortName = ShortName;
            //this.From = From;
            //this.To = To;
            this.type = type;            
            this.TimeData = new TimeData();
            InitData(InitPriceData, InitVolumeData, InitRsiData, InitMacdData, InitSignalData, InitPositionsData);
        }

        public ChartResponse(string Name, string ShortName, DateTime DateFrom, DateTime DateTo, bool InitPriceData = true, bool InitVolumeData = true, bool InitRsiData = true, bool InitMacdData = true, bool InitSignalData = true, bool InitPositionsData = true, string type = "daily")
        {            
            this.Name = Name;
            this.ShortName = ShortName;
            this.DateFrom = DateFrom;
            this.DateTo = DateTo;
            this.type = type;
            this.DateTimeData = new DateTimeData();
            InitData( InitPriceData, InitVolumeData, InitRsiData, InitMacdData, InitSignalData, InitPositionsData);
        }

        public void InitData(bool InitPriceData, bool InitVolumeData, bool InitRsiData, bool InitMacdData, bool InitSignalData, bool InitPositionsData)
        {
            if (InitPriceData)
            {
                this.PriceData = new PriceData("Price", "line");
                this.PercentData = new PercentData("Percent", "line");
            }
            if (InitVolumeData)
            {
                this.CapitalizationData = new CapitalizationData("Capitalization", "line");
                this.VolumeData = new VolumeData("Volume", "line");
            }
            if (InitRsiData)
                this.RsiData = new RsiData("RSI", "line");
            if (InitMacdData)
                this.MacdData = new MacdData();
            if (InitSignalData)
                this.SignalData = new SignalData();
            if (InitPositionsData)
                this.PositionsData = new PositionsData();
            this.TimingData = new TimingData();
        }
    }

    public class TimeData
    {
        public List<TimeSpan> points { get; set; }
        public TimeSpan min { get; set; }
        public TimeSpan max { get; set; }
        public TimeData()
        {
            this.points = new List<TimeSpan>();
        }
    }

    public class DateTimeData
    {
        public List<DateTime> points { get; set; }
        public DateTime min { get; set; }
        public DateTime max { get; set; }
        public DateTimeData()
        {
            this.points = new List<DateTime>();
        }
    }

    public class PriceData : SeriesData
    {
        public List<double> points { get; set; }
        public List<OHLC> ohlcpoints { get; set; }
        public PriceData(string name, string type)
        {
            this.points = new List<double>();
            this.ohlcpoints = new List<OHLC>();
            base.name = name;
            base.type = type;
        }
    }

    public class OHLC
    {
        public double open { get; set; }
        public double high { get; set; }
        public double low { get; set; }
        public double close { get; set; }
    }

    public class PercentData : SeriesData
    {
        public List<double> points { get; set; }
        public string color { get; set; }
        public PercentData(string name, string type, string color = "#0000FF")
        {
            this.points = new List<double>();
            base.name = name;
            base.type = type;
            this.color = color;
        }
    }

    public class TimingData
    {
        public List<Timing> times { get; set; }
        public int miliseconds { get; set; }
        public string comment { get; set; }
        public TimingData()
        {
            this.times = new List<Timing>();
        }
    }

    public class CapitalizationData : SeriesData
    {
        public List<double> points { get; set; }
        public CapitalizationData(string name, string type)
        {
            this.points = new List<double>();
            base.name = name;
            base.type = type;
        }
    }
    
    public class VolumeData : SeriesData
    {
        public List<double> points { get; set; }
        public VolumeData(string name, string type)
        {
            this.points = new List<double>();
            base.name = name;
            base.type = type;
        }
    }

    public class RsiData : SeriesData
    {
        public List<double> points { get; set; }
        public RsiData(string name, string type)
        {
            this.points = new List<double>();
            base.name = name;
            base.type = type;
        }
    }

    public class MacdData
    {
        public Macd Macd { get; set; }
        public MacdHistogram MacdHistogram { get; set; }
        public MacdSignal MacdSignal { get; set; }
        public MacdData()
        {
            this.Macd = new Macd("MACD", "line");
            this.MacdHistogram = new MacdHistogram("MACD Histogram", "line");
            this.MacdSignal = new MacdSignal("MACD Signal", "line");
        }
    }

    public class Macd : SeriesData
    {
        public List<double> points { get; set; }
        public Macd(string name, string type)
        {
            this.points = new List<double>();
            base.name = name;
            base.type = type;
        }
    }

    public class MacdHistogram : SeriesData
    {
        public List<double> points { get; set; }
        public MacdHistogram(string name, string type)
        {
            this.points = new List<double>();
            base.name = name;
            base.type = type;
        }
    }

    public class MacdSignal : SeriesData
    {
        public List<double> points { get; set; }
        public MacdSignal(string name, string type)
        {
            this.points = new List<double>();
            base.name = name;
            base.type = type;
        }
    }



    public class SeriesData
    {
        public SeriesData()
        {
        }
        public SeriesData(string name, string type)
        {
            this.name = name;
            this.type = type;
        }
        public string name { get; set; } = "Name";
        public string type { get; set; } = "line";
        public double min { get; set; }
        public double max { get; set; }
        public double interval { get; set; }
    }

    public class MACDValues : DateTimeDoublePair
    {
        public double macd { get; set; }
        public double hist { get; set; }
        public double signal { get; set; }
    }

    public class DateTimeDoublePair
    {
        public DateTime date { get; set; }
        public double value { get; set; }
    }

    public class SignalData
    {
        public SignalData()
        {
            this.signals = new List<Signal>();
        }
        public List<Signal> signals { get; set; }
    }

    /********************************************/

    public class StockDataResponse
    {
        public List<SeriesObject> list { get; set; }
        public double minPrice { get; set; }
        public double maxPrice { get; set; }
        public double intvPrice { get; set; }
        public double minVolume { get; set; }
        public double maxVolume { get; set; }
        public double intvVolume { get; set; }
        public macdminmax MacdValues { get; set; }
        public List<Signal> signals { get; set; }
        public PositionsData positions { get; set; }
        public List<Dividend> dividends { get; set; }
        public List<Timing> timings { get; set; }
        public string comment { get; set; }
        public List<Point> VolumeData { get; set; }

        public StockDataResponse()
        {
            this.VolumeData = new List<Point>();
            this.timings = new List<Timing>();
            this.dividends = new List<Dividend>();
            this.list = new List<SeriesObject>();
            this.MacdValues = new macdminmax();
            this.signals = new List<Signal>();
            this.positions = new PositionsData();
        }
    }

    //public class SeriesObject {}

    public class Dividend
    {
        public DateTime date { get; set; }
        public double dividendAmount { get; set; }
    }

    public class Timing
    {
        public TimeSpan time { get; set; }
        public string function { get; set; }
    }

    public class Signal : Point
    {
        public int pos { get; set; } // x tengely mentén hanyadik ponthoz tartozik
        public string type { get; set; } // BUY - SELL,  LONG, SHORT, CLOSE
        public string comment { get; set; }
        public double signalValue { get; set; } // Pl RSI 29 v MACD -21.5
    }

    public class macdminmax
    {
        public double macdMin { get; set; }
        public double macdMax { get; set; }
        public double macdHistMin { get; set; }
        public double macdHistMax { get; set; }
        public double macdSignalMin { get; set; }
        public double macdSignalMax { get; set; }

        public double macd { get; set; }
        public double hist { get; set; }
        public double sig { get; set; }
    }

    public class PointBase
    {
        public string x { get; set; } // Dátum
        public DateTime date { get; set; }
    }

    public class Point : PointBase
    {
        public double value { get; set; }
    }

    public class RSIPoint : Point
    {
        public double rsi { get; set; }
    }

    public class MACDPoint : Point
    {
        public double macd { get; set; }
        public double hist { get; set; }
        public double signal { get; set; }
    }

    public class SeriesObject
    {
        public List<Point> points { get; set; }
        public string name { get; set; } = "Name";
        public string type { get; set; } = "line";
    }


    public class PositionsData
    {
        public List<OpenPosition> openPositions { get; set; }
        public List<ClosedPosition> closedPositions { get; set; }
        public double sumProfit { get; set; }

        public PositionsData()
        {
            this.openPositions = new List<OpenPosition>();
            this.closedPositions = new List<ClosedPosition>();
        }
    }

    public class OpenPosition
    {
        //public string openedStr { get; set; } // Depricated
        public double openPrice { get; set; }
        public DateTime opened { get; set; }
    }

    public class ClosedPosition : OpenPosition
    {
        //public string closedStr { get; set; } // Depricated
        public double closePrice { get; set; }
        public double profit { get; set; }
        public double profitPercent { get; set; }
        public DateTime closed { get; set; }
        public int interval { get; set; }
    }
}
