﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.ResponseClasses
{
    public class MultiThreadedResult : ResultBase
    {
        public List<int> Results { get; set; }
        public override string ToString()
        {
            return "Affinity " + base.Affinity + " " + base.DateTime.ToString() + " Results: " + this.Results.Count + "\n";
        }
    }

    public class ResultBase
    {
        public DateTime DateTime { get; set; }
        public int Affinity { get; set; }
    }
}

