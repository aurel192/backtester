﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.ResponseClasses
{
    public class compareInstrumentsParam
    {
        public List<FormElement> formData { get; set; }
        public DateTime datefrom { get; set; }
        public DateTime dateto { get; set; }
    }

    public class FormElement
    {
        public int type { get; set; }
        public int stock { get; set; }
        public string text { get; set; }
    }
}
