﻿using ClassLibrary1.ResponseClasses;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class MultiThreadedWorker
    {
        public List<MultiThreadedResult> resultList;

        public MultiThreadedWorker()
        {
            this.resultList = new List<MultiThreadedResult>();
        }

        public List<MultiThreadedResult> WorkerMain(int num)
        {
            List<Thread> threadList = new List<Thread>();

            int affinity = 1;
            for (int i = 0; i < num; i++)
            {
                var tws = new ThreadWithState("Thread " + i + " started.   Affinity: " + affinity + " \n", affinity, new Callback(ResultCallback));
                threadList.Add(new Thread(new ThreadStart(tws.ThreadProc)));
                affinity *= 2;
            }

            foreach (var t in threadList)
                t.Start();

            foreach (var t in threadList)
                t.Join();

            return resultList;
        }
        
        public void ResultCallback(MultiThreadedResult result)
        {
            this.resultList.Add(result);
            Console.WriteLine("Independent task printed {0} lines.", result.DateTime.ToShortTimeString());
        }
    }

    public class ThreadWithState
    {
        private object Parameter;
        private int Affinity;        
        private Callback CallbackDelegate;
        
        public ThreadWithState(string strParam, int affinity, Callback callbackDelegate)
        {
            //Process.GetCurrentProcess().ProcessorAffinity = new IntPtr(affinity);
            //Thread.BeginThreadAffinity();
            this.Parameter = strParam;
            this.Affinity = affinity;
            this.CallbackDelegate = callbackDelegate;
        }

        public static List<int> GeneratePrimesNaive(int n)
        {
            List<int> primes = new List<int> { 2 };
            int nextPrime = 3;
            while (primes.Count < n)
            {
                int sqrt = (int)Math.Sqrt(nextPrime);
                bool isPrime = true;
                for (int i = 0; (int)primes[i] <= sqrt; i++)
                {
                    if (nextPrime % primes[i] == 0)
                    {
                        isPrime = false;
                        break;
                    }
                }
                if (isPrime)
                {
                    primes.Add(nextPrime);
                }
                nextPrime += 2;
            }
            return primes;
        }

        public void ThreadProc()
        {
            //Thread.Sleep(this.value);
            List<int> primes = GeneratePrimesNaive(500000);
            Console.WriteLine("ThreadProc " + Affinity);
            //Thread.EndThreadAffinity();
            if (this.CallbackDelegate != null)
                this.CallbackDelegate(new MultiThreadedResult() { DateTime = DateTime.Now, Results = primes, Affinity = Process.GetCurrentProcess().ProcessorAffinity.ToInt32() });
        }
    }
    
    public delegate void Callback(MultiThreadedResult result);

}
