﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;
using System.IO;
using ExcelDataReader;
using ClassLibrary1.ResponseClasses;

namespace ClassLibrary1
{
    public static class AKKHelper
    {
        // http://www.akk.hu/hu/statisztika/hozamok-indexek-forgalmi-adatok/max-index?dateStart=2017-01-01&dateEnd=2018-01-01&download=1

        public static List<TIME_SERIES_DAILY_ADJUSTED> GetMaxIndexDataBetweenDates(DateTime fromDate, DateTime toDate, string index)
        {
            return (from m in Db.Instance.akk_max
                    where m.Index == index
                    && m.Datum >= fromDate && m.Datum <= toDate
                    orderby m.Datum
                    select new TIME_SERIES_DAILY_ADJUSTED
                    {
                        timestamp = m.Datum,
                        adjusted_close = (double)m.Ertek,
                        volume = m.EvesHozam
                    }).ToList();
        }
        
        public static void DownloadArchiveData()
        {
            for (int y=1997; y<=2018; y++)
            {
                string path = "hu/statisztika/hozamok-indexek-forgalmi-adatok/max-index?dateStart=" + y + "-01-01&dateEnd=" + y + "-12-31&download=1";
                Console.WriteLine("\n\n" + path);
                string fileName = "MAX" + y.ToString() + ".xlsx";
                Common.SaveFileFromURL("http://www.akk.hu/" + path, @"c:\shared\AKK\" + fileName, 10);
            }
        }

        public static void ParseXLS()
        {
            for (int y = 1997; y <= 2018; y++)
            {
                ExcelReader(@"c:\shared\AKK\" + "MAX" + y + ".xlsx");
            }
        }

        public static void ExcelReader(string filePath)
        {
            Console.WriteLine("ExcelReader " + filePath);
            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    do
                    {
                        bool header = true;
                        while (reader.Read())
                        {
                            try
                            {
                                if (header == false)
                                {
                                    var datum = reader.GetValue(0);
                                    var erteknap = reader.GetValue(1);
                                    var index = reader.GetValue(2);
                                    var ertek = reader.GetValue(3);
                                    var valtozas = reader.GetValue(4);
                                    var eveshozam = reader.GetValue(5);
                                    Console.WriteLine(datum + " " + erteknap + " " + index + " " + ertek + " " + valtozas + " " + eveshozam);
                                    akk_max max = new akk_max();
                                    max.Datum = DateTime.Parse((string)datum);
                                    max.Erteknap = DateTime.Parse((string)erteknap);
                                    max.Index = (string)index;
                                    max.Ertek = ((double)ertek).ToFloat();
                                    max.Valtozas = ((double)valtozas).ToFloat();
                                    max.EvesHozam = ((double)eveshozam).ToFloat();
                                    Db.Instance.akk_max.Add(max);
                                }
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                            header = false;
                        }
                    } while (reader.NextResult());
                }
            }
            Db.Instance.SaveChanges();
        }

    }
}
