﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public static class Common
    {
        public static List<T> ParseCSV<T>(string csvString, string delimiter)
        {
            List<T> response = new List<T>();
            try
            {
                using (TextReader sr = new StringReader(csvString))
                {
                    CsvHelper.Configuration.Configuration config = new CsvHelper.Configuration.Configuration() { CultureInfo = new CultureInfo("en-US") };
                    config.MissingFieldFound = null;
                    config.Delimiter = delimiter;
                    var csv = new CsvReader(sr, config);
                    csv.Read();
                    csv.ReadHeader();
                    response = csv.GetRecords<T>().ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response;
        }

        public static int GetNthIndex(string s, char t, int n)
        {
            int count = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] == t)
                {
                    count++;
                    if (count == n)
                    {
                        return i;
                    }
                }
            }
            return -1;
        }

        public static string readStringFromFile(string path)
        {
            string text = System.IO.File.ReadAllText(path);
            return text;
        }

        public static void Logger(string contents)
        {
            Console.WriteLine(contents);
            //set up a filestream
            FileStream fs = new FileStream(@"d:\log_" + DateTime.Today.ToDateStringWithoutSpecialCharacters() + ".txt", FileMode.OpenOrCreate, FileAccess.Write);
            //set up a streamwriter for adding text
            StreamWriter sw = new StreamWriter(fs);
            //find the end of the underlying filestream
            sw.BaseStream.Seek(0, SeekOrigin.End);
            //add the text 
            sw.WriteLine(contents);
            //add the text to the underlying filestream
            sw.Flush();
            //close the writer
            sw.Close();
        }
                
        public static bool SaveFileFromURL(string url, string destinationFileName, int timeoutInSeconds)
        {
            // Create a web request to the URL
            HttpWebRequest MyRequest = (HttpWebRequest)WebRequest.Create(url);
            MyRequest.Timeout = timeoutInSeconds * 1000;
            try
            {
                // Get the web response
                HttpWebResponse MyResponse = (HttpWebResponse)MyRequest.GetResponse();
                // Make sure the response is valid
                if (HttpStatusCode.OK == MyResponse.StatusCode)
                {
                    // Open the response stream
                    using (Stream MyResponseStream = MyResponse.GetResponseStream())
                    {
                        // Open the destination file
                        using (FileStream MyFileStream = new FileStream(destinationFileName, FileMode.OpenOrCreate, FileAccess.Write))
                        {
                            // Create a 4K buffer to chunk the file
                            byte[] MyBuffer = new byte[4096];
                            int BytesRead;
                            // Read the chunk of the web response into the buffer
                            while (0 < (BytesRead = MyResponseStream.Read(MyBuffer, 0, MyBuffer.Length)))
                            {   // Write the chunk from the buffer to the file 
                                MyFileStream.Write(MyBuffer, 0, BytesRead);
                            }
                        }
                    }
                }
            }
            catch (Exception err) { throw new Exception("Error saving file from URL:" + err.Message, err); }
            return true;
        }

        public static string Yahoo()
        {
            // http://ichart.yahoo.com/table.csv?s=MSFT&a=0&b=1&c=2000&d=11&e=24&f=2014&g=w&ignore=.csv
            // https://www.quantconnect.com/blog/downloading-yahoo-finance-data-with-c/
            string address = "http://ichart.yahoo.com/table.csv?s=MSFT&a=0&b=1&c=2000&d=11&e=24&f=2014&g=w&ignore=.csv";
            string result = Http.Get(address, "").Result;
            return result;
        }

        public static List<users> Users()
        {
            testdbEntities1 db = new testdbEntities1();
            List<users> juzerz = (from u in db.users select u).ToList();
            return juzerz;
        }
    }

    public class asd
    {
        public int price { get; set; }

        public static bool operator ==(asd d1, asd d2)
        {
            return (d1.price == d2.price);
        }

        public static bool operator !=(asd d1, asd d2)
        {
            return (d1.price != d2.price);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public static class Extensions
    {
        public static string ToDateStringWithoutSpecialCharacters(this DateTime date)
        {
            return date.Year.ToString() + date.Month.ToString().PadLeft(2, '0') + date.Day.ToString().PadLeft(2, '0');
        }

        public static void CopyTo(this object S, object T)
        {
            foreach (var pS in S.GetType().GetProperties())
            {
                foreach (var pT in T.GetType().GetProperties())
                {
                    if (pT.Name != pS.Name) continue;
                    (pT.GetSetMethod()).Invoke(T, new object[] { pS.GetGetMethod().Invoke(S, null) });
                }
            };
        }

        public static string ReplaceAt(this string str, int index, int length, string replace)
        {
            return str.Remove(index, Math.Min(length, str.Length - index)).Insert(index, replace);
        }

        public static string ToStr(this DateTime d)
        {
            if (d == null)
            {
                return "1900-01-01";
            }
            return d.ToString("yyyy-MM-dd");
        }

        public static bool InRange(this DateTime date, DateTime from, DateTime to)
        {
            return (date >= from && date <= to);
        }

        public static float ToFloat(this double dValue)
        {            
            if (float.IsPositiveInfinity(Convert.ToSingle(dValue)))
            {
                return float.MaxValue;
            }
            if (float.IsNegativeInfinity(Convert.ToSingle(dValue)))
            {
                return float.MinValue;
            }
            return Convert.ToSingle(dValue);
        }
    }

}
