﻿using ClassLibrary1.ResponseClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public static class BetHelper
    {
        public static void ProcessCSVStrings(string basePath)
        {
            try
            {
                string[] dirs = Directory.GetFiles(basePath);
                foreach (string path in dirs)
                {
                    Console.WriteLine(path);
                    string betcsv = System.IO.File.ReadAllText(path);
                    betcsv = betcsv.Replace("Név", "Name")
                                   .Replace("Dátum", "Date")
                                   .Replace("Utolsó ár", "Close")
                                   .Replace("Nyitó ár", "Open")
                                   .Replace("Minimum ár", "Low")
                                   .Replace("Maximum ár", "High")
                                   .Replace("Átlag ár", "Avg")
                                   .Replace("Kapitalizáció", "Kapitalizacio")
                                   .Replace("Forgalom (db)", "Forgalom")
                                   .Replace("Forgalom (HUF érték)", "ForgalomHUF")
                                   .Replace("Forgalom (EUR érték)", "ForgalomEUR")
                                   .Replace("Kötések száma", "Kotesek")
                                   .Replace("Deviza", "Deviza");
                    var datas = Common.ParseCSV<BET_RESZVENY_NAPI_RESPONSE>(betcsv, ",").OrderBy(d => d.Date).ToList();
                    var reszvenyNevek = datas.GroupBy(d => d.Name).Distinct().Select(d => d.Key).ToList();
                    foreach (string reszvenyNev in reszvenyNevek)
                    {
                        bet_reszveny reszveny = Db.Instance.bet_reszveny.Where(r => r.Name == reszvenyNev).FirstOrDefault();
                        if (reszveny == null)
                        {
                            string deviza = datas.Where(r => r.Name == reszvenyNev).FirstOrDefault().Deviza;
                            DateTime first = datas.OrderBy(r => r.Date).FirstOrDefault().Date;
                            reszveny = new bet_reszveny { Name = reszvenyNev, Deviza = deviza, First = first, Last = first };
                            Db.Instance.bet_reszveny.Add(reszveny);
                            Db.Instance.SaveChanges();
                        }

                        ReszvenyAdatokFeltoltese(reszveny, datas.Where(d => d.Name == reszveny.Name).ToList());
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.Instance.LogException(ex);
            }
        }

        private static void ReszvenyAdatokFeltoltese(bet_reszveny betReszveny, List<BET_RESZVENY_NAPI_RESPONSE> datas)
        {
            var ExistingDates = Db.Instance.bet_reszveny_napi.Where(r => r.BetReszvenyId == betReszveny.Id).Select(r => r.Date).ToList();
            var NapiAdatok = datas.Where(d => ExistingDates.Contains(d.Date) == false)
                                                           .Select(d => new bet_reszveny_napi
                                                           {
                                                               BetReszvenyId = betReszveny.Id,
                                                               Date = d.Date,
                                                               Open = d.Open,
                                                               Avg = d.Avg,
                                                               Low = d.Low,
                                                               High = d.High,
                                                               Close = d.Close,
                                                               ForgalomDb = d.ForgalomDb,
                                                               ForgalomEUR = d.ForgalomEUR,
                                                               ForgalomHUF = d.ForgalomHUF,
                                                               Kotesek = d.Kotesek,
                                                               Kapitalizacio = d.Kapitalizacio
                                                           }).ToList();
            int cntr = 0;
            foreach (var napiAdat in NapiAdatok)
            {
                Db.Instance.bet_reszveny_napi.Add(napiAdat);
                if (cntr++ == 100)
                {
                    Db.Instance.SaveChanges();
                    betReszveny.Last = Db.Instance.bet_reszveny_napi
                                       .Where(r => r.BetReszvenyId == betReszveny.Id)
                                       .OrderByDescending(r => r.Date).FirstOrDefault().Date;
                    Db.Instance.SaveChanges();
                    cntr = 0;
                }
            }
            Db.Instance.SaveChanges();
            var last = Db.Instance.bet_reszveny_napi
                         .Where(r => r.BetReszvenyId == betReszveny.Id)
                         .OrderByDescending(r => r.Date).FirstOrDefault();
            if (last != null)
                betReszveny.Last = last.Date;
            Db.Instance.SaveChanges();
        }

        public static List<StockListItem> getBetEquities()
        {
            return Db.Instance.bet_reszveny.Select(b => new StockListItem { Id = b.Id, min = b.First, max = b.Last, name = b.Name }).ToList();
        }

        public static List<bet_reszveny_napi> getBetEquityDailyDatas(DateTime fromDate, DateTime toDate, string Name)
        {
            bet_reszveny reszveny = Db.Instance.bet_reszveny.Where(r => r.Name == Name).FirstOrDefault();
            if (reszveny == null)
                throw new Exception("BET Equity not found");
            return Db.Instance.bet_reszveny_napi.Where(r => r.BetReszvenyId == reszveny.Id && r.Date >= fromDate && r.Date <= toDate && r.Close > 0)
                                                .OrderBy(r=>r.Date).ToList();
        }
    }
}
/*
select BetReszvenyId,Name,First,Last, count(*)as Darab from bet_reszveny_napi
join bet_reszveny on bet_reszveny.id = bet_reszveny_napi.BetReszvenyId
group by BetReszvenyId;

BetReszvenyId	Name	First	            Last	            Darab
7	            RICHTER	1994-11-09 00:00:00	2017-10-24 00:00:00	5784
8	            OTP	    1995-01-05 00:00:00	2017-10-24 00:00:00	5599
9	            MOL	    1995-01-05 00:00:00	2017-10-24 00:00:00	5522



 */
