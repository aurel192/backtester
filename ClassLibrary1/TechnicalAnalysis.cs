﻿using ClassLibrary1.ResponseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacTec.TA.Library;

namespace ClassLibrary1
{
    public static class TechnicalAnalysis
    {
        public static List<RSIPoint> RSI(List<DateTimeDoublePair> points, int period = 14)
        {
            List<RSIPoint> result = new List<RSIPoint>();
            try
            {
                int begIdx = -1;
                int numberOfElements = -1;
                double[] outArray = new double[points.Count + period];
                var srcArray = (from p in points select p.value).ToArray();
                Core.Rsi(0, points.Count - 1, srcArray, period, out begIdx, out numberOfElements, outArray);
                int i = 0;
                foreach (var p in points)
                {
                    RSIPoint rsiPoint = new RSIPoint() { date = p.date, rsi = 0, x = "", value = p.value };
                    if (i >= begIdx)
                        rsiPoint.rsi = outArray[i - begIdx];
                    else
                        rsiPoint.rsi = 50;
                    result.Add(rsiPoint);
                    i++;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public static List<MACDValues> MACD(List<DateTimeDoublePair> points, int fast = 12, int slow = 26, int signal = 9)
        {
            List<MACDValues> result = new List<MACDValues>();
            try
            {
                int begIdx = -1;
                int numberOfElements = -1;
                int size = points.Count + (slow * 2);
                double[] outMACD = new double[size];
                double[] outHist = new double[size];
                double[] outMACDSignal = new double[size];
                var srcArray = (from p in points select p.value).ToArray();
                Core.Macd(0, points.Count - 1, srcArray, fast, slow, signal, out begIdx, out numberOfElements, outMACD, outMACDSignal, outHist);
                int i = 0;
                foreach (var p in points)
                {
                    MACDValues macdPoint = new MACDValues() { date = p.date, value = p.value };
                    if (i >= begIdx)
                    {
                        macdPoint.macd = outMACD[i - begIdx];
                        macdPoint.hist = outHist[i - begIdx];
                        macdPoint.signal = outMACDSignal[i - begIdx];
                    }
                    result.Add(macdPoint);
                    i++;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
