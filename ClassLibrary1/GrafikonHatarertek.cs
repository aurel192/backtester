﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public static class GrafikonHatarertek
    {
        public static Tuple<double, double, double> Szamitas(double min, double max)
        {
            //var kisebbnagyobb_min = getKisebbNagyobb(min);
            var kisebbnagyobb_max = getKisebbNagyobb(max);
            //Console.WriteLine("\nmin = " + min + "   (" + kisebbnagyobb_min.Item1 + " , " + kisebbnagyobb_min.Item2 + ")");            
            //Console.WriteLine("\nmax = " + max + "   (" + kisebbnagyobb_max.Item1 + " , " + kisebbnagyobb_max.Item2 + ")");
            var t = getMinMax(min, max, kisebbnagyobb_max.Item1 / 100);
            return new Tuple<double, double, double>(t.Item1, t.Item2, (t.Item2 - t.Item1) / 10);
        }

        private static Tuple<double, double> getMinMax(double min, double max, double p)
        {
            double from = -1;
            double to = -1;
            for (int i = 0; ; i++)
            {
                if ((i * p) < min)
                {
                    from = i * p;
                }
                if ((i * p) > max && to == -1)
                {
                    to = i * p;
                }
                if (from != -1 && to != -1) break;
            }
            return new Tuple<double, double>(from, to);
        }

        private static Tuple<double, double> getKisebbNagyobb(double ertek)
        {
            double p = 0.00001;
            double lower = 0;
            double higher = 0;
            while (true)
            {
                if (p < ertek && (p * 10) > ertek)
                {
                    lower = p;
                    higher = p * 10;
                    break;
                }
                p = p * 10;
            }
            return new Tuple<double, double>(lower, higher);
        }
    }
}
