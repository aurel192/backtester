﻿using ClassLibrary1.Models;
using ClassLibrary1.ResponseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Helper
    {
        private static Helper instance;

        private Helper()
        {
        }

        public static Helper Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Helper();

                }
                return instance;
            }
        }

        public void LogException(Exception e)
        {
            string str = "Date: " + DateTime.Now.ToString();
            str += "\n\n======================  EXCEPTION ======================\n\n\n";
            while (e != null)
            {
                str += "\n\nMESSAGE: " + e.Message;
                str += "\n\nTYPE: " + e.GetType();
                str += "\n\nSOURCE: " + e.Source;
                str += "\n\nSTACKTRACE: " + e.StackTrace;
                if (e.InnerException != null)
                {
                    str += "\n\n======================  INNER EXCEPTION ======================\n\n";
                }
                e = e.InnerException;
            }
            Console.WriteLine(str);
            Common.Logger(str);
            Task.Run(() => Email.SendMail("error@collectioninventory.com", "FTM EXCEPTION", str));
            //Email.SendMail("error@collectioninventory.com", "FTM EXCEPTION", str);
            //Email.SendMail2("kovacsaurel@gmail.com", "BACKTESTING EXCEPTION", str);
        }

        public List<double> getPercentData(List<double> datas)
        {
            List<double> list = new List<double>();
            double first = datas.FirstOrDefault();
            foreach (var p in datas)
            {
                list.Add(Math.Round((100 * p / first), 2));
            }
            return list;
        }

        public async Task<List<TIME_SERIES_DAILY_ADJUSTED>> getInstrumentDataBetweenDates(DateTime dtFrom, DateTime dtTo, stockType type, int stock, string stockCode)
        {
            List<TIME_SERIES_DAILY_ADJUSTED> datasWithPriorData = null;
            try
            {
                switch (type)
                {
                    case stockType.Equities:
                        //AlphaVantageHelper.getCachedAlphaVantageData<TIME_SERIES_DAILY_ADJUSTED>(stockCode, dtFromPrior, dtTo);
                        var av_kvp = AlphaVantageHelper.KVPBuilder(stockCode);
                        string av_query = AlphaVantageHelper.QueryBuilder(av_kvp);
                        string av_responseCsv = await AlphaVantageHelper.GetResponse(av_query);
                        datasWithPriorData = Common.ParseCSV<TIME_SERIES_DAILY_ADJUSTED>(av_responseCsv, ",").OrderBy(d => d.timestamp)
                                                                                                             .Where(d => d.timestamp >= dtFrom && d.timestamp <= dtTo).ToList();
                        break;

                    case stockType.OtpFunds:
                        datasWithPriorData = Otp.GetOtpFundDataBetweenDates(stock, dtFrom, dtTo).Select(otp => new TIME_SERIES_DAILY_ADJUSTED
                        {
                            adjusted_close = otp.price,
                            close = otp.price,
                            timestamp = otp.date,
                            capitalization = otp.capitalization, //Volume: Calculated later from capitalization
                        }).OrderBy(otp => otp.timestamp).ToList();
                        break;

                    case stockType.HungarianEquities:
                        portfolio_hunstock equity = Db.Instance.portfolio_hunstock.Where(s => s.ticker == stock).FirstOrDefault();
                        return await PortfolioHelper.GetHungarianEquityBetweenDates(equity.ticker, dtFrom, dtTo);

                    case stockType.HungarianMutualFunds:
                        portfolio_hunfunds fund = Db.Instance.portfolio_hunfunds.Where(s => s.ticker == stock).FirstOrDefault();
                        return await PortfolioHelper.GetHungarianMutualFundBetweenDates(fund.ticker, dtFrom, dtTo);

                    case stockType.HungarianEquitiesBET:
                        datasWithPriorData = BetHelper.getBetEquityDailyDatas(dtFrom, dtTo, stockCode).Select(p => new TIME_SERIES_DAILY_ADJUSTED
                        {
                            timestamp = p.Date,
                            capitalization = p.Kapitalizacio.HasValue ? p.Kapitalizacio.Value : 0,
                            adjusted_close = p.Close.HasValue ? p.Close.Value : 0,
                            close = p.Close.HasValue ? p.Close.Value : 0,
                            high = p.High.HasValue ? p.High.Value : 0,
                            low = p.Low.HasValue ? p.Low.Value : 0,
                            open = p.Open.HasValue ? p.Open.Value : 0,
                            volume = p.ForgalomHUF.HasValue ? p.ForgalomHUF.Value : 0,
                            dividend_amount = 0,
                            split_coefficient = 0,
                        }).ToList();
                        break;

                    case stockType.HungarianMaxIndexes:
                        return AKKHelper.GetMaxIndexDataBetweenDates(dtFrom, dtTo, stockCode);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return datasWithPriorData;
        }
    }


    public class Db
    {
        private static OtpDb dbinstance;
        
        public static OtpDb Instance
        {
            get
            {
                if (dbinstance == null)
                {
                    dbinstance = new OtpDb();
                }
                return dbinstance;
            }
        }
    }
}
