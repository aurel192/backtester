﻿function AjaxCall(type, url, data, callback) {
    $.ajax({
        type: type,
        url: url,
        data: data,
        error: function (e) {
            console.log('ERROR', e);
            var htmlErrorText = '';
            if (e.status === 0) {
                htmlErrorText += '<b>Unable to establish connection</b>';
            }
            else {
                htmlErrorText += '<b>ExceptionMessage:</b> ' + e.responseJSON.ExceptionMessage;
                htmlErrorText += '<br><b>ExceptionType:</b>  ' + e.responseJSON.ExceptionType;
                htmlErrorText += '<br><b>Message:</b>  ' + e.responseJSON.Message;
                htmlErrorText += '<br><b>StackTrace:</b>  ' + e.responseJSON.StackTrace;
            }
            swal({
                title: 'ERROR (' + e.status + ') - ' + e.statusText,
                html: htmlErrorText,
                type: 'error',
                showCancelButton: false,
                width: 800,
                confirmButtonColor: '#3085d6'
            }).then((result) => {
               
            });
            return callback(false, e);
        },
        success: function (response) {
            return callback(response);
        }
    });
}

function SetLocalStorage(name, obj) {
    if (typeof Storage !== "undefined") {
        localStorage.setItem(name, JSON.stringify(obj));
        return true;
    }
    return false;
}

function GetLocalStorage(name) {
    if (typeof Storage !== "undefined") {
        var stored = localStorage.getItem(name);
        if (stored === null)
            return false;
        return JSON.parse(stored);
    }
    return false;
}

var typeList = [
    { name: "-- Select a type --", Id: -1 },
    { name: "Otp Funds", Id: 1 },
    { name: "Stocks", Id: 2 },
    //{ name: "Indicies", Id: 3 },
    { name: "Hungarian Equities", Id: 6 },
    { name: "Hungarian Mutual Funds", Id: 7 },
    { name: "Hungarian Equities (BÉT)", Id: 8 },
    { name: "Hungarian Max Indexes", Id: 9 }
];

var seriesArray = [];

function initSeriesArray(arr) {
    seriesArray = [];
    if (Array.isArray(arr)) {        
        arr.forEach(function (element) {
            seriesArray.push({
                points: [],//element.points,
                name: element.name,
                type: 'line',
                yAxisName: 'priceAxis',
                fill: element.color,
                enableAnimation: false,
                tooltip:
                {
                    visible: true,
                    format: " #series.name#  <br/> #point.x# : #point.y# % "
                }
            });
        });
    }
    else {
        seriesArray =
            [
                { // [0] Price
                    points: [],
                    name: 'Price',
                    type: 'line',
                    yAxisName: 'priceAxis',
                    fill: "#000066",
                    enableAnimation: false,
                    tooltip:
                    {
                        visible: true,
                        format: " #series.name#  <br/> #point.x# : #point.y#   "
                    }
                },
                { /// [1] Capitalization
                    points: [],
                    name: 'Capitalization',
                    type: 'line',
                    yAxisName: 'capitalizationAxis',
                    fill: "#ff9933",
                    tooltip:
                    {
                        visible: true,
                        format: "#series.name#  <br/> #point.x# <br /> #point.y#"
                    }
                },
                { // [2] Volume
                    points: [],
                    name: 'Volume',
                    fill: "#007700",
                    type: 'column',
                    yAxisName: 'volumeAxis',
                    enableAnimation: false,
                    tooltip:
                    {
                        visible: true,
                        format: "#series.name# <br/> #point.x# <br /> #point.y#"
                    }
                }
            ];
    }
}


var seriesRSI = {// [3] RSI
    points: [],
    name: 'RSI',
    yAxisName: 'rsiAxis',
    type: 'line',
    fill: "#0099cc",
    tooltip:
    {
        visible: true,
        format: " #series.name#  <br/> #point.x# : #point.y#"
    }
};
var seriesMACDSignal = { // [4] MACD Signal
    points: [],
    name: 'MACD Signal',
    type: 'line',
    yAxisName: 'macdAxis',
    fill: "#0000BB",
    enableAnimation: false
};
var seriesMACD = { // [5]  MACD
    points: [],
    name: 'MACD',
    type: 'line',
    yAxisName: 'macdAxis',
    fill: "#BB0000",
    enableAnimation: false
};
var seriesMACDHist = { // [6] MACD Histogram
    points: [],
    name: 'MACD Histogram',
    fill: "#888888",
    type: 'column',
    yAxisName: 'macdAxisCol',
    enableAnimation: false
};

// http://js.syncfusion.com/demos/web/#!/lime/chart/rangecolumn
// http://jsplayground.syncfusion.com/o3l2dgke

var VolumeAxis = {
    orientation: 'Vertical',
    hidePartialLabels: false,
    rowIndex: 0,
    plotOffset: 0,
    //range: { min: 0, max: 100 },// interval: 0.01
    majorGridLines: { visible: true },
    axisLine: { visible: false },
    name: 'volumeAxis',
    labelFormat: '{value}',
    title: { text: "Volume" },
    opposedPosition: false
};

var PriceAxis = {
    orientation: 'Vertical',
    hidePartialLabels: false,
    rowIndex: 1,
    plotOffset: 0,
    //range: { min: 0, max: 2000, interval: 100 },
    majorGridLines: { visible: true },
    axisLine: { visible: false },
    name: 'priceAxis',
    labelFormat: '{value}',
    title: { text: "Price" },
    opposedPosition: false
};

var CapitalizationAxis = {
    majorGridLines:
    {
        visible: false
    },
    orientation: 'Vertical',
    rowIndex: 1,
    opposedPosition: true,
    axisLine: { visible: false },
    //range: { min: 0, max: 5000, interval: 500 },
    name: 'capitalizationAxis',
    labelFormat: '{value} M',
    title: { text: "Capitalization (Millions)" }
};

var MacdAxis = {
    orientation: 'Vertical',
    hidePartialLabels: false,
    rowIndex: 2,
    plotOffset: 0,
    range: { min: 0, max: 100 },// interval: 0.01
    majorGridLines: { visible: true },
    axisLine: { visible: false },
    name: 'macdAxis',
    title: { text: "MACD" },
    opposedPosition: false
};

var MacdAxisCol = {
    orientation: 'Vertical',
    hidePartialLabels: false,
    rowIndex: 2,
    plotOffset: 0,
    range: { min: -100, max: 100 }, // interval: 0.01
    majorGridLines: { visible: true },
    axisLine: { visible: false },
    name: 'macdAxisCol',
    title: { text: "MACD" },
    opposedPosition: true
};

var RsiAxis = {
    range: { min: 0, max: 100 },
    labelFormat: '{value}',
    axisLine: { visible: false },
    title: { text: "RSI" },
    name: 'rsiAxis',
    rowIndex: 3,
    stripLine:
    [
        {
            start: 0,
            end: 29.5,
            color: '#fff5f5',
            zIndex: 'behind',
            borderWidth: 0,
            visible: true
        },
        {
            start: 29.5,
            end: 30.5,
            color: '#bcbcbc',
            zIndex: 'behind',
            borderWidth: 0,
            visible: true
        },
        {
            start: 30.5,
            end: 69.5,
            color: '#F5F5F5',
            zIndex: 'behind',
            borderWidth: 0,
            visible: true
        },
        {
            start: 69.5,
            end: 70.5,
            color: '#bcbcbc',
            zIndex: 'behind',
            borderWidth: 0,
            visible: true
        },
        {
            start: 70.5,
            end: 100,
            color: '#f5fff5',
            zIndex: 'behind',
            borderWidth: 0,
            visible: true
        }
    ]
};

var annotations = [];
var axes = [];
var rowDefinitions = [];

function initAxes(arg) {
    axes = [];
    rowDefinitions = [];
    switch (arg) {
        case 'price':
            PriceAxis.title.text = "Price";
            PriceAxis.labelFormat = '{value}';
            rowDefinitions.push({// Volume
                    rowHeight: 13,
                    unit: 'percentage'
                });
            rowDefinitions.push({// Price + Capitalization    
                rowHeight: 87,
                unit: 'percentage'
            });
            axes.push(VolumeAxis);
            axes.push(CapitalizationAxis);
            break;
        case 'pricemacdrsi':
            PriceAxis.title.text = "Price";
            PriceAxis.labelFormat = '{value}';
            rowDefinitions.push({// Volume
                rowHeight: 13,
                unit: 'percentage'
            });
            rowDefinitions.push({// Price + Capitalization    
                rowHeight: 55,
                unit: 'percentage'
            });
            rowDefinitions.push({// MACD
                rowHeight: 20,
                lineColor: "gray",
                lineWidth: 1,
                unit: 'percentage'
            });
            rowDefinitions.push({// RSI
                rowHeight: 12,
                unit: 'percentage'
            });
            axes.push(MacdAxis);
            axes.push(MacdAxisCol);
            axes.push(RsiAxis);
            axes.push(VolumeAxis);
            axes.push(CapitalizationAxis);
            break;
        case 'compare':
            PriceAxis.title.text = "Percent";
            PriceAxis.labelFormat = '{value} %';
            break;
    }
}

function MultipleAxesChart(div, title, macdrsi) {   
    $(div).ejChart(
        {
            rowDefinitions: rowDefinitions,
            //Initializing Primary X Axis
            primaryXAxis:
            {
                title: { text: "Month" },
                labelRotation: 270
            },
            //Initializing Primary Y Axis
            primaryYAxis: PriceAxis,
            axes: axes,
            annotations: annotations,
            //Initializing Series
            series: seriesArray,
            load: "loadTheme",
            canResize: true,
            commonSeriesOptions: { enableAnimation: false },
            title: { text: title },
            size: { height: "800" },
            legend: { visible: true }
        });
}