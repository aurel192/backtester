﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ClassLibrary1;
using ClassLibrary1.ResponseClasses;
using ClassLibrary1.Models;
using System.Threading.Tasks;

namespace WebApp.Controllers
{
    public class getInstrumentDataController : ApiController
    {
        public async Task<ChartResponse> Get(int type, int stock, string datefrom, string dateto, string stockCode, int rsi = 14, int macdfast = 12, int macdslow = 26, int macdsignal = 9)
        {
            DateTime _start = DateTime.Now;
            DateTime _subStart = DateTime.Now;
            ChartResponse response = null;
            try
            {
                DateTime dtFrom = new DateTime();
                DateTime dtTo = new DateTime();
                int _maxOffset = (new List<int>() { rsi, macdfast, macdslow, macdsignal }).Max() * 3;
                if (!(DateTime.TryParse(datefrom, out dtFrom) && DateTime.TryParse(dateto, out dtTo)))
                    throw new Exception("Unable to parse date values!");
                response = new ChartResponse(stockCode, stockCode, dtFrom, dtTo);
                DateTime dtFromPrior = dtFrom.AddDays(-1 * _maxOffset);
                
                List<TIME_SERIES_DAILY_ADJUSTED> datasWithPriorData = await Helper.Instance.getInstrumentDataBetweenDates(dtFromPrior, dtTo, (stockType)type, stock, stockCode);

                List<TIME_SERIES_DAILY_ADJUSTED> datas = datasWithPriorData.Where(d => d.timestamp >= dtFrom).ToList();
                response.Ranking = Ranking.CreateRanking(datas, stock, stockCode, (stockType)type);

                response.DateTimeData.points = datas.Where(d => d.timestamp >= dtFrom && d.timestamp <= dtTo).Select(d => d.timestamp).ToList();
                response.PriceData.points = datas.Where(d => d.timestamp >= dtFrom && d.timestamp <= dtTo).Select(d => d.adjusted_close).ToList();
                if (datas.Where(d => d.high > 0 && d.low > 0 && d.open > 0 && d.close > 0).Any())
                    response.PriceData.ohlcpoints = datas.Where(d => d.timestamp >= dtFrom && d.timestamp <= dtTo).Select(d => new OHLC { open = d.open, close = d.adjusted_close, high = d.high, low = d.low }).ToList();
                response.CapitalizationData.points = datas.Where(d => d.timestamp >= dtFrom && d.timestamp <= dtTo).Select(d => d.capitalization/1000000).ToList();
                response.PercentData.points = Helper.Instance.getPercentData(response.PriceData.points);

                if (type == (int)stockType.OtpFunds || type == (int)stockType.HungarianMutualFunds)
                    response.VolumeData.points = calculateVolumeDataFromCapitalization(datasWithPriorData, dtFrom, dtTo);
                else
                    response.VolumeData.points = getVolumeChangeData(datasWithPriorData, dtFrom, dtTo);

                // Technical Indicators
                _subStart = DateTime.Now;
                List<Signal> rsiSignals = new List<Signal>();
                List<Signal> macdSignals = new List<Signal>();
                if (rsi > 0)
                {
                    var rsiPoints = TechnicalAnalysis.RSI(datasWithPriorData.Select(d => new DateTimeDoublePair { date = d.timestamp, value = d.adjusted_close }).ToList(), rsi)
                                                                        .Where(d => d.date >= dtFrom && d.date <= dtTo).ToList();
                    response.RsiData.points = rsiPoints.Select(r => r.rsi).ToList();

                    rsiSignals = Signals.GetRSISignals(rsiPoints);
                }

                if (macdfast > 0 && macdslow > 0 && macdsignal > 0)
                {
                    List<MACDValues> macdpoints = TechnicalAnalysis.MACD(datasWithPriorData.Select(d => new DateTimeDoublePair { date = d.timestamp, value = d.adjusted_close }).ToList(), macdfast, macdslow, macdsignal)
                                                               .Where(d => d.date >= dtFrom && d.date <= dtTo).ToList();
                    response.MacdData.Macd.points = macdpoints.Select(m => m.macd).ToList();
                    response.MacdData.MacdHistogram.points = macdpoints.Select(m => m.hist).ToList();
                    response.MacdData.MacdSignal.points = macdpoints.Select(m => m.signal).ToList();
                    macdSignals = Signals.GetMACDSignals(macdpoints);

                    response.MacdData.Macd.min = response.MacdData.Macd.points.Min();
                    response.MacdData.Macd.max = response.MacdData.Macd.points.Max();
                    response.MacdData.Macd.min = new List<double> { Math.Abs(response.MacdData.Macd.min), response.MacdData.Macd.max }.Max() * -1;
                    response.MacdData.Macd.max = new List<double> { Math.Abs(response.MacdData.Macd.min), response.MacdData.Macd.max }.Max();

                    response.MacdData.MacdHistogram.min = response.MacdData.MacdHistogram.points.Min();
                    response.MacdData.MacdHistogram.max = response.MacdData.MacdHistogram.points.Max();
                    response.MacdData.MacdHistogram.min = new List<double> { Math.Abs(response.MacdData.MacdHistogram.min), response.MacdData.MacdHistogram.max }.Max() * -1;
                    response.MacdData.MacdHistogram.max = new List<double> { Math.Abs(response.MacdData.MacdHistogram.min), response.MacdData.MacdHistogram.max }.Max();

                    response.MacdData.MacdSignal.min = response.MacdData.MacdSignal.points.Min();
                    response.MacdData.MacdSignal.max = response.MacdData.MacdSignal.points.Max();
                    response.MacdData.MacdSignal.min = new List<double> { Math.Abs(response.MacdData.MacdSignal.min), response.MacdData.MacdSignal.max }.Max() * -1;
                    response.MacdData.MacdSignal.max = new List<double> { Math.Abs(response.MacdData.MacdSignal.min), response.MacdData.MacdSignal.max }.Max();
                }

                response.TimingData.times.Add(new Timing { time = DateTime.Now - _subStart, function = "Technical Indicators" });
                
                response.SignalData.signals.AddRange(rsiSignals);
                response.SignalData.signals.AddRange(macdSignals);
                response.SignalData.signals = response.SignalData.signals.OrderBy(s => s.date).ToList();

                // Positions
                response.PositionsData = Signals.CalculateProfit(response.SignalData.signals);
                response.TimingData.times.Add(new Timing { time = DateTime.Now - _subStart, function = "Signals & Positions" });

                // Series ranges
                response.DateTimeData.min = response.DateTimeData.points.Min();
                response.DateTimeData.max = response.DateTimeData.points.Max();

                response.PriceData.min = response.PriceData.points.Where(p => p > 0).Min();
                response.PriceData.max = response.PriceData.points.Max();
                var minmaxintv = GrafikonHatarertek.Szamitas(response.PriceData.min, response.PriceData.max);
                response.PriceData.min = minmaxintv.Item1;
                response.PriceData.max = minmaxintv.Item2;
                response.PriceData.interval = minmaxintv.Item3;

                response.PercentData.min = response.PercentData.points.Min();
                response.PercentData.max = response.PercentData.points.Max();
                minmaxintv = GrafikonHatarertek.Szamitas(response.PercentData.min, response.PercentData.max);
                response.PercentData.min = minmaxintv.Item1;
                response.PercentData.max = minmaxintv.Item2;
                response.PercentData.interval = minmaxintv.Item3;

                if (response.CapitalizationData.points.Where(p => p > 0).Any())
                {
                    response.CapitalizationData.min = response.CapitalizationData.points.Where(p => p > 0).Min();
                    response.CapitalizationData.max = response.CapitalizationData.points.Max();
                    minmaxintv = GrafikonHatarertek.Szamitas(response.CapitalizationData.min, response.CapitalizationData.max);
                    response.CapitalizationData.min = minmaxintv.Item1;
                    response.CapitalizationData.max = minmaxintv.Item2;
                    response.CapitalizationData.interval = minmaxintv.Item3;
                }
                else {
                    response.CapitalizationData.min = 0;
                    response.CapitalizationData.max = 100;
                    response.CapitalizationData.interval = 20;
                }

                // A legelso napnal a kiugroan magas erteket nem kell megjeleniteni
                if (response.VolumeData.points.Count > 0 && response.VolumeData.points.Max() == response.VolumeData.points.First())
                {
                    response.VolumeData.points.RemoveAt(0);
                    response.VolumeData.points.Insert(0, 0);
                }
                if (response.VolumeData.points.Count > 0 && response.VolumeData.points.Min() == response.VolumeData.points.Last())
                {
                    response.VolumeData.points.RemoveAt(response.VolumeData.points.Count-1);
                    response.VolumeData.points.Add(0);
                }
                response.VolumeData.min = response.VolumeData.points.Min();
                response.VolumeData.max = response.VolumeData.points.Max();

             
                response.TimingData.comment = "Memory consumption: " + (GC.GetTotalMemory(true) / (1024)).ToString() + " Kb";
                response.TimingData.times.Add(new Timing { time = DateTime.Now - _start, function = "ALL" });
            }
            catch (Exception ex)
            {
                Helper.Instance.LogException(ex);
                throw ex;
            }
            return response;
        }    

        private static List<double> getVolumeChangeData(List<TIME_SERIES_DAILY_ADJUSTED> datas, DateTime first, DateTime last)
        {
            List<double> list = new List<double>();
            TIME_SERIES_DAILY_ADJUSTED prev = null;
            foreach (TIME_SERIES_DAILY_ADJUSTED p in datas)
            {
                double vol = p.volume;
                if (prev == null)
                {
                    prev = new TIME_SERIES_DAILY_ADJUSTED();
                    p.CopyTo(prev);
                    if (p.timestamp >= first && p.timestamp <= last)
                        list.Add(vol);
                    continue;
                }
                if (prev.adjusted_close > p.adjusted_close)
                {
                    vol *= -1;
                }
                p.CopyTo(prev);
                if (p.timestamp >= first && p.timestamp <= last)
                    list.Add(vol);
            }
            return list;
        }

        private static List<double> calculateVolumeDataFromCapitalization(List<TIME_SERIES_DAILY_ADJUSTED> datas, DateTime first, DateTime last) {
            List<double> list = new List<double>();
            double firstClosePrice = datas.FirstOrDefault().adjusted_close;
            TIME_SERIES_DAILY_ADJUSTED prev = new TIME_SERIES_DAILY_ADJUSTED() {  adjusted_close = firstClosePrice};
            foreach (TIME_SERIES_DAILY_ADJUSTED p in datas)
            {   
                if (p.timestamp >= first && p.timestamp <= last)
                {
                    double volume = p.capitalization - prev.capitalization;
                    list.Add(volume);                    
                }
                p.CopyTo(prev);
            }
            return list;
        }
    }

}
