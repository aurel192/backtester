﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ClassLibrary1;
using ClassLibrary1.ResponseClasses;
using ClassLibrary1.Models;
using System.Threading.Tasks;

namespace WebApp.Controllers
{
    public class compareInstrumentsController : ApiController
    {
        public async Task<CompareResponse> Post(compareInstrumentsParam parameter)
        {
            CompareResponse response = new CompareResponse();
            try
            {
                List<string> colors = new List<string>() {"#e6194b","#3cb44b","#ffe119", "#0082c8", "#f58231", "#911eb4", "#46f0f0", "#f032e6", "#d2f53c", "#fabebe", "#008080", "#e6beff", "#aa6e28", "#fffac8", "#800000", "#aaffc3", "#808000", "#ffd8b1", "#000080", "#808080"};
                parameter.datefrom = new DateTime(parameter.datefrom.Year, parameter.datefrom.Month, parameter.datefrom.Day, 0, 0, 0);
                parameter.dateto = new DateTime(parameter.dateto.Year, parameter.dateto.Month, parameter.dateto.Day, 23, 59, 59);
                response.DateTimeData.min = DateTime.MaxValue;
                response.DateTimeData.max = DateTime.MinValue;

                List<TSDAListItem> listIncomplete = new List<TSDAListItem>();

                foreach (FormElement s in parameter.formData)
                {
                    if (s.type < 1 || s.stock < 1 || string.IsNullOrEmpty(s.text)) continue;
                    TSDAListItem listItem = new TSDAListItem() { name = s.text };
                    var datas = await Helper.Instance.getInstrumentDataBetweenDates(parameter.datefrom, parameter.dateto, (stockType)s.type, s.stock, s.text);
                    listItem.datas = datas.Select(d => new TIME_SERIES_DAILY_ADJUSTED_MIN
                    {
                        timestamp = d.timestamp,
                        adjusted_close = d.adjusted_close
                    }).ToList();
                    List<DateTime> newDates = listItem.datas.Select(d => d.timestamp).ToList();
                    response.DateTimeData.points.AddRange(newDates.Except(response.DateTimeData.points));
                    listIncomplete.Add(listItem);

                    response.RankingData.Add(Ranking.CreateRanking(datas, s.stock, s.text, (stockType)s.type));
                }
                response.DateTimeData.points = response.DateTimeData.points.OrderBy(d => d).ToList();

                List<TSDAListItem> listComplete = new List<TSDAListItem>();
                foreach (TSDAListItem item in listIncomplete)
                {
                    TSDAListItem cItem = new TSDAListItem() { name = item.name, datas = new List<TIME_SERIES_DAILY_ADJUSTED_MIN>() };
                    foreach (DateTime date in response.DateTimeData.points)
                    {
                        cItem.datas.Add(getNearestPoint(date, item.datas));
                    }
                    listComplete.Add(cItem);
                    item.datas = null;
                }

                response.DateTimeData.min = response.DateFrom = response.DateTimeData.points.Min();
                response.DateTimeData.max = response.DateTo = response.DateTimeData.points.Max();
                response.min = double.MaxValue;
                response.max = double.MinValue;
                int cntr = 0;
                foreach(TSDAListItem item in listComplete)
                {
                    PercentData pd = new PercentData(item.name, "line", colors.ElementAt(cntr++))
                    {
                        points = Helper.Instance.getPercentData(item.datas.Select(d => d.adjusted_close).ToList())
                    };
                    pd.min = pd.points.Min();
                    pd.max = pd.points.Max();
                    if (pd.min < response.min)
                        response.min = Math.Floor(pd.min);
                    if (pd.max > response.max)
                        response.max = Math.Ceiling(pd.max);
                    response.PercentData.Add(pd);
                }
            }
            catch (Exception ex)
            {
                Helper.Instance.LogException(ex);
                throw ex;
            }
            return response;
        }
        
        private static TIME_SERIES_DAILY_ADJUSTED_MIN getNearestPoint(DateTime date, List<TIME_SERIES_DAILY_ADJUSTED_MIN> datas)
        {
            TIME_SERIES_DAILY_ADJUSTED_MIN ret = new TIME_SERIES_DAILY_ADJUSTED_MIN() { timestamp = date };
            try
            {
                var value = datas.Where(d => d.timestamp == date).FirstOrDefault();
                if (value != null)
                {
                    ret.adjusted_close = value.adjusted_close;
                }
                else
                {
                    var nd = findNearestDateWhenDataAvailable(date, datas.Select(d => d.timestamp).ToList());
                    var ndValue = datas.Where(d => d.timestamp == nd).FirstOrDefault();
                    if (ndValue != null)
                    {
                        ret.adjusted_close = ndValue.adjusted_close;
                    }
                    else
                    {
                        ret.adjusted_close = 0;
                    }
                    
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return ret;
        }

        private static DateTime findNearestDateWhenDataAvailable(DateTime targetDate, List<DateTime> dates)
        {
            DateTime closestDate = new DateTime(1900, 1, 1);
            try
            {
                
                long min = long.MaxValue;
                foreach (DateTime date in dates)
                {
                    if (Math.Abs(date.Ticks - targetDate.Ticks) < min)
                    {
                        min = Math.Abs(date.Ticks - targetDate.Ticks);
                        closestDate = date;
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return closestDate;
        }

    }
}
