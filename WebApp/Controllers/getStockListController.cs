﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ClassLibrary1;
using ClassLibrary1.ResponseClasses;

namespace WebApp.Controllers
{
    public class getStockListController : ApiController
    {
        public StockListResponse Get(int type)
        {
            var result = new StockListResponse();
            result.list = new List<StockListItem>() { new StockListItem { Id = -1, name = "-- Select --" } };
            switch (type)
            {
                case (int)stockType.OtpFunds:
                    result.list.AddRange(Otp.getStockList("rank").list);
                    break;
                case (int)stockType.HungarianEquitiesBET:
                    result.list.AddRange(BetHelper.getBetEquities());
                    break;
                case (int)stockType.Equities:
                    result.list.Add(new StockListItem { Id = 1, name = "OTP" });
                    result.list.Add(new StockListItem { Id = 2, name = "AAPL" });
                    result.list.Add(new StockListItem { Id = 3, name = "NVDA" });
                    result.list.Add(new StockListItem { Id = 4, name = "DJIA" });
                    break;
                case (int)stockType.HungarianEquities:
                    result.list.AddRange(PortfolioHelper.GetHungarianEquitiesList());
                    break;
                case (int)stockType.HungarianMutualFunds:
                    result.list.AddRange(PortfolioHelper.GetHungarianMutualFundsList());
                    break;
                case (int)stockType.HungarianMaxIndexes:
                    result.list.Add(new StockListItem { Id = 1, name = "ZMAX" });
                    result.list.Add(new StockListItem { Id = 2, name = "RMAX" });
                    result.list.Add(new StockListItem { Id = 3, name = "MAX" });
                    result.list.Add(new StockListItem { Id = 4, name = "CMAX" });
                    break;
            }            
            return result;
        }        
    }
}

/*
PORTFOLIO MAGYAR RÉSZVÉNYEK
7326:4IG
199012:ALTEO
309088:ALTERA
14352:ANY
184094:APPENINN
73:BIF
201819:CIGPANNONIA
41715:CSEPEL
454107:DUNAHOUSE
132:EHEP
135:ELMU
136:EMASZ
54314:ENEFI
314:ESTMEDIA
38262:ETFBUXOTP
5686:FHB
159157:FINEXT
5151:FORRAS/OE
5152:FORRAS/T
245958:FUTURAQUA
308155:GRENERGIE
31203:GSPARK
165:KARPOT
141638:KEG
168:KONZUM
170:KPACK
157754:KULCSSOFT
246133:MASTERPLAST
206:MOL
178:MTELEKOM
162225:NORDTELEKOM
212286:NUTEX
277:OPUS
204343:ORMESTER
266:OTP
197477:OTT1
283:PANNERGY
274:PFLAX
212183:PLOTINUS
288:RABA
295:RICHTER
237592:SET
467518:UBM
225612:VISONKA
494053:WABERERS
316:ZWACK
*/
