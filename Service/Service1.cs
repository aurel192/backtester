﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Timers;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary1;
using System.Threading;

namespace Service
{
    public partial class Service1 : ServiceBase
    {
        System.Timers.Timer timer = new System.Timers.Timer();        
        int lastDayRun = -1;

        public Service1()
        {
            lastDayRun = -1;            
            this.ServiceName = "FTM Service";
            InitializeComponent();
        }

        public void OnDebug()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            Common.Logger("\nSERVICE STARTED " + this.ServiceName + "  " + DateTime.Now.ToString());                      
            //ad 1: handle Elapsed event 
            timer.Elapsed += new ElapsedEventHandler(EveryMinute);
            //ad 2: set interval to 1 minute (= 60,000 milliseconds)
            timer.Interval = 60000;
            //ad 3: enabling the timer
            timer.Enabled = true;            
        }

        protected override void OnStop()
        {
            timer.Enabled = false;
            Common.Logger("\nSERVICE STOPPED " + this.ServiceName + "  " + DateTime.Now.ToString());
        }


        private void EveryMinute(object source, ElapsedEventArgs e)
        {
            Common.Logger("EveryMinute" + DateTime.Now.ToString());
            if (DateTime.Now.TimeOfDay > new TimeSpan(9, 0, 0) && DateTime.Now.TimeOfDay < new TimeSpan(17, 0, 0))
                NineToFive();
            if (lastDayRun != DateTime.Today.Day)                            
                OnceADay();                      
        }

        private void NineToFive()
        {
            Common.Logger("NineToFive" + DateTime.Now.ToString());
        }

        private void OnceADay()
        {
            lastDayRun = DateTime.Today.Day;
            Common.Logger("\nSTARTING OTP UPDATE " + DateTime.Now.ToString());
            Thread otpThread = new Thread(Otp.UpdateOtpFunds); // Starting a Static function
            otpThread.Start();            
        }
        
    }
}
